import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SETTINGS } from '../../settings/environment';
import { Observable, throwError } from 'rxjs';
import { map, catchError} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private http: HttpClient,
    private settings: SETTINGS) { }

  private baseurl: string = this.settings.ENDPOINT.COMMON;

  public getAllMachines(): Observable<any> {
    return this.http.get(this.baseurl + "/Common/GetMachines");
  }

  public getAllProcesses(): Observable<any> {
    return this.http.get(this.baseurl + "/Common/GetProcesses");
  }

  public getAllProducts(): Observable<any> {
    return this.http.get(this.baseurl + "/Common/GetProducts");
  }
  public getAllModules(): Observable<any> {
    return this.http.get(this.baseurl + '/Common/GetModules');
  }

  public getAllProductionLines(): Observable<any> {
    return this.http.get(this.baseurl + '/Common/GetProductionLines');
  }
}
