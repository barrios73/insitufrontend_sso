import { TestBed } from '@angular/core/testing';

import { MlModelService } from './mlmodel.service';

describe('MlmodelService', () => {
  let service: MlModelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MlModelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
