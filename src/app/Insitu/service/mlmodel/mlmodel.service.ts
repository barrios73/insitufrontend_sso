// @ts-ignore

import { Injectable } from '@angular/core';
import { HttpClient , HttpErrorResponse } from '@angular/common/http';
import { SETTINGS } from '../../settings/environment';
import { Observable, throwError } from 'rxjs';
import { map, tap, catchError  } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class MlModelService {
  constructor(private http: HttpClient,
    private settings: SETTINGS) {  }

  readonly baseurl: string = this.settings.ENDPOINT.MLMODEL + '/MLModel/';fmo
  loading = false;
  apiURL: string;


  public getAllModelTypes(): Observable<any> {
    return this.http.get(this.baseurl + 'GetAllModelTypes');
  }

  public getAllModelFiles(): Observable<any> {
    return this.http.get(this.baseurl + 'GetAllModelFiles');
  }

  public getModelFilesByModel(modelTypedId: number): Observable<any> {
    return this.http.get(this.baseurl + 'GetModelFilesByModel?ModelTypeId=' + modelTypedId);
  }

  public getPreprocessingFiles(): Observable<any> {
    return this.http.get(this.baseurl + 'GetAllPreprocessingFiles');
  }

  public postModel(form): Observable<any> {
    this.apiURL = this.baseurl + 'AddModel';
    this.loading = true;
    return this.http.post(this.apiURL, JSON.parse(JSON.stringify(form)))
      .pipe(
      tap(() => (this.loading = false))
    );
    // public getAllModules(): Observable<any>{
    //   return this.http.get(this.baseurl + '/MLModel/GetAllPreprocessingFiles');
    // };
  }

  private getServerErrorMessage(error: HttpErrorResponse): string {
    switch (error.status) {
      case 404: {
        return `Not Found: ${error.message}`;
      }
      case 403: {
        return `Access Denied: ${error.message}`;
      }
      case 500: {
        return `Internal Server Error: ${error.message}`;
      }
      default: {
        return `Unknown Server Error: ${error.message}`;
      }
    }
  }
}
