export interface IResponse {
    hasError: boolean;
    msg: string;
    data: any[];
}

export interface ModelFile {
    id: number;
    modelTypeId: number;
    modelFileName: string;
}

export interface PreprocessingFile {
    id: number;
    preprocessingFile: string;
}

export interface ModelType {
    id: number;
    modelName: string;
    topic: string;
}