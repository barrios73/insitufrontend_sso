import { Component, OnInit } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { HttpClient } from '@angular/common/http';
import { PassRate, PassRateWithProcessId } from '../../passRate';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { SortDescriptor } from '@progress/kendo-data-query';
import * as global from '../../global';
import { DatePipe, formatDate } from '@angular/common';
import { HttpHeaders } from '@angular/common/http';
import { SETTINGS } from '../../settings/environment';
import { KeycloakService } from 'keycloak-angular';
import {TranslateService} from '@ngx-translate/core';
import {CommonService} from '../../service/common/common.service';
//import { formatDate } from '@angular/common';

interface Machine {
  id: number;
  machineId: string;
  module: string;
  productionLine : string
}


@Component({
  selector: 'app-prediction-summary-page',
  templateUrl: './prediction-summary-page.component.html',
  styleUrls: ['./prediction-summary-page.component.scss']
})


export class PredictionSummaryPageComponent implements OnInit {

  public predictionResultList: any[] = [];
  public ResultSummaryGridData: any[];
  public passRateList: Array<PassRate> = [];
  public passRateWithProcessIdList: Array<PassRateWithProcessId> = [];
  private _hubConnection: HubConnection;
  public dashboardtime: string;
  public time: Date = new Date();
  public pageTitle: string = '';
  public pageSubtitle: string;
  timer;
  chartTimer;
  public widthValue: number = 30;
    public PredictedPassResult: string = "000.00";
  public PredictedFailResult: string = "0.00";
  public TotalFailCase: number = 0;
  public TotalCase: number = 0;
  public TotalFailOverTotalCase: string = "";
  public listStatus: Array<string> = ['All', 'Pass', 'Fail'];
  public YieldByMachineGridData: any[];
  public YieldStatusByMachine: string = "All";
  public YieldByProcessGridData: any[];
  public YieldStatusByProcess: string = "All";
  public HardCodedTargetRate: number = 90;  //70;
  public realTimeChartSeries: any[];

  //Harded-code the daily chart
  public dailyChartSeries: any[] = [
    {
      name: "% Pass Prediction",
      data: [96.5, 91.7, 86.3, 88.05, 92.6, 92, 91.3, 86.9, 96.5, 98.3, 90.5, 86.5, 93.2, 91],
      markers:{
        size:5,
        type: 'circle',
        background:'blue'
      },
      color: "blue"
    },
    {
      name: "% Pass Actual",
      data: [93.3, 95.2, 87.1, 88.2, 90.4, 93.5, 88.1, 92.2, 99.5, 96.4, 94.5, 87.8, 91.2, 94.1],
      markers:{
        size:5,
        type:'square'
      },
      color:"orange"
    },
    {
      name: "Target",
      data: [90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90],
      markers:{
        visible: false
      },
      color: "#000000"
    }
  ];

  public categories: string[] = [];
  public categoriesRealTimeChart: string[] = [];
  public categoriesTime : string[] = [];
  public minYaxisRealTimeChart: number
  public maxYaxisRealTimeChart: number
  public minYaxisDailyYieldChart: number
  public maxYaxisDailyYieldChart: number


  public sortByMachine: SortDescriptor[] = [
    {
        field: 'machineId',
        dir: 'asc'
    }
  ];

  public sortByProcess: SortDescriptor[] = [
    {
        field: 'processId',
        dir: 'asc'
    }
  ];

  public loginToken: string;
  public pagename:string;
  public machineList: Array<Machine>;


  constructor(private http: HttpClient, private sanitizer: DomSanitizer,
    private keycloakService: KeycloakService,
    private translate: TranslateService,
    private commonService: CommonService,
    private settings: SETTINGS) {

    // this.keycloakService.getToken().then(token => {
    //   this.loginToken = token;
    // });

    const headers = new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', '*')
    //.set('Authorization', 'Bearer ' + this.loginToken)


    //set the next clean up time if no clean up time is specificed
    if(global.nextCleanUpTime === undefined){
      global.updateNextCleanUpTime(this.calculateNextCleanUpTime());
    }
    else{
      //if current time is more then clean up time.
      //update the next clean up time and reset the chart
      if(new Date() >= global.nextCleanUpTime){
        global.updateNextCleanUpTime(this.calculateNextCleanUpTime());
        global.resetSeries();
      }
    }

    //loop to declare last 13 days' date
    for(let i=13; i >= 0; i--){
      var currentDate = new Date();
      currentDate.setDate(currentDate.getDate() - i);
      this.categories.push(currentDate.toLocaleDateString('en-GB', {year: "numeric", month: "short", day: "numeric"}));
    }

    //192.168.137.1:5004
    this.http.get(this.settings.ENDPOINT.API + "/Database/Summary/GetCurrentPredictionResult", {'headers': headers}).subscribe((prediction:any) => {

      //when page is loaded, current prediction result (from dashboard start time to current time)
      //is retrieved and assigned to this.predictionResultList.  Subsequently, when new data is pushed
      //to frontend, that single prediction result is added to the list.

      this.calculatePassRate(prediction.data);
      this.ResultSummaryGridData = this.passRateList;
      this.populateYieldByMachineGridData(this.passRateList);
      this.populateYieldByProcessGridData(this.passRateWithProcessIdList);

      //if global series do not have data, populate new data
      //remove global.series[0].data.length === 0
      // if(global.series[0].data.length == 0){
          this.populateRealTimeChart();
      // }
      // else{
      //   //direct populate global series into chart
      //   this.realTimeChartSeries = global.series;
      //   this.categoriesRealTimeChart = this.categoriesTime;
      // }

      //populate daily chart
      this.populateDailyChart();
      this.predictionResultList = prediction.data;

    // }, err =>{
    //   console.log('Something went wrong ', err);
    })
  }

  ngOnInit(): void {

    this.commonService.getAllMachines().subscribe(resp => {
      this.machineList = resp;
    });

    //url = Server Url
    this._hubConnection = new HubConnectionBuilder().withUrl(this.settings.ENDPOINT.SIGNALR).build();
    this._hubConnection.start()
      .then(() => console.log('connection start'))
      .catch(err => {
        console.log('Error while establishing the connection')
      });

      this._hubConnection.on('BroadcastPredictionResult', (prediction) => {
        console.log(prediction);

        this.passRateList = [];
        this.passRateWithProcessIdList = [];

        let curDate = new Date();
        console.log("Data in: " + curDate);

        //add module and production line to prediction
        prediction.productionLine = this.machineList.filter(x => x.machineId == prediction.machineId)[0].productionLine;

        this.predictionResultList.push(prediction);
        //this.calculatePassRate(prediction);
        this.calculatePassRate(this.predictionResultList);
        //this.ResultSummaryGridData = this.passRateList;  commented 13 Sep
        this.populateYieldByMachineGridData(this.passRateList);
        this.populateYieldByProcessGridData(this.passRateWithProcessIdList);

      })

      this.timer = setInterval(() => {
        this.dashboardtime = this.createPageSubtitle();
        }, 1000);

      this.chartTimer = setInterval(() => {
        //if current time is more then clean up time.
        //update the next clean up time and reset the chart
        if(new Date() >= global.nextCleanUpTime){
          global.updateNextCleanUpTime(this.calculateNextCleanUpTime());
          global.resetSeries();
        }

        //if(global.series[0].data.length === 0 || this.PredictedPassResult !== global.series[0].data[global.series[0].data.length-1])
        //{
          //if global series do not have data, or latest pass rate different with last pass rate in global series, re-populate data
          this.populateRealTimeChart();
        //}
        //populate daily chart
        this.populateDailyChart();
      }, 15000);  //30sec

      //let title = "SSMC";
      this.pageTitle = "SSMC"
      this.pagename = "SSMC";
      //this.translate.instant('PAGETITLE', {value: "SSMC"}); //this.createPageTitle();  //
  }


  ngOnDestroy(){
    clearInterval(this.timer);
    clearInterval(this.chartTimer);
    this._hubConnection.stop()
      .then(() => console.log('Results page connection stop'))
      .catch(err => {
        console.log('Error while stopping the connection')
      });
  }

  public calculateNextCleanUpTime(): Date{

    //this method called only when that is no clean up time is specificed or current time is more then clean up time
    let currentDate = new Date();
    //below to cater the scenaria that user launch the application same date with dashboard start time
    //Example 1, Dashboard start time is 8am daily, user launch the application after 8am, the next clean up time will be next day 8am.
    //Example 2, Dashboard start time is 8am daily, user laucnh the application before 8am(anytime from 1am-8am), then the next clean up time will be same date 8am.
    if(currentDate.getHours() >= Number(global.DashStartTime)){
      currentDate.setDate(currentDate.getDate() + 1);
    }

    return new Date(formatDate(currentDate, 'yyyy-MM-dd', 'en_US') + " " + global.DashStartTime + ":00:00");
  }


  public populateRealTimeChart(){
    const myClonedArray = [];
    let localeId = this.translate.currentLang;

    global.series.forEach(val => myClonedArray.push(Object.assign({}, val)));

    if (myClonedArray[0].data.length == 6){
      myClonedArray[0].data.shift();
      myClonedArray[0].data.push(this.PredictedPassResult);

      //calculate y-axis min value
      this.minYaxisRealTimeChart = Math.floor(this.CalcChartYaxisMin(myClonedArray,this.HardCodedTargetRate)/10)*10;

      //calculate y-axis max value
      this.maxYaxisRealTimeChart = Math.ceil(this.CalcChartYaxisMax(myClonedArray,this.HardCodedTargetRate)/10)*10;

      myClonedArray[1].data.shift();
      myClonedArray[1].data.push(this.HardCodedTargetRate);

      this.realTimeChartSeries = myClonedArray;

      this.categoriesRealTimeChart = [...global.categoryRealTimeSummaryPg];
      let currentDate = new Date();
      this.categoriesRealTimeChart.shift();
      this.categoriesRealTimeChart.push(currentDate.toLocaleString(localeId, {hour: "numeric", minute: "numeric", second: "numeric"}));
      global.categoryRealTimeSummaryPg.shift();
      global.categoryRealTimeSummaryPg.push(currentDate.toLocaleString(localeId, {hour: "numeric", minute: "numeric", second: "numeric"}));

    }
    else {
      myClonedArray[0].data.push(this.PredictedPassResult);
      myClonedArray[1].data.push(this.HardCodedTargetRate);
      this.realTimeChartSeries = myClonedArray;

      //calculate y-axis min value
      this.minYaxisRealTimeChart = Math.floor(this.CalcChartYaxisMin(myClonedArray,this.HardCodedTargetRate)/10)*10;

      //calculate y-axis max value
      this.maxYaxisRealTimeChart = Math.ceil(this.CalcChartYaxisMax(myClonedArray,this.HardCodedTargetRate)/10)*10;

      this.categoriesRealTimeChart = [...global.categoryRealTimeSummaryPg];
      let currentDate = new Date();
      this.categoriesRealTimeChart.push(currentDate.toLocaleString(localeId, {hour: "numeric", minute: "numeric", second: "numeric"}));
      global.categoryRealTimeSummaryPg.push(currentDate.toLocaleString(localeId, {hour: "numeric", minute: "numeric", second: "numeric"}));

    }
  }


  public populateDailyChart(){

    this.dailyChartSeries[0].data[13] = this.PredictedPassResult;
    const myClonedArray = [];
    this.dailyChartSeries.forEach(val => myClonedArray.push(Object.assign({}, val)));
    this.dailyChartSeries = myClonedArray;

      //calculate y-axis min value
      this.minYaxisDailyYieldChart = Math.floor(this.CalcChartYaxisMin(myClonedArray,this.HardCodedTargetRate)/10)*10;

      //calculate y-axis max value
      this.maxYaxisDailyYieldChart = Math.ceil(this.CalcChartYaxisMax(myClonedArray,this.HardCodedTargetRate)/10)*10;
  }

  public calculatePassRate(predictionList: any){
    const DistinctMachine = predictionList.map(item => item.machineId).filter((value, index, self) => self.indexOf(value) === index);
    const DistinctProcessID = predictionList.map(item => item.processId).filter((value, index, self) => self.indexOf(value) === index);
    //const DistinctProductionLine = predictionList.map(item => item.productionLine).filter((value, index, self) => self.indexOf(value) === index);

    //to calculate the 3 KPIs
    if(predictionList.length > 0 ){
      this.TotalCase = predictionList.length;
      this.TotalFailCase = predictionList.filter(p => p.isOOS == true).length;
      this.TotalFailOverTotalCase = this.TotalFailCase.toString() + ' / ' + this.TotalCase.toString();
      this.PredictedPassResult = ((this.TotalCase - this.TotalFailCase) / this.TotalCase * 100).toFixed(2).toString();
      this.PredictedFailResult = (this.TotalFailCase / this.TotalCase * 100).toFixed(2).toString();
    }

    //for the Yield by Machine table
    DistinctMachine.forEach( (element) => {
      let pr = new PassRate();
      const TotalResult = predictionList.filter(p => p.machineId === element).length;
      const TotalPassResult = predictionList.filter(p => p.isOOS == false && p.machineId === element).length;
      const rate = TotalPassResult / TotalResult * 100;
      pr.machineId = element;
      pr.passRate = rate.toFixed(2).toString() + "%";
      pr.targetPassRate = this.HardCodedTargetRate + "%";
      this.passRateList.push(pr);
    });

    //for the Yield by Production line table
    // DistinctProductionLine.forEach( (element) => {
    //   let pr = new PassRate();
    //   const TotalResult = predictionList.filter(p => p.productionLine === element).length;
    //   const TotalPassResult = predictionList.filter(p => p.isOOS == false && p.productionLine === element).length;
    //   const rate = TotalPassResult / TotalResult * 100;
    //   pr.machineId = element;
    //   pr.passRate = rate.toFixed(2).toString() + "%";
    //   pr.targetPassRate = this.HardCodedTargetRate + "%";
    //   this.passRateList.push(pr);
    // });

    //for the Yield by Process table
    DistinctProcessID.forEach( (element) => {
      let pr = new PassRateWithProcessId();
      const TotalResult = predictionList.filter(p => p.processId === element).length;
      const TotalPassResult = predictionList.filter(p => p.isOOS == false && p.processId === element).length;
      const rate = TotalPassResult / TotalResult * 100;
      pr.processId = element;
      pr.passRate = rate.toFixed(2).toString() + "%";
      pr.targetPassRate = this.HardCodedTargetRate + "%";
      this.passRateWithProcessIdList.push(pr);
    });
  }

  public backgroundColorCode(passRate: string, targetPassRate: string): SafeStyle {
    let result;
    let numPassRate: number = Number(passRate.split('%')[0]);
    let numTargetPassRate: number = Number(targetPassRate.split('%')[0]);


    if (numTargetPassRate - numPassRate > 0){
      result = '#ff0000';//red
    }
    else{
      result = '#0f0';//green
    }
    return this.sanitizer.bypassSecurityTrustStyle(result);
  }

  public colorCode(passRate: string, targetPassRate: string): SafeStyle {
    let result;
    let numPassRate: number = Number(passRate.split('%')[0]);
    let numTargetPassRate: number = Number(targetPassRate.split('%')[0]);


    if (numTargetPassRate - numPassRate > 0){
      result = '#fff';//White
    }
    else{
      result = '#000000';//Black
    }
    return this.sanitizer.bypassSecurityTrustStyle(result);
  }

  //user selects All/Pass/Fail dropdown for Yield by Machine
  public byMachineValueChange(value: any): void {
    this.YieldStatusByMachine = value;
    this.populateYieldByMachineGridData(this.passRateList);
  }

  //user selects All/Pass/Fail dropdown for Yield by Process
  public byProcessValueChange(value: any): void {
    this.YieldStatusByProcess = value;
    this.populateYieldByProcessGridData(this.passRateWithProcessIdList);
  }

  public populateYieldByMachineGridData(prediction: any){
    switch(this.YieldStatusByMachine.toLowerCase()){
      case "all":{
        this.YieldByMachineGridData = prediction;
        break;
      }
      case "pass":{
        this.YieldByMachineGridData = prediction.filter(p => +p.passRate.split('%')[0] >= +p.targetPassRate.split('%')[0] );
        break;
      }
      case "fail":{
        this.YieldByMachineGridData = prediction.filter(p => +p.passRate.split('%')[0] < +p.targetPassRate.split('%')[0] );
        break;
      }
    }
  }

  public populateYieldByProcessGridData(prediction: any){
    switch(this.YieldStatusByProcess.toLowerCase()){
      case "all":{
        this.YieldByProcessGridData = prediction;
        break;
      }
      case "pass":{
        this.YieldByProcessGridData = prediction.filter(p => +p.passRate.split('%')[0] >= +p.targetPassRate.split('%')[0] );
        break;
      }
      case "fail":{
        this.YieldByProcessGridData = prediction.filter(p => +p.passRate.split('%')[0] < +p.targetPassRate.split('%')[0] );
        break;
      }
    }
  }

  public cellColorCode(passRate: string, targetPassRate: string): SafeStyle {
    let result;

    if (+passRate.split('%')[0] < +targetPassRate.split('%')[0]){
      result = '#f31700';
    }
    else{
      result = 'transparent';
    }

    return this.sanitizer.bypassSecurityTrustStyle(result);
  }


  public createPageTitle(): string{
    let title = 'Real-Time Factory Quality Dashboard';
    return title;
  }

  public createPageSubtitle():string{
    return this.calcDashboardTime();
  }


  private calcDashboardTime(): string{

    let localeId = this.translate.currentLang;
    let d = new Date();
    d.setHours(8);
    d.setMinutes(0);
    d.setSeconds(0);
    d.setMilliseconds(0);
    let startdatetime = d.getTime();

    let curTime = new Date().getTime();

    if (curTime < startdatetime){
      let startdate = new Date((startdatetime - (24*60*60*1000)));
      let curdate = new Date(curTime);

      let dashboardDate = formatDate(startdate, 'd MMM yyyy h:mm:ss a', localeId) + '-' + formatDate(curdate, 'd MMM yyyy h:mm:ss a', localeId);
      return dashboardDate;
      //return startdate.toLocaleString(localeId, {year: "numeric", month: "short", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"}) + ' - ' + curdate.toLocaleString(localeId, {year: "numeric", month: "short", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"});

      //let pipe = new DatePipe("fr-FR");
      //let dashboarddate = pipe.transform(startdate);
      //return dashboarddate;

    }
    else{
      //let options = {year: 'numeric', month: 'short', day: 'numeric'};
      let startdate = new Date(startdatetime);
      let curdate = new Date(curTime);

      let dashboardDate = formatDate(startdate, 'd MMM yyyy h:mm:ss a', localeId) + '-' + formatDate(curdate, 'd MMM yyyy h:mm:ss a', localeId);
      return dashboardDate;
      //return startdate.toLocaleString(localeId, {year: "numeric", month: "short", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"}) + ' - ' + curdate.toLocaleString(localeId, {year: "numeric", month: "short", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"});

      // let localeId = "zh-CN";
      // let pipe = new DatePipe(localeId);
      // let dashboarddate = pipe.transform(startdate, localeId, "yyyy-MM-dd hh:mm:ss");
      // return dashboarddate;
    }
  }

  private CalcChartYaxisMax(data:any[],targetline: number){
    let maxYaxisRealTimeChart: number

    let max = Math.max(...data[0].data);
      if (max >= targetline){
        maxYaxisRealTimeChart = Math.round(max + 0.2*max);
      }
      else {
        maxYaxisRealTimeChart = Math.round(targetline + 0.2*targetline);
      }

      return maxYaxisRealTimeChart;
  }

  private CalcChartYaxisMin(data:any[],targetline: number){
    let minYaxisRealTimeChart: number

      let min = Math.min(...data[0].data);
      if (min <= targetline){
        minYaxisRealTimeChart = Math.round(min - 0.2*min);
      }
      else{
        minYaxisRealTimeChart = Math.round(targetline - 0.2*targetline)
      }
      if (minYaxisRealTimeChart <= 0){
        minYaxisRealTimeChart = 0;
      }

      return minYaxisRealTimeChart;
  }
}
