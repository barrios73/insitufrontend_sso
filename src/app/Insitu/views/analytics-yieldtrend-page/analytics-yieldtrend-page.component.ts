import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {FormControl, FormGroup} from '@angular/forms';
import { SETTINGS } from '../../settings/environment';
import {CommonService} from '../../service/common/common.service';


interface Product {
  id: number;
  productId: string;
  productName: string;
}

interface Process {
  id: number;
  processId: string;
  processName: string;
}

interface Machine {
  id: number;
  machineId: string;  
}



@Component({
  selector: 'app-analytics-yieldtrend-page',
  templateUrl: './analytics-yieldtrend-page.component.html',
  styleUrls: ['./analytics-yieldtrend-page.component.scss']
})
export class AnalyticsYieldTrendPageComponent implements OnInit {

  public dailyYieldPerf: any[]
  public searchForm: FormGroup;
  public pageTitle: string = 'Yield Trend Performance';
  public format = 'MM/dd/yyyy HH:mm';

  //for product
  public selectedProduct: Product;
  public defaultProduct: Product = {
    id:null,
    productId: null,
    productName:"Select product"
  }
  public productList: Array<Product>; //= ['P001','P002','P003', 'P004', 'P005', 'P006', 'P007', 'P008', 'P009', 'P010'];
  
  
  //for process
  public selectedProcess: Process;
  public defaultProcess: Process = {
    id:null,
    processId: null,
    processName:"Select process"
  }
  public processList: Array<Process>;  //= ['PR001','PR002','PR003', 'PR004', 'PR005', 'PR006', 'PR007', 'PR008', 'PR009', 'PR010'];
  
  
  //for machine
  public selectedMachine: Machine;
  public defaultMachine: Machine = {
    id:null,
    machineId: "Select machine"
  }
  public machineList: Array<Machine>; //= ['ALD501', 'ALD502','ALD503', 'IM001', 'IM002', 'IM003','LW001', 'LW002', 'OVEN001', 'OVEN002', 'OVEN003'];
  
  
  public daterange = { start: null, end: null };

  //for yield trend chart
  public yieldDate: string[]=[];  
  public dailyYieldSeries: any[] = [
    {
      name: "Daily % Predicted Pass Yield",
      data:  [],
      markers:{
        size:5,
        type: 'circle'
      }
    }
  ]

  //for total OOS count and OOS rate chart
  public oosCountRateSeries: any[] = [
    {
      type: 'line',
      name: "Total Daily OOS Count",
      data:  [],
      markers:{
        size:5,
        type: 'circle'
      },
      axis: "oosCount",
      color: "#cc6e38"

    },
    {
      type: "line",
      name: "OOS Rate (%)",
      data:  [],
      markers:{
        size:5,
        type: 'circle'
      },
      axis: "oosRate",
      color: "#000000",
    }
  ]

  public oosCountRateValueAxes: any[] =
  [
    {
      name: "oosCount",
      title: "Total OOS Count",
      min: 0,
      max:100,
      majorUnit: 20
    },
    {
      name: "oosRate",
      title: "OOS Rate (%)",
      min: 0,
      max: 100,
      majorUnit: 10,
    }
  ]
  
  public noOfCategories: number = 0;
  public crossingValues: number[];



  constructor(private http: HttpClient,
    private commonService: CommonService,
    private settings: SETTINGS) { }

  ngOnInit(): void {

    this.commonService.getAllMachines().subscribe(resp => {
      this.machineList = resp;
    });

    this.commonService.getAllProcesses().subscribe(resp => {
      this.processList = resp;
    });

    this.commonService.getAllProducts().subscribe(resp => {
      this.productList = resp;
    });

    this.searchForm = new FormGroup({
      process: new FormControl(),
      product: new FormControl(),
      machine: new FormControl(),
      startdate: new FormControl(new Date()),
      endate: new FormControl(new Date())
    });

  }



  public getYieldTrend(){

    let processId: string;
    let productId: string;
    let machineId: string;
    if (this.selectedProcess == null){
      processId = null;
    }
    else {
      processId = this.selectedProcess.processId;
    }

    if (this.selectedProduct == null){
      productId = null;
    }
    else {
      productId = this.selectedProduct.productId;
    }

    if (this.selectedMachine == null){
      machineId = null;
    }
    else {
      machineId = this.selectedMachine.machineId;
    }

    const headers = { 'Content-Type': 'application/json; charset=utf-8' };
    const body = { Process:  processId, //this.searchForm.get('process').value,
                   Product: productId, //this.searchForm.get('product').value,
                   Machine: machineId, //this.searchForm.get('machine').value,
                   StartDate: this.searchForm.get('startdate').value,
                   EndDate: this.searchForm.get('endate').value
                };
    this.http.post<any>(this.settings.ENDPOINT.API + '/Database/History/GetDailyYield', JSON.stringify(body), { headers }).subscribe(resp => {
        this.dailyYieldPerf = resp.data;
        this.dailyYieldSeries[0].data = this.dailyYieldPerf.map(val => val.yield*100);
        
        
        this.oosCountRateSeries[0].data = this.dailyYieldPerf.map(val => val.ooScount);
        this.oosCountRateValueAxes[0].max = Math.round(Math.max(...this.oosCountRateSeries[0].data)*1.1); 
        this.oosCountRateValueAxes[0].majorUnit = Math.round(this.oosCountRateValueAxes[0].max/10); 

        this.oosCountRateSeries[1].data = this.dailyYieldPerf.map(val => val.ooSrate);
        this.oosCountRateValueAxes[1].max = Math.round(Math.max(...this.oosCountRateSeries[1].data)*1.1);
        

        this.noOfCategories =  this.dailyYieldPerf.length;
        this.crossingValues = new Array(0, this.noOfCategories); 

        this.yieldDate = this.dailyYieldPerf.map(val => new Date(val.createdDate).toLocaleDateString('en-GB', {year: "numeric", month: "2-digit", day: "numeric"}));
        console.log(this.noOfCategories);
    });

  }

}
