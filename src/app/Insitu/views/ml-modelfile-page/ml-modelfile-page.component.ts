import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import {DataBindingDirective, EditEvent, RemoveEvent} from '@progress/kendo-angular-grid';
import { HttpClient , HttpErrorResponse} from '@angular/common/http';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { SETTINGS } from '../../settings/environment';
import {MlModelService} from '../../service/mlmodel/mlmodel.service';
import { Observable } from 'rxjs';
import { SuccessEvent, UploadEvent, ErrorEvent } from "@progress/kendo-angular-upload"
import {
  DialogService,
  DialogRef,
  DialogCloseResult,
} from '@progress/kendo-angular-dialog';
import { ActionsLayout } from '@progress/kendo-angular-dialog';


interface response {  
  hasError: boolean;
  msg: string;
  data : any[];
}

interface ModelType {
  id: number;
  modelName: string;
}

interface ModelFile {
  id: number;
  modelTypeId: number;
  modelFileName: string;
}

type HorizontalPosition = 'left' | 'center' | 'right';
type VerticalPosition = 'top' | 'bottom';

@Component({
  selector: 'app-ml-modelfile-page',
  templateUrl: './ml-modelfile-page.component.html',
  styleUrls: ['./ml-modelfile-page.component.scss']
})
export class MlModelfilePageComponent implements OnInit {

  
  ////////////for Search Model File function////////////////
  public searchModelFileForm: FormGroup;
  public mySelection: number[] = [];
  public modelFileList: any[]
  
  public modelFiles: Array<ModelFile>;
  public selectedModelFile: ModelFile;
  public defaultModelFile: ModelFile = {
    id: null,
    modelTypeId: null,
    modelFileName:"Select model file"
  }
  
  public modelTypes: Array<ModelType>;
  public selectedModelType: ModelType;
  public defaultModelType: ModelType = {
    id: null,
    modelName:"Select model"
  }
  public modelFileArr: Array<string>;
  public modelFileData;

      ///////////////////////////////////////////////////////

  ////////////////for Upload File function///////////////
  public showUploadFile: boolean = false;
  public uploadSaveUrl = this.settings.ENDPOINT.MLMODEL + "/MLModel/UploadFile";
  public uploadRemoveUrl = this.settings.ENDPOINT.MLMODEL + "/MLModel/DeleteFile"; // should represent an actual API endpoint
  public windowTop = 100;
  public windowLeft = 415;


  constructor(private http: HttpClient, 
    private mlModelService: MlModelService,
    private settings: SETTINGS,
    private dialogService: DialogService,) { }


 // for alerts
 @ViewChild('appendTo', { read: ViewContainerRef, static: false })
 public appendTo: ViewContainerRef;
 public horizontal: HorizontalPosition = 'center';
 public vertical: VerticalPosition = 'bottom';
 public showDialog: boolean = false;
 public dialogTitle = '';
 public dialogText = '';
 public actionsLayout: ActionsLayout = 'normal';
 
  ngOnInit(): void {
    this.searchModelFileForm = new FormGroup({
      modelName: new FormControl(),
      modelFile: new FormControl()
    });

    this.mlModelService.getAllModelTypes().subscribe(resp => {
      this.modelTypes = resp.data
    }
    );

    const headers = { 'Content-Type': 'application/json; charset=utf-8' };
    this.http.post<response>(   this.settings.ENDPOINT.MLMODEL   + '/MLModel/SearchMLFile', { headers }).subscribe(resp => {
      const data = resp.data.filter(x => x.modelFileName);
      this.modelFileArr = new Array<string>();
      data.forEach(item => {
        this.modelFileArr.push(item.modelFileName);
      });
    });
  }


  public searchMLModelFile(){
    let modelType: number
    let modelFile: string

    if(this.selectedModelType == null){
        modelType = null;
    }
    else{
      modelType = this.selectedModelType.id
    }

    modelFile = this.searchModelFileForm.get('modelFile').value;
    

    const headers = { 'Content-Type': 'application/json; charset=utf-8' };
    const body = { 
                   ModelTypeId: modelType,
                   ModelFile: modelFile
                };

    this.http.post<response>(this.settings.ENDPOINT.MLMODEL + '/MLModel/SearchMLFile', JSON.stringify(body), { headers }).subscribe(resp => {
      this.modelFileList = resp.data;
	  if (resp.data.length > 0){
        this.modelFileList = resp.data;
      }
      else {
        this.dialogTitle = 'Search ML Model';
        this.dialogText = 'No ML model file found based on search criteria';
        this.showDialog = true;
      }
    });
  }

  modelFileFilter(value) {
    this.modelFileData = this.modelFileArr.filter(
        (s) => s.toLowerCase().indexOf(value.toLowerCase()) !== -1
    );
  };


  ////////////////////////////////////////////////////////////////////////
  ///////////////           Upload File           ////////////////////////
  ////////////////////////////////////////////////////////////////////////

  public openUploadFileModal(){
    this.showUploadFile = true;
  }

  public closeUploadFileWindow(){
    this.showUploadFile = false;
  }


  public uploadEventHandler(e: UploadEvent){
    e.data = {modelTypeId: this.selectedModelType.id};
  }


  public uploadFail(e: ErrorEvent){
    this.closeUploadFileWindow();
    this.dialogTitle = 'Upload Model File';
    this.dialogText = e.response['error']['msg'];
    this.showDialog = true;    
  }

  public transferSuccessHandler(e: SuccessEvent){
    this.closeUploadFileWindow();
    this.dialogTitle = 'Upload Model File';
    this.dialogText = 'Successfully uploaded model file: ' + e.files[0].name;
    this.showDialog = true; 

    // if (e.operation == 'upload'){
    //   let filename = e.files[0].name;
    //   let modelTypeId = this.selectedModelType.id;

      // this.http.get(this.settings.ENDPOINT.MLMODEL + '/MLModel/AddModelFile?filename=' + filename +
      // '&modelTypeId=' + modelTypeId).
      // subscribe(resp => {
      //   const headers = { 'Content-Type': 'application/json; charset=utf-8' };
      //   const body = { 
      //                  ModelTypeId: "",
      //                  ModelFile: ""
      //               };
    
      //   this.http.post<response>(this.settings.ENDPOINT.MLMODEL + '/MLModel/SearchMLFile', JSON.stringify(body), { headers }).subscribe(resp => {
      //     this.modelFileList = resp.data;
      //   });
        
      //})

    //}
  }


  //close upload file dialog
  closeDialog(): void {
    this.showDialog = false;
  }

}
