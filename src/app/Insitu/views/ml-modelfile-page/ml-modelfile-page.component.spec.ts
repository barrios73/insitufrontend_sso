import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MlModelfilePageComponent } from './ml-modelfile-page.component';

describe('MlModelfilePageComponent', () => {
  let component: MlModelfilePageComponent;
  let fixture: ComponentFixture<MlModelfilePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MlModelfilePageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MlModelfilePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
