import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalyticsIssuesPageComponent } from './analytics-issues-page.component';

describe('AnalyticsIssuesPageComponent', () => {
  let component: AnalyticsIssuesPageComponent;
  let fixture: ComponentFixture<AnalyticsIssuesPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnalyticsIssuesPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalyticsIssuesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
