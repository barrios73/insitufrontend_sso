import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {FormControl, FormGroup} from '@angular/forms';
import { SETTINGS } from '../../settings/environment';
import {CommonService} from '../../service/common/common.service';


interface Product {
  id: number;
  productId: string;
  productName: string;
}

interface Process {
  id: number;
  processId: string;
  processName: string;
}

interface Machine {
  id: number;
  machineId: string;  
}


@Component({
  selector: 'app-analytics-issues-page',
  templateUrl: './analytics-issues-page.component.html',
  styleUrls: ['./analytics-issues-page.component.scss']
})
export class AnalyticsIssuesPageComponent implements OnInit {

    public searchForm: FormGroup;
    public daterange = { start: null, end: null };
    public qualityIssuesOpen: any[];
    public qualityIssuesClose: any[];

    //for product
    public selectedProduct: Product;
    public defaultProduct: Product = {
      id:null,
      productId: null,
      productName:"Select product"
    }
    public productList: Array<Product>; //= ['P001','P002','P003', 'P004', 'P005', 'P006', 'P007', 'P008', 'P009', 'P010'];
    
    //for table
    public mySelection: number[] = [];


    
    //for process
    public selectedProcess: Process;
    public defaultProcess: Process = {
      id:null,
      processId: null,
      processName:"Select process"
    }
    public processList: Array<Process>;  //= ['PR001','PR002','PR003', 'PR004', 'PR005', 'PR006', 'PR007', 'PR008', 'PR009', 'PR010'];
    
    
    //for machine
    public selectedMachine: Machine;
    public defaultMachine: Machine = {
      id:null,
      machineId: "Select machine"
    }
    public machineList: Array<Machine>; //= ['ALD501', 'ALD502','ALD503', 'IM001', 'IM002', 'IM003','LW001', 'LW002', 'OVEN001', 'OVEN002', 'OVEN003'];
  
    
  constructor(private http: HttpClient,
    private commonService: CommonService,
    private settings: SETTINGS) { }

  ngOnInit(): void {
    this.commonService.getAllMachines().subscribe(resp => {
      this.machineList = resp;
    });

    this.commonService.getAllProcesses().subscribe(resp => {
      this.processList = resp;
    });

    this.commonService.getAllProducts().subscribe(resp => {
      this.productList = resp;
    });

    this.searchForm = new FormGroup({
      process: new FormControl(),
      product: new FormControl(),
      machine: new FormControl(),
      startdate: new FormControl(new Date()),
      endate: new FormControl(new Date())
    });
  }


  public getQualityIssues(){
  
    let processId: string;
    let productId: string;
    let machineId: string;

    if (this.selectedProcess == null){
      processId = null;
    }
    else {
      processId = this.selectedProcess.processId;
    }

    if (this.selectedProduct == null){
      productId = null;
    }
    else {
      productId = this.selectedProduct.productId;
    }

    if (this.selectedMachine == null){
      machineId = null;
    }
    else {
      machineId = this.selectedMachine.machineId;
    }

    const headers = { 'Content-Type': 'application/json; charset=utf-8' };
    const body = { Process:  processId, //this.searchForm.get('process').value,
                   Product: productId, //this.searchForm.get('product').value,
                   Machine: machineId, //this.searchForm.get('machine').value,
                   StartDate: this.searchForm.get('startdate').value,
                   EndDate: this.searchForm.get('endate').value
                };
    this.http.post<any>(this.settings.ENDPOINT.QUALITY + '/Disposition/GetQualityIssues', JSON.stringify(body), { headers }).subscribe(resp => {
      this.qualityIssuesOpen = resp.data.filter(x => x.status == "Open");
      this.qualityIssuesClose = resp.data.filter(x => x.status == "Close");
    });
  }


  public closeQualityIssue(data: any){

    const headers = { 'Content-Type': 'application/json; charset=utf-8' };
    const body = data;
                

    this.http.post<any>(this.settings.ENDPOINT.QUALITY + '/Disposition/CloseQualityIssues', JSON.stringify(body), { headers }).subscribe(resp => {
    
      const headers = { 'Content-Type': 'application/json; charset=utf-8' };
      const body = { Process: null, //this.searchForm.get('process').value,
                   Product: null, //this.searchForm.get('product').value,
                   Machine: null, //this.searchForm.get('machine').value,
                   StartDate: null,
                   //EndDate: this.searchForm.get('endate').value
                };
      this.http.post<any>(this.settings.ENDPOINT.QUALITY + '/Disposition/GetQualityIssues', JSON.stringify(body), { headers }).subscribe(resp => {
          this.qualityIssuesOpen = resp.data.filter(x => x.status == "Open");
          this.qualityIssuesClose = resp.data.filter(x => x.status == "Close");
      });
  });
  }


}
