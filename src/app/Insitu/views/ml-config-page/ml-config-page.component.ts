import {Component, Input, TemplateRef, OnInit, OnDestroy, ViewChild, ViewContainerRef} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup } from '@angular/forms';
import { SETTINGS } from '../../settings/environment';
import {MlModelService} from '../../service/mlmodel/mlmodel.service';
import {CommonService} from '../../service/common/common.service';
import {environment} from '../../../../environments/environment';
import { Subscription } from 'rxjs';


// import kendo elements
import {EditEvent} from '@progress/kendo-angular-grid';
import { NotificationService, NotificationSettings } from '@progress/kendo-angular-notification';
import {toJSON} from '@progress/kendo-angular-grid/dist/es2015/filtering/operators/filter-operator.base';
import {catchError} from 'rxjs/operators';
import {error} from 'protractor';
import {parseJson} from '@angular/cli/utilities/json-file';
import {
  DialogService,
  DialogRef,
  DialogCloseResult,
} from '@progress/kendo-angular-dialog';
import { ActionsLayout } from '@progress/kendo-angular-dialog';

// end of kendo elements

interface MlModelConfig {
  topic: string;
  processingfile: string;
  modeltypeid: number;
  modelfileid: number;
  module: string;
  productionLine: string;
  createdby: string;
}

interface ModelType {
  id: number;
  modelName: string;
}

interface ModelFile {
  id: number;
  modelTypeId: number;
  modelFile: string;
}

interface ModelStatus {
  status: string;
  isActive: boolean;
}

interface Module {
  id: number;
  moduleName: string;
}

interface ProductionLine {
  id: number;
  line: string;
  moduleId: number;
}

interface PreprocessingFile {
  id: number;
  preprocessingFile: string;
}

interface response {
  hasError: boolean;
  msg: string;
  data: any[];
}

type HorizontalPosition = 'left' | 'center' | 'right';
type VerticalPosition = 'top' | 'bottom';

@Component({
  selector: 'app-ml-config-page',
  templateUrl: './ml-config-page.component.html',
  styleUrls: ['./ml-config-page.component.scss']
})

export class MlConfigPageComponent
  implements OnInit, OnDestroy {
  // tslint:disable-next-line:max-line-length

  // constructor
  constructor(private http: HttpClient,
              private mlModelService: MlModelService,
              private commonService: CommonService,
              private notificationService: NotificationService,
              private dialogService: DialogService,
              private settings: SETTINGS) { }

  @Input() public set model(mlModelConfig: MlModelConfig) {
    this.editModelForm.reset(mlModelConfig);
  }

  // declarations
  private userId: string = environment.DEV_TEST_USER.id;

  // for alerts
  @ViewChild('appendTo', { read: ViewContainerRef, static: false })
  public appendTo: ViewContainerRef;
  public horizontal: HorizontalPosition = 'center';
  public vertical: VerticalPosition = 'bottom';
  public dialogTitle = '';
  public dialogText = '';
  public submitIsClicked = false;
  public opened = false;


  // for selected model type and file in Search Model form
  public selectedModelType: ModelType;
  public selectedModelFile: ModelFile;

  // this is for the Model Type dropdown in Search Model form
  public modelTypes: Array<ModelType>;
  public defaultModelType: ModelType = {
    id: null,
    modelName: 'Select model'
  };

  // this is for the Model File dropdown in Search Model form
  public modelFiles: Array<ModelFile>;
  public defaultModelFile: ModelFile = {
    id: null,
    modelTypeId: null,
    modelFile: 'Select model file'
  };

  // for update model
  public modelFilesEdit: Array<ModelFile>;
  public selectedModelTypeEdit: ModelType;
  public selectedModelFileEdit: ModelFile;
  public selectedStatus: ModelStatus;
  public updateMLModelOriginalTopic: string;
  public selectedModuleEdit: Module;
  public selectedProdLineEdit: ProductionLine;
  public selectedPreprocessingFileEdit: PreprocessingFile;

  // list of module, prod lines and processingfiles
  public moduleList: Array<Module>;
  public prodlineList: Array<ProductionLine>;
  public prodlineListAdd: Array<ProductionLine>;
  public prodlineListEdit: Array<ProductionLine>;
  public preprocessingFiles: Array<PreprocessingFile>;
  // list of module, prod lines and processingfiles end

  // for add model
  public modelFilesAdd: Array<ModelFile>;
  public selectedModelTypeAdd: ModelType;
  public selectedModelFileAdd: ModelFile;
  public selectedModuleAdd: Module;
  public selectedProductionLineAdd: ProductionLine;
  public selectedPreprocessingFileAdd: PreprocessingFile;
  public searchModelForm: FormGroup;

  public modelList: any[];
  public topicArr: Array<string>;
  public topicData: Array<string>;

  public mySelection: number[] = [];
  isEditModelWindowOpened = false;
  isAddModelWindowOpened = false;
  editModelForm: FormGroup;
  addModelForm: FormGroup;
  editedRowIndex: number;

  public modelStatus: Array<ModelStatus>;
  obsMLModelService: Subscription;

  // dialog
  public actionsLayout: ActionsLayout = 'normal';


  ngOnInit(): void {
    this.searchModelForm = new FormGroup({
      topic: new FormControl(),
      modelName: new FormControl(),
      modelFile: new FormControl(),
    });

    this.mlModelService.getAllModelTypes().subscribe(resp => {
        this.modelTypes = resp.data;
      }
    );

    this.mlModelService.getAllModelFiles().subscribe(resp =>
      this.modelFiles = resp.data);

    this.mlModelService.getPreprocessingFiles().subscribe(resp =>
      this.preprocessingFiles = resp.data);


    this.modelStatus = [];
    this.modelStatus.push({
      status: 'Yes',
      isActive: true
    });

    this.modelStatus.push({
      status: 'No',
      isActive: false
    });

    // get prod lines and module list
    this.commonService.getAllModules().subscribe(resp => {
      this.moduleList = resp;
    });

    this.commonService.getAllProductionLines().subscribe(resp => {
      this.prodlineList = resp;
    });

    const headers = { 'Content-Type': 'application/json; charset=utf-8' };
    this.http.post<response>(   this.settings.ENDPOINT.MLMODEL   + '/MLModel/SearchMLModel', { headers }).subscribe(resp => {
      const data = resp.data.filter(x => x.topic);
      this.topicArr = new Array<string>();
      data.forEach(item => {
        this.topicArr.push(item.topic);
      });
    });
  }

  ngOnDestroy(): void {
    if (this.obsMLModelService){
      this.obsMLModelService.unsubscribe();
    }
  }

  // alerts
  // public showAlert(): void {
  //   this.notificationService.show({
  //     content: "Your data has been saved. Time for tea!",
  //     cssClass: "button-notification",
  //     animation: { type: "slide", duration: 400 },
  //     position: { horizontal: "center", vertical: "bottom" },
  //     type: { style: "success", icon: true },
  //     closable: true,
  //   });
  // }

  // public showDialog(template: TemplateRef<unknown>): void {
  //   this.dialogService.open({
  //     content: template,
  //     actions: [{ text: 'Cancel' }, { text: 'Delete', themeColor: 'primary' }],    });
  // }

  public close(): void {
    this.opened = false;
  }


  public searchMLModel(){

    let modelType: number
    let modelFile: number

    if(this.selectedModelType == null){
      modelType = null;
    }
    else{
      modelType = this.selectedModelType.id
    }

    if(this.selectedModelFile == null){
      modelFile = null;
    }
    else{
      modelFile = this.selectedModelFile.id
    }

    const headers = { 'Content-Type': 'application/json; charset=utf-8' };
    const body = { Topic:  this.searchModelForm.get('topic').value,
      ModelTypeId: modelType,
      ModelFileId: modelFile
    };

    this.http.post<response>(this.settings. ENDPOINT.MLMODEL + '/MLModel/SearchMLModel', JSON.stringify(body), { headers }).subscribe(resp => {
      if (resp.data.length > 0){
        this.modelList = resp.data;
      }
      else {
        this.dialogTitle = 'Search ML Model';
        this.dialogText = 'No ML model info found based on search criteria';
        this.submitIsClicked = true;
      }
      
    });
  }

  topicFilter(value) {
    this.topicData = this.topicArr.filter(
        (s) => s.toLowerCase().indexOf(value.toLowerCase()) !== -1
    );
  };

  clearSearchFields(): void {
    this.searchModelForm.reset();
  }

  // when click on Edit button in one row of the table of ML models
  onEditClick(event: EditEvent): void {

    this.mlModelService.getModelFilesByModel(event.dataItem.modelTypeId).subscribe(resp =>
      {
        this.modelFilesEdit = resp.data;
        
        this.isEditModelWindowOpened = !this.isEditModelWindowOpened;
        this.editedRowIndex = event.rowIndex;
        this.updateMLModelOriginalTopic = event.dataItem.topic;
        this.editModelForm = new FormGroup( {
          editTopic: new FormControl({value: event.dataItem.topic, disabled: false}),
          editModule: new FormControl({value: event.dataItem.module, disabled: false}),
          editProductionline: new FormControl({value: event.dataItem.productionLine, disabled: false}),
          editPreprocessingfile: new FormControl({value: event.dataItem.preprocessingFile, disabled: false}),
          editModeltype: new FormControl({value: event.dataItem.modelType, disabled: false}),
          editModelfile: new FormControl({value: event.dataItem.modelFileName, disabled: false}),
          editModelstatus: new FormControl({value: event.dataItem.isActive, disabled: false})
        });
        this.selectedModelTypeEdit = this.modelTypes.filter(p => p.id === event.dataItem.modelTypeId)[0];
        this.selectedModelFileEdit = this.modelFiles.filter(p => p.modelFile === event.dataItem.modelFile)[0];
        this.selectedModuleEdit = this.moduleList.filter(p => p.moduleName === event.dataItem.module)[0];
        this.selectedProdLineEdit = this.prodlineList.filter(p => p.line === event.dataItem.productionLine)[0];
        this.selectedStatus = this.modelStatus.filter(p => p.isActive === event.dataItem.isActive)[0];
        // this.selectedPreprocessingFileEdit = this.preprocessingFiles.filter(
        //   p => p.preprocessingFile === event.dataItem.preprocessingFile)[0];
      }
    );
  }

  updateModel(): void {
    const headers = { 'Content-Type': 'application/json; charset=utf-8' };
    const body = {
      Id: this.modelList[this.editedRowIndex].id,
      Topic:  this.editModelForm.get('editTopic').value,
      OriginalTopic: this.updateMLModelOriginalTopic,
      // PreprocessingFileId: this.selectedPreprocessingFileEdit.id,
      // PreprocessingFile: this.selectedPreprocessingFileEdit.preprocessingFileName,
      PreprocessingFile: this.editModelForm.get('editPreprocessingfile').value,
      ModelTypeId: this.selectedModelTypeEdit.id,
      ModelFileId: this.selectedModelFileEdit.id,
      ModelFile: this.selectedModelFileEdit.modelFile,
      Module: this.selectedModuleEdit.moduleName,
      ProductionLine: this.selectedProdLineEdit.line,
      CreatedBy: this.modelList[this.editedRowIndex].createdBy,
      // LastModifiedBy: this.userId,       //wstan
      LastModifiedBy: "Steven",
      IsActive: this.selectedStatus.isActive,
      InputParam: ["X1","X3","X9","X11"],
      MetaData: ["MachineId","WOId","ProductId","ProcessId","WorkpieceId","TotalQty"]
    };

    this.http.post<response>(this.settings.ENDPOINT.MLMODEL + '/MLModel/UpdateModel', JSON.stringify(body), { headers }).subscribe(resp => {

      this.dialogTitle = 'Update Model';
      this.dialogText = 'ML model updated.'; 

      const headers = { 'Content-Type': 'application/json; charset=utf-8' };
      const body = {
        Topic:  this.editModelForm.get('editTopic').value,
        ModelTypeId: this.selectedModelTypeEdit.id,
        ModelFileId: this.selectedModelFileEdit.id,

      };

      this.http.post<response>(this.settings.ENDPOINT.MLMODEL + '/MLModel/SearchMLModel', JSON.stringify(body), { headers }).subscribe(resp => {
        this.modelList = resp.data;
      }
      )
    },
    errors => {
      console.log(errors);
      this.handleError(errors, 'UpdateModel');
    });
  }

  closeEditModelWindow(): void {
    this.isEditModelWindowOpened = false;
    window.scroll(0, 0); // return to top of window
  }

  closeAddModelWindow(): void {
    this.isAddModelWindowOpened = false;
    this.addModelForm.reset();  // clear the input details
    window.scroll(0, 0); // return to top of window
  }

  public modelTypeChange(value: any): void {

    this.mlModelService.getModelFilesByModel(value.id).subscribe(resp => {
        this.modelFilesEdit = resp.data;
        this.selectedModelFileEdit = this.modelFilesEdit[0];
      },
      (error) => {                              // Error callback
        console.log(error);
        // console.error('error caught in component')
        // this.errorMessage = error;
        // this.loading = false;

        // throw error;   //You can also throw the error to a global error handler
      }
    );
  }

  public modelTypeChangeAdd(value: any): void {

    this.mlModelService.getModelFilesByModel(value.id).subscribe(resp => {
        this.modelFilesAdd = resp.data;
        this.selectedModelFileAdd = this.modelFilesAdd[0];
      }
    );
  }

  // module prod event add form
  public moduleChangeAdd(value: any): void {
    for (let x of this.prodlineList)
    {
      if (x.moduleId === value.id)
      {
        this.selectedProductionLineAdd =  x;
        this.prodlineListAdd = new Array<ProductionLine>();
        this.prodlineListAdd.push(x);
        this.selectedProductionLineAdd = x;
      }
    }
  }

  // module prod event edit form
  public moduleChangeEdit(value: any): void {
    for (let x of this.prodlineList)
    {
      if (x.moduleId === value.id)
      {
        this.selectedProdLineEdit =  x;
        this.prodlineListEdit = new Array<ProductionLine>();
        this.prodlineListEdit.push(x);
        this.selectedProdLineEdit = x;
      }
    }
  }

  openAddModelModal(): void{
    this.isAddModelWindowOpened = !this.isAddModelWindowOpened;

    this.addModelForm = new FormGroup({
      addTopic: new FormControl({value: null, disabled: false}),
      addPreprocessingFile: new FormControl({value: null, disabled: false}),
      addModelTypeId: new FormControl({value: null, disabled: false}),
      addModelFileId: new FormControl({value: null, disabled: false}),
      addModule: new FormControl({value: this.selectedModuleAdd, disabled: false}),
      addProductionLine: new FormControl({value: this.selectedProductionLineAdd, disabled: false}),
    });
    // console.log(JSON.stringify(this.preprocessingFiles));
  }

  saveNewModel(): number {
    let returnNum = -1;
    const body =
      {
        Topic : this.addModelForm.controls.addTopic.value,
        PreprocessingFile : this.addModelForm.controls.addPreprocessingFile.value,
        ModelTypeId : this.addModelForm.controls.addModelTypeId.value.id,
        ModelFileId: this.addModelForm.controls.addModelFileId.value.id,
        Module : this.addModelForm.controls.addModule.value.moduleName,
        ProductionLine : this.addModelForm.controls.addProductionLine.value.line,
        CreatedBy : this.userId,
        InputParam: ["X2","X4","X5","X6"],
        MetaData: ["MachineId", "WOId", "PartId", "ProcessId", "WorkpieceId", "TotalQty"]
      };
    // console.log(body);
    this.obsMLModelService =  this.mlModelService.postModel(body).subscribe(
      res => {
        this.dialogTitle = 'Add Model';
        this.dialogText = 'New ML model added.'; 
      },
      errors => {
        this.handleError(errors, 'AddModel');
        returnNum = 0 ;
      }
      // err => {
      //   console.log(err);
      // }
    );
    return returnNum;
  }

  onSubmitAddModel(): void {
    // thisForm = this.addModelForm;
    try {        
        this.saveNewModel();
        this.dialogTitle = 'Add Model';
        this.dialogText = 'Saving model ......';
        // this.showAlert('Model created successfully');
      //}
    }
    catch (e) {
      this.dialogTitle = 'Incomplete information';
      this.dialogText = 'Please enter ALL REQUIRED information before adding model';
    } finally {
      // thisForm.reset();
    }
    this.submitIsClicked = true;
  }


  onSubmitEditModel(): void {
    // thisForm = this.addModelForm;
    try {        
        this.updateModel();
        this.dialogTitle = 'Update Model';
        this.dialogText = 'Updating model ......';
        // this.showAlert('Model created successfully');
      //}
    }
    catch (e) {
      this.dialogTitle = 'Incomplete information';
      this.dialogText = 'Please enter ALL REQUIRED information before adding model';
    } finally {
      // thisForm.reset();
    }
    this.submitIsClicked = true;
  }


  closeDialog(): void {
    this.submitIsClicked = false;
    this.isAddModelWindowOpened = false;
    this.isEditModelWindowOpened = false;
  }

  // Error responses
  handleError(errors: any, fn: string) {
    // client-side error
    if (errors.error instanceof ErrorEvent) {
      // display error message
      this.dialogService.open(errors.message);
    }
    // server-side error
    else {
      if (errors.status === 500) {
        switch(fn){
          case 'AddModel':{
            this.dialogTitle = 'Error adding model';            
            break;
          }
          case 'UpdateModel':{
            this.dialogTitle = 'Error updating model';            
            break;
          }
        }

        this.dialogText = errors.error.msg;
        //this.dialogTitle = '';
        //this.dialogText = 'Topic name exist in system. Please change.';        //  redirect to error page
        // this._router.navigate(['/server-error']);
      // } else {
      //   // display error message
      //   this.dial.open(error.message, "Error");
      }
    }
  }
}
