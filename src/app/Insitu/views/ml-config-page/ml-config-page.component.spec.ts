import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MlConfigPageComponent } from './ml-config-page.component';

describe('MlConfigPageComponent', () => {
  let component: MlConfigPageComponent;
  let fixture: ComponentFixture<MlConfigPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MlConfigPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MlConfigPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
