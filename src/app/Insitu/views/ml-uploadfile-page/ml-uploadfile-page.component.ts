import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { SETTINGS } from '../../settings/environment';
import { SelectEvent } from "@progress/kendo-angular-layout";
import { SuccessEvent} from "@progress/kendo-angular-upload"
import {MlModelService} from '../../service/mlmodel/mlmodel.service';

interface ModelType {
  id: number;
  modelName: string;
}

@Component({
  selector: 'app-ml-uploadfile-page',
  templateUrl: './ml-uploadfile-page.component.html',
  styleUrls: ['./ml-uploadfile-page.component.scss']
})
export class MlUploadfilePageComponent implements OnInit {


  public modelTypes: Array<ModelType>;
  public defaultModelType: ModelType = {
    id: null,
    modelName:"Select model"
  }
  public selectedModelType: ModelType;

  public uploadSaveUrl = this.settings.ENDPOINT.MLMODEL + "/MLModel/UploadFile"; // should represent an actual API endpoint
  public uploadRemoveUrl = this.settings.ENDPOINT.MLMODEL + "/MLModel/DeleteFile"; // should represent an actual API endpoint

  constructor(
    private http: HttpClient,
    private mlModelService: MlModelService,
    private settings: SETTINGS
  ) { }

  ngOnInit(): void {
    this.mlModelService.getAllModelTypes().subscribe(resp => {
      this.modelTypes = resp.data
    })
  }

  public onTabSelect(e: SelectEvent) {
    
  }


  public transferSuccessHandler(e: SuccessEvent){
    if (e.operation == 'upload'){
      let filename = e.files[0].name;
      let modelTypeId = this.selectedModelType.id;

      this.http.get(this.settings.ENDPOINT.MLMODEL + '/MLModel/AddModelFile?filename=' + filename +
      '&modelTypeId=' + modelTypeId).
      subscribe(resp => {
        
      })

    }
  }

}
