import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MlUploadfilePageComponent } from './ml-uploadfile-page.component';

describe('MlUploadfilePageComponent', () => {
  let component: MlUploadfilePageComponent;
  let fixture: ComponentFixture<MlUploadfilePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MlUploadfilePageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MlUploadfilePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
