import {Component, OnInit, NgZone, AfterViewInit, Input} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { PassRateWithProcessId } from '../../passRate';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { SortDescriptor } from '@progress/kendo-data-query';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import * as global from '../../global';
import { formatDate } from '@angular/common';
import { HttpHeaders } from '@angular/common/http';
import { SETTINGS } from '../../settings/environment';
import { IntlService } from '@progress/kendo-angular-intl';
import { GridComponent } from "@progress/kendo-angular-grid";
import { take } from "rxjs/operators";
import {TranslateService} from '@ngx-translate/core';


@Component({
  selector: 'app-prediction-machine-page',
  templateUrl: './prediction-machine-page.component.html',
  styleUrls: ['./prediction-machine-page.component.scss']
})
export class PredictionMachinePageComponent implements OnInit, AfterViewInit {

  public grid: GridComponent;

  public machineId: string;
  public machineImage: string;
  public time: Date = new Date();
  public dashboardtime: string;
  timer;
  chartTimer;
  public pageTitle: string = '';
  public predictionResultList: any[];
  public PredictedPassResult: string = "000.00";
  public PredictedFailResult: string = "0.00";
  public TotalFailCase: number = 0;
  public TotalCase: number = 0;
  public TotalFailOverTotalCase: string = "";
  public passRateWithProcessIdList: Array<PassRateWithProcessId> = [];
  private _hubConnection: HubConnection;
  public YieldByProcessGridData: any[];
  public HardCodedTargetRate: number = 90;  //70;
  
  public dailyChartSeries: any[] = [
    {
      name: "% Pass Prediction",
      data: [96, 92.7, 89.3, 87.5, 91.6, 92, 91.3, 86.9, 94.5, 98.3, 89.9, 86.9, 93.2, 91.8],
      markers:{
        size:5,
        type:'circle'
      },
      color:"blue"
    },
    {
      name: "% Pass Actual",
      data: [91, 88.2, 93.1, 91.2, 87.4, 90.5, 85.3, 92.2, 98.5, 86.4, 89.5, 86.8, 91.2, 94.1],
      markers:{
        size:5,
        type:'square'
      },
      color:"orange"
    },
    {
      name: "Target",
      data: [90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90],
      markers:{
        visible:false
      },
      color:"#000000"
    }
  ];

  //real-time chart
  public realTimeChartSeries: any[];
  public categories: string[] = [];
  public categoriesRealTimeChart: string[] = [];
  public minYaxisRealTimeChart: number
  public maxYaxisRealTimeChart: number
  public minYaxisDailyYieldChart: number
  public maxYaxisDailyYieldChart: number

  public sortByProcess: SortDescriptor[] = [
    {
        field: 'processId',
        dir: 'asc'
    }
  ];

  //for the lot list window
  public showLotList: boolean = false;
  public lotListTitle: string;
  public lotListData: any[];
  public windowTop = 100;
  public windowLeft = 365;
  public showMachineId: boolean = false;
  public showPredictedResult: boolean = false;
  public batchIdColName: string
  public workpieceIdColName: string

  constructor(private http: HttpClient, private route: ActivatedRoute,
    private sanitizer: DomSanitizer,
    public intl: IntlService,
    private ngZone: NgZone,
    private translate: TranslateService,
    private settings: SETTINGS) {

    const headers= new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', '*')

    if(global.nextMachineCleanUpTime === undefined){
      global.updateNextMachineCleanUpTime(this.calculateNextCleanUpTime());
    }
    else{
      if(new Date() >= global.nextMachineCleanUpTime){
        global.updateNextMachineCleanUpTime(this.calculateNextCleanUpTime());
        global.resetMachineSeries();
      }
    }

    for(let i=13; i >= 0; i--){
      var currentDate = new Date();
      currentDate.setDate(currentDate.getDate() - i);
      //this.categories.push(currentDate.toLocaleDateString('en-GB', {year: "numeric", month: "short", day: "numeric"}));
      this.categories.push(formatDate(currentDate, 'd MMM yyyy', this.settings.LOCALE.ID))
    }

    //"http://192.168.137.1:5004/api
    this.http.get<any>(this.settings.ENDPOINT.API + "/Database/Summary/GetCurrentPredictionResult").subscribe(prediction => {
      this.predictionResultList = prediction.data;
      this.CalculatePassRate(prediction.data);
      this.YieldByProcessGridData = this.passRateWithProcessIdList;

      //if(global.machineSeries[0].data.length === 0){
        this.populateRealTimeChart();
      //}
      //else{
        //this.realTimeChartSeries = global.machineSeries;
      //}

      this.populateDailyChart();

    }, err =>{
      console.log('Something went wrong ', err);
    })
  }

  ngOnInit(): void {
    this.route
    .queryParams
    .subscribe(params => {
      this.machineId = params['machineId'];
    });

    if(global.machineId === ""){
      global.UpdateMachineID(this.machineId);
    }
    else{
      if(global.machineId !== this.machineId){
        global.resetMachineSeries();
        global.UpdateMachineID(this.machineId);
      }
    }

    //url = Server Url
    this._hubConnection = new HubConnectionBuilder().withUrl(this.settings.ENDPOINT.SIGNALR).build();
    this._hubConnection.start()
      .then(() => console.log('result page connection start'))
      .catch(err => {
        console.log('Error while establishing the connection')
      });

    this._hubConnection.on('BroadcastPredictionResult', (prediction) => {
      this.passRateWithProcessIdList = [];
      this.predictionResultList.push(prediction);
      this.CalculatePassRate(this.predictionResultList);
      this.YieldByProcessGridData = this.passRateWithProcessIdList;
    })

    //timer to update the dashboard time every 1s
    this.timer = setInterval(() => {
      this.dashboardtime = this.createPageSubtitle();
    }, 1000);

    //timer to update the real-time predicted yield chart every 15s
    this.chartTimer = setInterval(() => {
      if(new Date() >= global.nextMachineCleanUpTime){
        global.updateNextMachineCleanUpTime(this.calculateNextCleanUpTime());
        global.resetMachineSeries();
      }

      // if(global.machineSeries[0].data.length === 0 || this.PredictedPassResult !== global.machineSeries[0].data[global.machineSeries[0].data.length-1])
      // {
        this.populateRealTimeChart();
      //}

      this.populateDailyChart();
    }, 15000);


    this.pageTitle = this.createPageTitle();

    if (this.machineId == "Laser Welding"){
      this.machineImage = "assets/img/laser_welding.jpg"
    } 
    else if (this.machineId == "PIM001" || this.machineId == "Injection Molding"){
      this.machineImage = "assets/img/injection_molding.jpg"
    }
  }


  ngOnDestroy(){
    clearInterval(this.timer);
    clearInterval(this.chartTimer);
    this._hubConnection.stop()
    .then(() => console.log('Results page connection stop'))
    .catch(err => {
      console.log('Error while stopping the connection')
    });
  }

  public calculateNextCleanUpTime(): Date{

    let currentDate = new Date();
    if(currentDate.getHours() >= Number(global.DashStartTime)){
      currentDate.setDate(currentDate.getDate() + 1);
    }

    return new Date(formatDate(currentDate, 'yyyy-MM-dd', 'en_US') + " " + global.DashStartTime + ":00:00");
  }

  public populateRealTimeChart(){
    const myClonedArray = [];

    //let localeId = this.translate.currentLang;

    global.machineSeries.forEach(val => myClonedArray.push(Object.assign({}, val)));

    if (myClonedArray[0].data.length == 6){
      myClonedArray[0].data.shift();
      myClonedArray[0].data.push(this.PredictedPassResult);

      //calculate y-axis min value
      this.minYaxisRealTimeChart = Math.floor(this.CalcChartYaxisMin(myClonedArray,this.HardCodedTargetRate)/10)*10;

      //calculate y-axis max value
      this.maxYaxisRealTimeChart = Math.ceil(this.CalcChartYaxisMax(myClonedArray,this.HardCodedTargetRate)/10)*10;

      myClonedArray[1].data.shift();
      myClonedArray[1].data.push(this.HardCodedTargetRate);

      this.realTimeChartSeries = myClonedArray;

      this.categoriesRealTimeChart = [...global.categoryRealTimeMachinePg];
      let currentDate = new Date();
      this.categoriesRealTimeChart.shift();
      //this.categoriesRealTimeChart.push(currentDate.toLocaleString(localeId, {hour: "numeric", minute: "numeric", second: "numeric"}));
      this.categoriesRealTimeChart.push(formatDate(currentDate, 'h:mm:ss a', this.settings.LOCALE.ID));
      
      global.categoryRealTimeMachinePg.shift();
      //global.categoryRealTimeMachinePg.push(currentDate.toLocaleString(localeId, {hour: "numeric", minute: "numeric", second: "numeric"}));
      global.categoryRealTimeMachinePg.push(formatDate(currentDate, 'h:mm:ss a', this.settings.LOCALE.ID))
    }
    else {
      myClonedArray[0].data.push(this.PredictedPassResult);
      myClonedArray[1].data.push(this.HardCodedTargetRate);
      this.realTimeChartSeries = myClonedArray;

      //calculate y-axis min value
      this.minYaxisRealTimeChart = Math.floor(this.CalcChartYaxisMin(myClonedArray,this.HardCodedTargetRate)/10)*10;

      //calculate y-axis max value
      this.maxYaxisRealTimeChart = Math.ceil(this.CalcChartYaxisMax(myClonedArray,this.HardCodedTargetRate)/10)*10;

      this.categoriesRealTimeChart = [...global.categoryRealTimeMachinePg];
      let currentDate = new Date();
      //this.categoriesRealTimeChart.push(currentDate.toLocaleString(localeId, {hour: "numeric", minute: "numeric", second: "numeric"}));      
      //global.categoryRealTimeMachinePg.push(currentDate.toLocaleString(localeId, {hour: "numeric", minute: "numeric", second: "numeric"}));
      this.categoriesRealTimeChart.push(formatDate(currentDate, 'h:mm:ss a', this.settings.LOCALE.ID));
      global.categoryRealTimeMachinePg.push(formatDate(currentDate, 'h:mm:ss a', this.settings.LOCALE.ID));

      
      
    }

  }

  public populateDailyChart(){

    this.dailyChartSeries[0].data[13] = this.PredictedPassResult;
    const myClonedArray = [];
    this.dailyChartSeries.forEach(val => myClonedArray.push(Object.assign({}, val)));
    this.dailyChartSeries = myClonedArray;

      //calculate y-axis min value
      this.minYaxisDailyYieldChart = Math.floor(this.CalcChartYaxisMin(myClonedArray,this.HardCodedTargetRate)/10)*10;

      //calculate y-axis max value
      this.maxYaxisDailyYieldChart = Math.ceil(this.CalcChartYaxisMax(myClonedArray,this.HardCodedTargetRate)/10)*10;

  }

  public CalculatePassRate(prediction: any){

    if(prediction.length > 0){
      this.TotalCase = prediction.filter(p => p.machineId === this.machineId).length;
      this.TotalFailCase = prediction.filter(p => p.isOOS == true && p.machineId === this.machineId).length;
      this.TotalFailOverTotalCase = this.TotalFailCase.toString() + '/' + this.TotalCase.toString();
      this.PredictedPassResult = ((this.TotalCase - this.TotalFailCase) / this.TotalCase * 100).toFixed(2).toString();
      this.PredictedFailResult = (this.TotalFailCase / this.TotalCase * 100).toFixed(2).toString();
    }

    const DistinctProcess = prediction.filter(p => p.machineId === this.machineId).map(item => item.processId).filter((value, index, self) => self.indexOf(value) === index);
    DistinctProcess.forEach( (process) => {
      let pr = new PassRateWithProcessId();
      const TotalResult = prediction.filter(p => p.machineId === this.machineId && p.processId === process);
      const TotalPassResult = prediction.filter(p => p.isOOS == false && p.machineId === this.machineId && p.processId === process);
      const rate = TotalPassResult.length / TotalResult.length * 100;
      pr.machineId = this.machineId;
      pr.processId = process;
      pr.passRate = rate.toFixed(2).toString() + "%";
      pr.targetPassRate = this.HardCodedTargetRate + "%"
      this.passRateWithProcessIdList.push(pr);
    });
  }

  public cellColorCode(passRate: string, targetPassRate: string): SafeStyle {
    let result;

    if (+passRate.split('%')[0] < +targetPassRate.split('%')[0]){
      result = '#f31700';
    }
    else{
      result = 'transparent';
    }

    return this.sanitizer.bypassSecurityTrustStyle(result);
  }

  public createPageTitle(): string{
    let title = 'Machine ' + this.machineId + ': Quality Dashboard';
    return title;
  }


  public createPageSubtitle():string{
    return this.calcDashboardTime();
  }


  private calcDashboardTime(): string{

    //let localeId = this.translate.currentLang;
    let d = new Date();
    d.setHours(8);
    d.setMinutes(0);
    d.setSeconds(0);
    d.setMilliseconds(0);
    let startdatetime = d.getTime();


    let curTime = new Date().getTime();

    if (curTime < startdatetime){
      let startdate = new Date((startdatetime - (24*60*60*1000)));
      let curdate = new Date(curTime);

      let dashboardDate = formatDate(startdate, 'd MMM yyyy h:mm:ss a', this.settings.LOCALE.ID) + '-' + formatDate(curdate, 'd MMM yyyy h:mm:ss a', this.settings.LOCALE.ID);
      return dashboardDate;
      //return startdate.toLocaleString(localeId, {year: "numeric", month: "short", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"}) + ' - ' + curdate.toLocaleString('en-GB', {year: "numeric", month: "short", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"});
    }
    else{
      let startdate = new Date(startdatetime);
      let curdate = new Date(curTime);

      let dashboardDate = formatDate(startdate, 'd MMM yyyy h:mm:ss a', this.settings.LOCALE.ID) + '-' + formatDate(curdate, 'd MMM yyyy h:mm:ss a', this.settings.LOCALE.ID);
      return dashboardDate;
      //return startdate.toLocaleString(localeId, {year: "numeric", month: "short", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"}) + ' - ' + curdate.toLocaleString('en-GB', {year: "numeric", month: "short", day: "numeric", hour: "numeric", minute: "numeric", second: "numeric"});
    }
  }

  public openLotList(status){

    let isOOS:boolean
    if (status.toLowerCase() == "pass"){
      isOOS = false;
    }
    else if (status.toLowerCase() == "fail"){
      isOOS = true;
    }

    this.http.get(this.settings.ENDPOINT.API + "/Database/Summary/GetCurrentPredictionResult").subscribe((prediction:any) => {

      //datetime is returned as a string e.g. "2022-01-25T01:20:25" in UTC format
      //by appending 'Z' to this string, the date is in ISO8601 format
      //using new Date(xxxx) will automatically convert xxxx to local datetime
      //this only works because the returned time from API is UTC
      //if API returns time in local time, disable this
      // prediction.data.forEach(element => {
      //   element.createdDateTime = new Date(element.createdDateTime + 'Z');
      // });

      this.lotListData = prediction.data.filter(p => p.machineId === this.machineId && p.isOOS === isOOS);
      this.lotListTitle = "Machine: " + this.machineId + " Lot List for " + status.toUpperCase() + " Predictions";

      this.showLotList = true;
      this.showMachineId = false;
      this.showPredictedResult = false;
      this.batchIdColName = "Work Order No.";
      this.workpieceIdColName = "Shot";

      window.scroll(0,0);
    }, err =>{
      console.log('Something went wrong ', err);
    })
  }

  public closeLotListWindow(){
    this.showLotList = false;
    window.scrollTo(0,0);
  }


  public ngAfterViewInit(): void {
    this.fitColumns();
  }

  public onDataStateChange(): void {
    this.fitColumns();
  }

  private fitColumns(): void {
    this.ngZone.onStable
      .asObservable()
      .pipe(take(1))
      .subscribe(() => {
        this.grid.autoFitColumns();
      });
  }


  private CalcChartYaxisMax(data:any[],targetline: number){
    let maxYaxisRealTimeChart: number

    let max = Math.max(...data[0].data);
      if (max >= targetline){
        maxYaxisRealTimeChart = Math.round(max + 0.2*max);
      }
      else {
        maxYaxisRealTimeChart = Math.round(targetline + 0.2*targetline);
      }

      return maxYaxisRealTimeChart;
  }

  private CalcChartYaxisMin(data:any[],targetline: number){
    let minYaxisRealTimeChart: number

      let min = Math.min(...data[0].data);
      if (min <= targetline){
        minYaxisRealTimeChart = Math.round(min - 0.2*min);
      }
      else{
        minYaxisRealTimeChart = Math.round(targetline - 0.2*targetline)
      }
      if (minYaxisRealTimeChart <= 0){
        minYaxisRealTimeChart = 0;
      }

      return minYaxisRealTimeChart;
  }
}
