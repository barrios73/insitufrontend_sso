import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MlModeltypePageComponent } from './ml-modeltype-page.component';

describe('MlModeltypePageComponent', () => {
  let component: MlModeltypePageComponent;
  let fixture: ComponentFixture<MlModeltypePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MlModeltypePageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MlModeltypePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
