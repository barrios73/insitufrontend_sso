import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PredictionMachinetrayPageComponent } from './prediction-machinetray-page.component';

describe('PredictionMachinetrayPageComponent', () => {
  let component: PredictionMachinetrayPageComponent;
  let fixture: ComponentFixture<PredictionMachinetrayPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PredictionMachinetrayPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PredictionMachinetrayPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
