import {Component, ElementRef, OnInit, Inject, TemplateRef, ViewContainerRef, Input} from '@angular/core';
import { IconThemeColor} from '@progress/kendo-angular-icons/dist/es2015/common/models/theme-color';
import { IconModule} from '@progress/kendo-angular-icons';
import {setData} from '@progress/kendo-angular-intl';
import {TranslateService} from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import * as global from '../../global';
import { formatDate, DOCUMENT } from '@angular/common';
import { SETTINGS } from '../../settings/environment';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { HttpClient } from '@angular/common/http';
import {STATUS_CODES} from 'http';
import {totalmem} from 'os';
import { SequenceEqualSubscriber } from 'rxjs/internal/operators/sequenceEqual';
import {toFixedPrecision} from "@progress/kendo-angular-inputs/dist/es2015/common/math";
import {MlModelService} from '../../service/mlmodel/mlmodel.service';
import { MlConfigPageComponent } from "../ml-config-page/ml-config-page.component";
import {DomSanitizer, SafeStyle} from '@angular/platform-browser';
import { PaginationComponent } from '../../pagination/pagination.component';
import { FormControl } from '@angular/forms';

// import services and interfaces
import { IResponse, ModelFile, PreprocessingFile, ModelType } from "../../interface/ml-interfaces";
import {any} from "codelyzer/util/function";
import {EditEvent} from "@progress/kendo-angular-grid";
import {element} from "protractor";
import {workspaceSchemaPath} from "@angular/cli/utilities/config";

interface IMachineTray {

  workOrderNo: string;
  trayNumber: number;
  numItemPerTray: number;
  // workPieceList: WorkPiece[];
  rowSize: number;              // ws: how many wp each row contains
}

interface IWorkPiece {
  wpId: number;                       // ws: start from 1 to tray
  wpName: string;
  processingStatus: number;         // 0:in queue, 1:completed
  predictResult: string;
  isOOS: boolean            //  1:fail , 2: pass
}

interface response {
  hasError: boolean;
  msg: string;
  data: any[];
}

/*export interface PaginatedResponse<T> {
  // items: T[];
  items: Array<any>;
  total: number;
}*/

@Component({
  selector: 'app-prediction-machinetray-page',
  templateUrl: './prediction-machinetray-page.component.html',
  styleUrls: ['./prediction-machinetray-page.component.scss']
})

export class PredictionMachinetrayPageComponent implements OnInit {

  private _hubConnection: HubConnection;
  
  public dashboardtime: string;
  public timer;
  public chartTimer;
  public pageTitle = '';
  public machineId: string;


  ////////////////////////////////////////////////////////////////////////
  // for the 3 KPIs calculation
  public predictionResultList: any[] = [];
  public PredictedPassResult = '0';
  public PredictedFailResult = '0.00';
  public TotalFailCase = 0;
  public TotalCase = 0;
  public TotalFailOverTotalCase = '';

  // for real-time chart
  public realTimeChartSeries: any[];
  public categoriesRealTimeChart: string[] = [];
  public minYaxisRealTimeChart: number;
  public maxYaxisRealTimeChart: number;
  public minYaxisDailyYieldChart: number;
  public maxYaxisDailyYieldChart: number;
  public HardCodedTargetRate = 90;  //70;
  ////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////
  //for tray physical layout
  public numItemPerTray = 12;	//controls physical layout of tray
  public numItemPerRow = 4;	//controls physical layout of tray
  public numOfRowsPerTrayPhysical: number = this.numItemPerTray/this.numItemPerRow ;  //not hardcoded, = numItemPerTray/numItemPerRow
  rowArray: Array<Number> = createArray(this.numOfRowsPerTrayPhysical)				  //array size based on numOfRowsPerTray, from 0 to numOfRowsPerTray - 1
  //////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////
  // for visualisation
  public batchSizeByMachine: any[] = [
    {
      machineId:"Laser Welding",
      batchSize:1
    },
    {
      machineId:"PIM001",
      batchSize:4
    }
  ];
  public predictionBatchSize = 1;  // steven, hardcoded to 1 for laser welding
  public workOrderNo: string; // = 22092000005;
  public workPieceList: Array<IWorkPiece>;
  public wpList: Array<IWorkPiece>; // steven
  public elemId: string; // steven
  public predictResult: number;  // steven
  public hasRetrievedCompletedPredictions = false; // steven, should be false, set to true for testing
  public counter = 0;  // steven
  public completedPredictionsByWO: any[];  // steven
  public noCompletedPredictionsByWO: number; // steven, set to 0 for testing
  public noOfPredictionsCnt = 0; // steven
  
  public prevWO = '';
  public predictionType = '';
  public mlModel = '';
  public mlLibrary = '';
  public wpCounter = 0;
  public wpNameArray: Array<string> = ["[1,1]", "[1,2]", "[1,3]", "[1,4]",
    "[2,1]", "[2,2]", "[2,3]", "[2,4]",
    "[3,1]", "[3,2]", "[3,3]", "[3,4]"
  ]
  //////////////////////////////////////////////////////////////////////////


  public highlightRow: boolean = false;

  // default configs  --ws
  public defaultWp: IWorkPiece = {
    wpId: 0,
    wpName: '',
    processingStatus: 0,
    predictResult: '',
    isOOS: false
  };

  public defaultTray: IMachineTray = {
    workOrderNo: '',
    trayNumber: 1,
    numItemPerTray: 12,
    rowSize: 4,
  };

  // for table  --ws
  public predHistTableDs: any[] = [];
  public status: any[] = [
    {
      kind: 'Pass',
      color: 'green'
    },
    {
      kind: 'Fail',
      color: 'red'
    },
    {
      kind: 'Pending',
      color: 'grey'
    },
    // {
    //     kind: 'Aborted',            // added by ws. NIU for now
    //     color: 'orange'
    // }
  ];


  public modelTypes: Array<ModelType>;
  public preprocessingFiles: Array<PreprocessingFile>;
  public resultRow = '';


  //////////////////////////////////////////////////////////////////////////////////////
  //for display of tray when click view details in table
  public isViewDetailsClicked = false;    // was false;
  public vdTitle = '';
  vdworkOrderNo: string; // = 22092000005;
  public numItemPerVdTray = 12;
  public numItemPerVdRow = 4
  public numOfRowsPerVdTrayPhysical = this.numItemPerVdTray/this.numItemPerVdRow;
  public vdRowArr:  Array<Number> = createArray(this.numOfRowsPerVdTrayPhysical);
  public vdWpList: Array<IWorkPiece>;
  public prevVdWO = '';
  public vdPredictionType = '';
  public vdMlModel = '';
  public vdMlLibrary = '';
  public vdInterval;
  public notFirstTray = false;
  //////////////////////////////////////////////////////////////////////////////////////

  // this is for the Model File
  public modelFiles: Array<ModelFile>;
  public defaultModelFile: ModelFile = {
    id: null,
    modelTypeId: null,
    modelFileName: 'Select model file'
  };


  //define colour of % pass KPI
  public value = 10;
  public colors = [
    {
      to: 50,
      color: '#FF5733',
    },
    {
      from: 50,
      to: 75,
      color: '#FF981A',
    },
    {
      from: 75,
      color: '#4CBB17',
    },
  ];


  // for next batch prediction
  public futurePrediction: any[] = [];
  public nextBatchPredictedResults: any[] = [];
  public showNextBatchPrediction = false;
  public onPageLoad = false;  // default true
  public nextWorkpieceArray = [
    {
      curWorkpieceId: '11',
      nextWorkpiece: '12'
    },
    {
      curWorkpieceId: '12',
      nextWorkpiece: '13'
    },
    {
      curWorkpieceId: '13',
      nextWorkpiece: '14'
    },
    {
      curWorkpieceId: '14',
      nextWorkpiece: '21'
    },
    {
      curWorkpieceId: '21',
      nextWorkpiece: '22'
    },
    {
      curWorkpieceId: '22',
      nextWorkpiece: '23'
    },
    {
      curWorkpieceId: '23',
      nextWorkpiece: '24'
    },
    {
      curWorkpieceId: '24',
      nextWorkpiece: '31'
    },
    {
      curWorkpieceId: '31',
      nextWorkpiece: '32'
    },
    {
      curWorkpieceId: '32',
      nextWorkpiece: '33'
    },
    {
      curWorkpieceId: '33',
      nextWorkpiece: '34'
    }
  ];


  // pagination
  public moreThanOneTray: Boolean = false;
  public current: number;
  /*
    public items = [...Array(180).keys()].map((x) => `item ${++x}`);
    public itemsToDisplay: string[] = [];
  */
  public items = new Array<IWorkPiece>();
  public itemsToDisplay = new Array<IWorkPiece>();
  public total: number;
  /*
    public perPage = 10;  // vdnumitemsperpg
  */
  /*
    public total = Math.ceil(this.items.length / this.numItemPerVdTray);
  */

  constructor(
      private sanitizer: DomSanitizer,
      private translate: TranslateService,
      private mlModelService: MlModelService,
      private route: ActivatedRoute,
      public http: HttpClient,
      public settings: SETTINGS,
      @Inject(DOCUMENT) private document: Document) {

    this.mlModelService.getAllModelTypes().subscribe(resp => {
          this.modelTypes = resp.data;
        }
    );

    this.mlModelService.getAllModelFiles().subscribe(resp =>
        this.modelFiles = resp.data);

    this.mlModelService.getPreprocessingFiles().subscribe(resp =>
        this.preprocessingFiles = resp.data);



  }

  ngOnInit(): void {
    this.route
        .queryParams
        .subscribe(params => {
          this.machineId = params['machineId'];
        });


    this.predictionBatchSize = this.batchSizeByMachine.filter(x => x.machineId == this.machineId)[0].batchSize;


    if (global.machineId === '') {
      global.UpdateMachineID(this.machineId);
    } else {
      if (global.machineId !== this.machineId) {
        global.resetMachineSeries();
        global.UpdateMachineID(this.machineId);
      }
    }


    this.http.get<any>(this.settings.ENDPOINT.API + '/Database/Summary/GetCurrentPredictionResult').subscribe(prediction => {
      this.predictionResultList = prediction.data;

      if (prediction.data.length > 0){
        this.CalculatePassRate(prediction.data);
        this.populateRealTimeChart();      
        this.predHistTableDs = this.setTable(this.predictionResultList.filter(p => p.machineId === this.machineId));
        this.setTrayOnPgLoad(this.predHistTableDs);
      }
    }, err => {
      console.log('Something went wrong ', err);
    });


    //for visualisation, create the tray layout
    this.wpList = new Array<IWorkPiece>(this.numItemPerTray);
    this.vdWpList = new Array<IWorkPiece>(this.numItemPerVdTray);
    for (let i = 0; i < this.numItemPerTray; i++) {
      let pcs = {
        wpId: 0,
        wpName: '',
        processingStatus: 0,
        predictResult: '',
        isOOS: false
      }

      let pcsVdWp = {
        wpId: 0,
        wpName: '',
        processingStatus: 0,
        predictResult: '',
        isOOS: false
      }
      this.wpList[i] = pcs;
      this.vdWpList[i] = pcsVdWp;
    }

    this.addRows(this.numItemPerTray, this.wpList);
    this.addRowforVD(this.numItemPerVdTray, this.vdWpList);

    // //for pagination in vd
    // this.paginationControl.valueChanges.subscribe(this.onPageChange.bind(this));
    // this.visibleItems = {
    //   items: this.vdWpList.slice(0, this.numItemPerVdTray),
    //   total: this.vdWpList.length,
    // };
    this.itemsToDisplay = this.paginate(this.current, this.numItemPerTray, this.predictionBatchSize);

    //url = Server Url
    this._hubConnection = new HubConnectionBuilder().withUrl(this.settings.ENDPOINT.SIGNALR).build();
    this._hubConnection.start()
        .then(() => console.log('result page connection start'))
        .catch(err => {
          console.log('Error while establishing the connection');
        });

    this._hubConnection.on('BroadcastPredictionResult', (prediction) => {
      if (prediction.machineId === this.machineId) {
        console.log(prediction);

        const topic = prediction.topic.toString().toUpperCase();

        if (topic.includes('NADINE') || topic.includes('PARSNET')) {
          this.predictionType = 'Next Batch';
        } else {
          this.predictionType = 'Current Batch';
        }

        this.mlModel = prediction.modelName
        this.setColorPredictionResult(prediction, this.predictionType);
        this.predictionResultList.push(prediction);
        this.CalculatePassRate(this.predictionResultList);
        // this.updateStatsRow(prediction);
        this.updateStatsRow(this.predictionResultList);

        if (this.predictionType === 'Next Batch') {
          let workpieceId = String(prediction.workpieceId);
          workpieceId = '[' + workpieceId.substring(0, 1) + ',' + workpieceId.substring(1, 2) + ']';
          prediction.workpieceId = workpieceId;
          this.futurePrediction.push(prediction);
          this.nextBatchPredictedResults = [...this.futurePrediction];
          this.showNextBatchPrediction = true;
        } else {
          // this.nextBatchPredictedResults = []
          this.showNextBatchPrediction = false;
        }
      }
    })

    //timer to update the dashboard time every 1s
    this.timer = setInterval(() => {
      this.dashboardtime = this.createPageSubtitle();
    }, 1000);

    //timer to update the real-time predicted yield chart every 15s
    this.chartTimer = setInterval(() => {
      if (new Date() >= global.nextMachineCleanUpTime) {
        global.updateNextMachineCleanUpTime(this.calculateNextCleanUpTime());
        global.resetMachineSeries();
      }

      // if(global.machineSeries[0].data.length === 0 || this.PredictedPassResult !== global.machineSeries[0].data[global.machineSeries[0].data.length-1])
      // {
      this.populateRealTimeChart();
      //}

      //this.populateDailyChart();
    }, 15000);


    this.timer = setInterval(() => {
      this.dashboardtime = this.createPageSubtitle();
    }, 1000);

    this.pageTitle = this.createPageTitle()
  }

  ngAfterViewInit(): void{
    this.setSquareHeight();
    // this.addRowforVD(this.numItemPerVdTray, this.vdWpList);
  }


  ngOnDestroy(): void {

  }

  searchMLModelByTopic(searchTop: string): void {

    this.mlModelService.getAllModelTypes().subscribe(resp => {
          this.modelTypes = resp.data;
        }
    );

    this.mlModelService.getAllModelFiles().subscribe(resp =>
        this.modelFiles = resp.data);

    this.mlModelService.getPreprocessingFiles().subscribe(resp =>
        this.preprocessingFiles = resp.data);


    let topic: string = searchTop;
    let modelType: number = null;
    let modelFile: number = null;

    let modelList: any[] = null;

    const headers = {'Content-Type': 'application/json; charset=utf-8'};
    const body = {
      Topic: topic,
      ModelTypeId: modelType,
      ModelFileId: modelFile,
    };

    this.http.post<response>(this.settings.ENDPOINT.MLMODEL + '/MLModel/SearchMLModel', JSON.stringify(body), {headers}).subscribe(resp => {
      modelList = resp.data;
      // set tray
      this.predictionType = 'Current Batch'
      this.mlLibrary = modelList[0].library
      this.mlModel = modelList[0].modelName
    });
  }


  searchMLModelByTopicforVd( searchTop: string  ): void  {

    this.mlModelService.getAllModelTypes().subscribe(resp => {
          this.modelTypes = resp.data;
        }
    );

    this.mlModelService.getAllModelFiles().subscribe(resp =>
        this.modelFiles = resp.data);

    this.mlModelService.getPreprocessingFiles().subscribe(resp =>
        this.preprocessingFiles = resp.data);


    const topic: string = searchTop;
    const modelType: number = null;
    const modelFile: number = null;

    let modelList: any[] = null;

    const headers = { 'Content-Type': 'application/json; charset=utf-8' };
    const body = {
      Topic:  topic,
      ModelTypeId: modelType,
      ModelFileId: modelFile,
    };

    this.http.post<response>(this.settings.ENDPOINT.MLMODEL + '/MLModel/SearchMLModel', JSON.stringify(body), { headers }).subscribe(resp => {
      modelList = resp.data;
      // set tray
      this.vdPredictionType = 'Current Batch';
      this.vdMlModel = modelList[0].modelName;
      this.vdMlLibrary = modelList[0].library;
    });
  }

  public clearTray( wplist: Array<any> ) {
    wplist.forEach(function (item, index) {
      let square = document.getElementById(item.wpId.toString())
      square.classList.remove('fail', 'pass')
      square.classList.add('square')

    })
  }


  public createPageTitle(): string {
    return 'Quality Dashboard: ' + this.machineId;
  }

  public createPageSubtitle(): string {
    return this.calcDashboardTime();
  }

  public CalculatePassRate(prediction: any) {

    if (prediction.length > 0) {
      this.TotalCase = prediction.filter(p => p.machineId === this.machineId).length*this.predictionBatchSize;
      this.TotalFailCase = prediction.filter(p => p.isOOS == true && p.machineId === this.machineId).length*this.predictionBatchSize;
      this.TotalFailOverTotalCase = this.TotalFailCase.toString() + '/' + this.TotalCase.toString();
      this.PredictedPassResult = Math.round(((this.TotalCase - this.TotalFailCase) / this.TotalCase * 100)).toString();
      this.PredictedFailResult = (this.TotalFailCase / this.TotalCase * 100).toFixed(2).toString();
    }

    // const DistinctProcess = prediction.filter(p => p.machineId === this.machineID).map(item => item.processId).filter((value, index, self) => self.indexOf(value) === index);
    // DistinctProcess.forEach( (process) => {
    //   let pr = new PassRateWithProcessId();
    //   const TotalResult = prediction.filter(p => p.machineId === this.machineID && p.processId === process);
    //   const TotalPassResult = prediction.filter(p => p.isOOS == false && p.machineId === this.machineID && p.processId === process);
    //   const rate = TotalPassResult.length / TotalResult.length * 100;
    //   pr.machineId = this.machineID;
    //   pr.processId = process;
    //   pr.passRate = rate.toFixed(2).toString() + "%";
    //   pr.targetPassRate = this.HardCodedTargetRate + "%"
    //   this.passRateWithProcessIdList.push(pr);
    // });
  }

  public calculateNextCleanUpTime(): Date {

    let currentDate = new Date();
    if (currentDate.getHours() >= Number(global.DashStartTime)) {
      currentDate.setDate(currentDate.getDate() + 1);
    }

    return new Date(formatDate(currentDate, 'yyyy-MM-dd', 'en_US') + ' ' + global.DashStartTime + ':00:00');
  }

  public populateRealTimeChart() {
    const myClonedArray = [];

    let localeId = this.translate.currentLang;

    global.machineSeries.forEach(val => myClonedArray.push(Object.assign({}, val)));

    if (myClonedArray[0].data.length == 6) {
      myClonedArray[0].data.shift();
      myClonedArray[0].data.push(this.PredictedPassResult);

      //calculate y-axis min value
      this.minYaxisRealTimeChart = Math.floor(this.CalcChartYaxisMin(myClonedArray, this.HardCodedTargetRate) / 10) * 10;

      //calculate y-axis max value
      this.maxYaxisRealTimeChart = Math.ceil(this.CalcChartYaxisMax(myClonedArray, this.HardCodedTargetRate) / 10) * 10;

      myClonedArray[1].data.shift();
      myClonedArray[1].data.push(this.HardCodedTargetRate);

      this.realTimeChartSeries = myClonedArray;

      this.categoriesRealTimeChart = [...global.categoryRealTimeMachinePg];
      let currentDate = new Date();
      this.categoriesRealTimeChart.shift();
      this.categoriesRealTimeChart.push(currentDate.toLocaleString(localeId, {
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
      }));
      global.categoryRealTimeMachinePg.shift();
      global.categoryRealTimeMachinePg.push(currentDate.toLocaleString(localeId, {
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
      }));
    } else {
      myClonedArray[0].data.push(this.PredictedPassResult);
      myClonedArray[1].data.push(this.HardCodedTargetRate);
      this.realTimeChartSeries = myClonedArray;

      //calculate y-axis min value
      this.minYaxisRealTimeChart = Math.floor(this.CalcChartYaxisMin(myClonedArray, this.HardCodedTargetRate) / 10) * 10;

      //calculate y-axis max value
      this.maxYaxisRealTimeChart = Math.ceil(this.CalcChartYaxisMax(myClonedArray, this.HardCodedTargetRate) / 10) * 10;

      this.categoriesRealTimeChart = [...global.categoryRealTimeMachinePg];
      let currentDate = new Date();
      this.categoriesRealTimeChart.push(currentDate.toLocaleString(localeId, {
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
      }));
      global.categoryRealTimeMachinePg.push(currentDate.toLocaleString(localeId, {
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
      }));

    }
  }

  private setSquareHeight( ) {
    let squaresEle = document.getElementsByClassName('square');
    let labelsEle = document.getElementsByClassName('label');
    let stat1 = 84 / this.numItemPerRow;        // 96%
    let stat2 = 300 / this.numOfRowsPerTrayPhysical;
    if (this.numOfRowsPerTrayPhysical > 1) {
      for (let i = 0; i < squaresEle.length; i++) {
        const sqEle = squaresEle[i] as HTMLElement;
        const labelEle = labelsEle[i] as HTMLElement;
        // sqEle.style.height = `${width}%`;
        sqEle.style.height = `${stat2}px`;
        // sqEle.style.height = `50px`;
        sqEle.style.width = `${stat1}%`;
        labelEle.style.width = `${stat1}%`;
      }
    }
    for (let i = 0; i < squaresEle.length; i++) {
      let stat3 = 0.25 * this.numItemPerRow;
      const sqEle = squaresEle[i] as HTMLElement;
      sqEle.style.borderRadius = `${stat3}em`
    }
    // console.log(`numofRows : rows ${this.numOfRowsPerTrayPhysical}`)
    // console.log(`Final sq : ht ${stat2}px and wt ${stat1}%`)
  }

  public setTableHeight(row: number) : number {
    let gridHt = 150;
    if (row >= 2) {
      gridHt = 100*row;
    }
    return gridHt;
  }

  // public addRowsold(itemRow: any, total: any, arr: Array<any>): Array<any> {
  //   for (let i = 0; i < arr.length; i++) {
  //     if ((i + 1) % itemRow === 0) {
  //       arr[i] = itemRow;
  //
  //       if (Math.floor(i / this.numItemPerRow) == 0) {
  //         const rowNumber = '1';
  //         // this.wpList[i].wpId = i + 1;
  //         this.wpList[i].wpName = "[" + rowNumber + "," + (itemRow).toString() + "]";
  //       } else if (Math.floor(i / this.numItemPerRow) == 1) {
  //         const rowNumber = '2';
  //         this.wpList[i].wpId = i + 1;
  //         this.wpList[i].wpName = "[" + rowNumber + "," + (itemRow).toString() + "]";
  //       } else if (Math.floor(i / this.numItemPerRow) == 2) {
  //         const rowNumber = '3';
  //         this.wpList[i].wpId = i + 1;
  //         this.wpList[i].wpName = "[" + rowNumber + "," + (itemRow).toString() + "]";
  //       }
  //
  //     } else {
  //       arr[i] = (i + 1) % itemRow;
  //
  //       if (Math.floor(i / this.numItemPerRow) == 0) {
  //         const rowNumber = '1';
  //         this.wpList[i].wpId = i + 1;
  //         this.wpList[i].wpName = "[" + rowNumber + "," + ((i + 1) % itemRow).toString() + "]";
  //       } else if (Math.floor(i / this.numItemPerRow) == 1) {
  //         const rowNumber = '2';
  //         this.wpList[i].wpId = i + 1;
  //         this.wpList[i].wpName = "[" + rowNumber + "," + ((i + 1) % itemRow).toString() + "]";
  //       } else if (Math.floor(i / this.numItemPerRow) == 2) {
  //         const rowNumber = '3';
  //         this.wpList[i].wpId = i + 1;
  //         this.wpList[i].wpName = "[" + rowNumber + "," + ((i + 1) % itemRow).toString() + "]";
  //       }
  //     }
  //   }
  //   return arr;
  // }

  public addRows(numItemPerTray: number, list: Array<IWorkPiece>) {
    for (let i = 0; i < numItemPerTray; i++) {
      list[i].wpId = i + 1;
      list[i].wpName = this.wpNameArray[i];
    }
  }

  public addRowforVD(numItemPerTray: number, list: Array<IWorkPiece>) {
    for (let i = 0; i < numItemPerTray; i++) {
      list[i].wpId = 800 + i + 1;
      list[i].wpName = this.wpNameArray[i];
    }
  }

  public onSelect(thisWorkOrder: any) {
    //scroll to top
    topFunction();
    this.isViewDetailsClicked = true;
    this.current = 1;
    //for visualisation, create the tray layout
    /* this.vdWpList = new Array<IWorkPiece>(thisWorkOrder.workpieces.length);
     this.numOfRowsPerVdTrayPhysical = this.vdWpList.length /  this.numItemPerRow;*/
    /*this.vdRowArr = createArray(this.numOfRowsPerVdTrayPhysical);
    for (let i = 0; i < this.vdWpList.length; i++) {
      let pcsVdWp = {
        wpId: 0,
        wpName: '',
        processingStatus: 0,
        predictResult: '',
        isOOS: false
      }
      this.vdWpList[i] = pcsVdWp;
    }
    this.addRowforVD(thisWorkOrder.workpieces.length, this.vdWpList);
    this.setTrayForVD(thisWorkOrder);*/
    this.vdTitle = thisWorkOrder.id;

    // this.setVdtrayLayout(thisWorkOrder);
    let numOfRows = Math.ceil((thisWorkOrder.workpieces.length*this.predictionBatchSize / this.numItemPerVdRow));
    if (numOfRows >= this.numOfRowsPerVdTrayPhysical)
    {
      this.vdRowArr = createArray(numOfRows);
    }else {
      this.vdRowArr = createArray(this.numOfRowsPerVdTrayPhysical);
    }
    this.items  = JSON.parse(JSON.stringify(thisWorkOrder.workpieces));
    this.total = Math.ceil(this.items.length*this.predictionBatchSize/this.numItemPerVdTray);
    this.searchMLModelByTopicforVd(thisWorkOrder.topic);
    this.moreThanOneTray = thisWorkOrder.workpieces.length*this.predictionBatchSize > this.numItemPerVdTray;
    if (this.moreThanOneTray === true ) {
      this.itemsToDisplay = this.paginate(this.current, this.numItemPerVdTray, this.predictionBatchSize);
      this.setTrayForVDPg(this.itemsToDisplay);
    }else {
      this.setTrayForVDPg(thisWorkOrder);
    }
  }

  closeVD(): void {
    // clear pagination
    this.moreThanOneTray = false;
    // this.addRows(this.numItemPerVdTray, this.vdWpList);
    this.isViewDetailsClicked = false;
    this.clearTray(this.vdWpList);
    window.scroll(0, 0); // return to top of window
  }

  // public setVdtrayLayout(machineId?: string) {
  //   // for visualisation, create the tray layout for view details overlay -- ws
  //   switch (this.machineID) {
  //     default:
  //       // this.wpList =  this.createTrayLayout(this.numOfRowsPerTrayPhysical, this.numItemPerRow);
  //       this.vdWpList = this.createTrayLayout(this.numItemPerVdTray, 'vd');
  //   }
  // }



  public colorCode(code: string): SafeStyle {
    if (parseInt(code) < this.HardCodedTargetRate) {
      this.resultRow = '#FF0000';
    } else {
      this.resultRow = 'transparent';
    }
    return this.sanitizer.bypassSecurityTrustStyle(this.resultRow);
  }

  private createTrayLayout( wpNumInTray: number, str?: string): Array<any> {
    let count = 1;
    if (str !== undefined) {
      // return Array.from({length: numOfRowsPerTrayPhysical}, () => Array.from({length: wpNumInTray}, () => `${str}${count++}`));
      return Array.from({length: wpNumInTray}, () => `${str}${count++}`);
    }
    // return Array.from({length: numOfRowsPerTrayPhysical}, () => Array.from({length: wpNumInTray}, () => count++));
  }

  private calcDashboardTime(): string {

    let localeId = this.translate.currentLang;
    let d = new Date();
    d.setHours(8);
    d.setMinutes(0);
    d.setSeconds(0);
    d.setMilliseconds(0);
    let startdatetime = d.getTime();

    let curTime = new Date().getTime();

    if (curTime < startdatetime) {
      let startdate = new Date((startdatetime - (24 * 60 * 60 * 1000)));
      let curdate = new Date(curTime);

      return startdate.toLocaleString(localeId, {
        year: 'numeric',
        month: 'short',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
      }) + ' - ' + curdate.toLocaleString(localeId, {
        year: 'numeric',
        month: 'short',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
      });

      //let pipe = new DatePipe("fr-FR");
      //let dashboarddate = pipe.transform(startdate);
      //return dashboarddate;

    } else {
      //let options = {year: 'numeric', month: 'short', day: 'numeric'};
      let startdate = new Date(startdatetime);
      let curdate = new Date(curTime);

      return startdate.toLocaleString(localeId, {
        year: 'numeric',
        month: 'short',
        day: 'numeric',
        hour: "numeric",
        minute: 'numeric',
        second: "numeric"
      }) + ' - ' + curdate.toLocaleString(localeId, {
        year: 'numeric',
        month: 'short',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
      });

      // let localeId = "zh-CN";
      // let pipe = new DatePipe(localeId);
      // let dashboarddate = pipe.transform(startdate, localeId, "yyyy-MM-dd hh:mm:ss");
      // return dashboarddate;
    }
  }

  private CalcChartYaxisMax(data: any[], targetline: number) {
    let maxYaxisRealTimeChart: number

    let max = Math.max(...data[0].data);
    if (max >= targetline) {
      maxYaxisRealTimeChart = Math.round(max + 0.2 * max);
    } else {
      maxYaxisRealTimeChart = Math.round(targetline + 0.2 * targetline);
    }

    return maxYaxisRealTimeChart;
  }

  private CalcChartYaxisMin(data: any[], targetline: number) {
    let minYaxisRealTimeChart: number

    let min = Math.min(...data[0].data);
    if (min <= targetline) {
      minYaxisRealTimeChart = Math.round(min - 0.2 * min);
    } else {
      minYaxisRealTimeChart = Math.round(targetline - 0.2 * targetline)
    }
    if (minYaxisRealTimeChart <= 0) {
      minYaxisRealTimeChart = 0;
    }

    return minYaxisRealTimeChart;
  }

  private setColorPredictionResult(prediction: any, predictionType: string) {
    console.log('enter setColorPredictionResult ');
    if (prediction.woId === this.prevWO) {
      // get length of WO to date
      let index = this.predHistTableDs.length -1 ;
      this.wpCounter = this.predHistTableDs[index].workpieces.length;
      if (this.wpCounter % this.numItemPerTray === 0) {
        this.clearTray(this.wpList);
        this.setTrayLivePred(prediction, 0);
      }else {
        this.setTrayLivePred(prediction, this.wpCounter);
      }
    } else {
      this.workOrderNo = prediction.woId;
      this.clearTray(this.wpList);
      this.setTrayLivePred(prediction, 0);
    }
    console.log('exit setColorPredictionResult ');
  }

  
  private setTrayOnPgLoad(thisPred: any): boolean {

    let notFirstTray = false
    let lastIndex = thisPred.length - 1;

    // check if pred length is more than 1
    if (thisPred.length > 0) {
      this.setTray(thisPred[lastIndex], thisPred[lastIndex].workpieces.length);
      this.searchMLModelByTopic(thisPred[lastIndex].topic)
      notFirstTray = true;
    }
    return notFirstTray;
  }

  // private setTray(predictionLastWO: any) {
  //
  //   this.workOrderNo = predictionLastWO.id;
  //
  //   let noOfCompletedPredictionsByWO = predictionLastWO.workpieces.length
  //
  //   let firstNPredictions
  //   if (noOfCompletedPredictionsByWO > 0) {
  //     if (noOfCompletedPredictionsByWO % 12 != 0) {
  //       firstNPredictions = Math.floor(noOfCompletedPredictionsByWO / 12) * 12
  //     } else {
  //       firstNPredictions = 0
  //     }
  //   } else {
  //     firstNPredictions = 0
  //   }
  //
  //   let lastTrayCompletedPredictions = predictionLastWO.workpieces.slice(firstNPredictions, noOfCompletedPredictionsByWO)
  //
  //
  //   if (lastTrayCompletedPredictions.length > 0) {
  //     lastTrayCompletedPredictions.forEach(x => {
  //       this.setWorkpieceColor(x.itemId, !x.predictResult)
  //     })
  //   }
  // }

  private setTray( prediction: any, wpCount?: number)   {

    this.workOrderNo = prediction.id;

    if (wpCount !== undefined)
    {
      this.wpCounter = wpCount;
    }else {
      this.wpCounter = 0;
    };

    // this.wpCounter = prediction.workpieces.length;
    console.log('end counter: ' + this.wpCounter);

    let numRowsPerTray = this.numItemPerTray / this.predictionBatchSize;

    const noOfCompletedPredictionsByWO = prediction.workpieces.length;

    let firstNPredictions;
    if (noOfCompletedPredictionsByWO > 0){
      if (noOfCompletedPredictionsByWO % numRowsPerTray !== 0) {
        firstNPredictions = Math.floor(noOfCompletedPredictionsByWO / numRowsPerTray) * numRowsPerTray;
      }
      else {
        firstNPredictions = 0;
      }
    }
    else {
      firstNPredictions = 0;
    }

    const lastTrayCompletedPredictions =  prediction.workpieces.slice(firstNPredictions, noOfCompletedPredictionsByWO);


    console.log('lastTrayCompletedPredictions: ' + lastTrayCompletedPredictions.length);

    let predictionCnt;
    for (predictionCnt = 1; predictionCnt <= lastTrayCompletedPredictions.length; predictionCnt++){

      let rowNo
      for (rowNo = 1; rowNo <= numRowsPerTray; rowNo++){
        if (Number.isInteger((predictionCnt - rowNo)/numRowsPerTray+1)) { break;}
      }

      let startId = 1 + (rowNo-1)* this.predictionBatchSize


      //injection molding
      //startId = 2, startId + this.predictionBatchSize = 4
      for (let p = startId; p < startId + this.predictionBatchSize; p++){
        let sqId = p;
        this.setWorkpieceColor(sqId.toString(), lastTrayCompletedPredictions[predictionCnt-1].isOOS);
      }

    }
  }


  private setTrayLivePred(prediction: any, wpCount?: number) {
    this.workOrderNo = prediction.woId;

    if (wpCount !== undefined) {
      this.wpCounter = wpCount + 1;
    } else {
      this.wpCounter = 0;
    };


    let numRowsPerTray = this.numItemPerTray / this.predictionBatchSize;

    let rowNo;
    for (rowNo = 1; rowNo <= numRowsPerTray; rowNo++) {
      if (Number.isInteger((this.wpCounter - rowNo) / numRowsPerTray + 1)) {
        break;
      }
    }

    let startId = 1 + (rowNo - 1) * this.predictionBatchSize;
    for (let p = startId; p < startId + this.predictionBatchSize; p++) {
      let sqId = p;
      if (sqId === 1) {
        this.wpList.forEach(function (item, index) {
          if (item.wpId != 1) {
            let square = document.getElementById(item.wpId.toString())
            square.classList.remove('fail', 'pass')
            square.classList.add('square')
          }
        })
      }
      this.setWorkpieceColor(sqId.toString(), prediction.isOOS);
    }
    console.log('end counter: ' + this.wpCounter);
  }


  private setTrayForVD( thisPred: any )   {
    this.wpCounter = 0;
    // this.lastNthEle = thisPred.workpieces;
    thisPred.workpieces.forEach (x => {
      let sqId = this.wpCounter + 1;
      this.setWorkpieceColor( (800+sqId).toString(), x.isOOS);
      this.wpCounter++;
    });
  }

  private setTrayForVDPg( thisPred: any )   {
    this.wpCounter = 0;
    let sqId = 0;
    this.clearTray(this.vdWpList);
    // this.lastNthEle = thisPred.workpieces;
    thisPred.forEach (x => {
      //let sqId = this.wpCounter + 1;
      //let p = sqId;
      for (let p=sqId; p < sqId + this.predictionBatchSize; p++){        
        this.setWorkpieceColor( (800+p+1).toString(), x.isOOS);
      }
    
      sqId = sqId + this.predictionBatchSize;
      //this.wpCounter++;
    });
  }

  /*  private setPagination(thisRowWp: any) {
      //for pagination in vd
      this.paginationControl.valueChanges.subscribe(this.onPageChange.bind(this));
      this.visibleItems = {
        items: thisRowWp.slice(0, this.numItemPerVdTray),
        total: thisRowWp.length,
      };
    }*/

  // pagination

  public onGoTo(page: number): void {
    this.current = page;
    this.itemsToDisplay = this.paginate(this.current, this.numItemPerVdTray, this.predictionBatchSize);
    this.setTrayForVDPg(this.itemsToDisplay);
  }

  public onNext(page: number): void {
    this.current = page + 1;
    this.itemsToDisplay = this.paginate(this.current, this.numItemPerVdTray, this.predictionBatchSize);
    this.setTrayForVDPg(this.itemsToDisplay);
  }

  public onPrevious(page: number): void {
    this.current = page - 1;
    this.itemsToDisplay = this.paginate(this.current, this.numItemPerVdTray, this.predictionBatchSize);
    this.setTrayForVDPg(this.itemsToDisplay);
  }

  public paginate(current: number, perPage: number, predictionBatchSize: number): any[] {
    return [...this.items.slice((current - 1) * perPage/predictionBatchSize).slice(0, perPage/predictionBatchSize)];
  }

  private setWorkpieceColor(id: string, isOOS: boolean){
    const square = document.getElementById(id);
    if (isOOS === true){
      square.classList.add('fail');
      square.classList.remove('pass');
    }
    else if (isOOS === false){
      square.classList.add('pass');
      square.classList.remove('fail');
    }
  }

  private updateStatsRow(pred: any) {
    this.populateRealTimeChart();
    this.predHistTableDs = this.setTable(pred);
  }

  private setTable(clonePredResultList: any[]): any[] {

    let predHistTableDs = [], tblWpList = [];
    let lastWorkOrder = 0, noOfWO = predHistTableDs.length;

    clonePredResultList = clonePredResultList.filter(p => p.machineId === this.machineId);

    for (let i = 0; i < clonePredResultList.length; i++) {
      // get WO id
      const workOrderNo = clonePredResultList[i].woId;

      // check WO exist. For 1st Wo
      if (predHistTableDs.length === 0) {
        // set first work order as the id
        const thisWorkOrder = createNewWO(clonePredResultList[i], workOrderNo, noOfWO, []);
        // add work order into ds
        predHistTableDs.push(thisWorkOrder);
        // create wp array
        tblWpList.push(
          {
            wpId: clonePredResultList[i].workpieceId,
            wpName: '',
            processingStatus: 0,
            predictResult: clonePredResultList[i].predictResult,
            isOOS: clonePredResultList[i].isOOS
          });
        // insert data
        predHistTableDs[0].workpieces = tblWpList;

        // if not the first entry
      } else {
        noOfWO = predHistTableDs.length;
        lastWorkOrder = predHistTableDs[(noOfWO - 1)].id;

        if (lastWorkOrder !== workOrderNo) {
          if (checkIfWOExist(predHistTableDs, workOrderNo) != null) {

            const indexOfWO = checkIfWOExist(predHistTableDs, workOrderNo);
            const thisWpList = predHistTableDs[indexOfWO].workpieces;
            thisWpList.push(
              {
                wpId: clonePredResultList[i].workpieceId,
                wpName: '',
                processingStatus: 0,
                predictResult: clonePredResultList[i].predictResult,
                isOOS: clonePredResultList[i].isOOS
              });
            predHistTableDs[indexOfWO].workpieces = thisWpList;
            // copy to new index
            predHistTableDs.push(predHistTableDs[indexOfWO]);
            predHistTableDs.splice(indexOfWO, 1);

          } else {
            const thisPred = clonePredResultList[i];
            const thisWorkOrder = createNewWO(thisPred, workOrderNo, noOfWO, []);
            // add work order into ds
            predHistTableDs.push(thisWorkOrder);
            // create wp array
            tblWpList = new Array<IWorkPiece>();
            tblWpList.push(
              {
                wpId: clonePredResultList[i].workpieceId,
                wpName: '',
                processingStatus: 0,
                predictResult: clonePredResultList[i].predictResult,
                isOOS: clonePredResultList[i].isOOS
              });
            // insert data
            predHistTableDs[noOfWO].workpieces = tblWpList;
          }
        } else {
          const thisWpList = predHistTableDs[noOfWO - 1].workpieces;
          thisWpList.push(
            {
              wpId: clonePredResultList[i].workpieceId,
              wpName: '',
              processingStatus: 0,
              predictResult: clonePredResultList[i].predictResult,
              isOOS: clonePredResultList[i].isOOS
            });
          predHistTableDs[noOfWO - 1].workpieces = thisWpList;
        }

      }
    }

    const lastWOIndex = predHistTableDs.length - 1;
    this.prevWO = predHistTableDs[lastWOIndex].id;

    for (let x = 0; x < predHistTableDs.length; x++) {
      setDetailsForWO(predHistTableDs, x, this.predictionBatchSize);
    }


    return predHistTableDs;
  }

  public extractNumber( str: string): number {
    const regex = /\d+/g;
    return str.match(regex) ? +str.match(regex)[0] : null;
  }

  public trackByFn (index, item) {
    return index;
  }

  // paginationold
  /*  public onPageChange(pagination: PaginationValue): void {
      const startIndex = (pagination.page - 1) * pagination.pageSize;
      let fullWOList = this.setTable(this.predictionResultList.filter(p => p.machineId === this.machineID));
      let thisWorkOrder = fullWOList[ checkIfWOExist(fullWOList, this.vdTitle)];
      const items = thisWorkOrder.workpieces.slice(
        startIndex,
        startIndex + pagination.pageSize
      );
      this.visibleItems = { items, total: thisWorkOrder.length };
      this.setTrayForVDPg(items);
    }*/
}

function createNewWO( thisPred: any, workOrderNo: any, noOfWOinArr: number, wpList: any ) : any {
  let thisWO = {
    id: workOrderNo,
    topic: thisPred.topic,
    status: 'Pending',
    startDt: formatDate(thisPred.createdDateTime, 'dd-MMM-yyyy hh:mm a', 'en_US'),
    passPredict: 0,
    failTotalQty: 'na',
    workpieces: wpList,
  }
  return thisWO;
}

function setDetailsForWO ( predHistTableDs: any, index: number, predictionBatchSize: number ) {
  let totalQty = predHistTableDs[index].workpieces.length*predictionBatchSize  ;

  predHistTableDs[index].status = 'Completed'
  let predCnt = predHistTableDs[index].workpieces.filter ( p => p.isOOS === false).length*predictionBatchSize;
  let failCnt = totalQty  - predCnt;
  // console.log(' last index pass of ' + workOrderNo + ' : ' + predCnt)
  // console.log('last index fail of ' + workOrderNo + ' : ' +failCnt + ', total : ' + predHistTableDs[lastIndex].totalqty)
  predHistTableDs[index].passPredict = toFixedPrecision ( ( predCnt / totalQty ) * 100, 2)
  predHistTableDs[index].failTotalQty = failCnt + ' / ' + totalQty
}

function checkIfWOExist ( predDs: any, thisWONum: any ) : number {
  let woExist: number = null;
  for (let w = 0; w < predDs.length; w++) {
    if (thisWONum === predDs[w].id)
      woExist = w;
  }
  return woExist;
}

function  createArray(n: number): number[] {
  let array = [];
  for (let i = 0; i < n; i++) {
    array.push(i);
  }
  return array;
}

function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}

