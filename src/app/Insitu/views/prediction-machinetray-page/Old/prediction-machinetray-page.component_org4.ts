import {Component, ElementRef, OnInit, Inject} from '@angular/core';
import { IconThemeColor} from '@progress/kendo-angular-icons/dist/es2015/common/models/theme-color';
import { IconModule} from '@progress/kendo-angular-icons';
import {setData} from '@progress/kendo-angular-intl';
import {TranslateService} from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import * as global from '../../global';
import { formatDate, DOCUMENT } from '@angular/common';
import { SETTINGS } from '../../settings/environment';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { HttpClient } from '@angular/common/http';
import {STATUS_CODES} from 'http';
import {totalmem} from 'os';
import { SequenceEqualSubscriber } from 'rxjs/internal/operators/sequenceEqual';
import {toFixedPrecision} from "@progress/kendo-angular-inputs/dist/es2015/common/math";
import {MlModelService} from '../../service/mlmodel/mlmodel.service';
import { MlConfigPageComponent } from "../ml-config-page/ml-config-page.component";
import {DomSanitizer, SafeStyle} from '@angular/platform-browser';

// import services and interfaces
import { IResponse, ModelFile, PreprocessingFile, ModelType } from "../../interface/ml-interfaces";
import {any} from "codelyzer/util/function";
import {EditEvent} from "@progress/kendo-angular-grid";

interface IMachineTray {

  workOrderNo: string;
  trayNumber: number;
  numItemPerTray: number;
  // workPieceList: WorkPiece[];
  rowSize: number;              // ws: how many wp each row contains
}

interface IWorkPiece {
  wpId: number;                       // ws: start from 1 to tray
  wpName: string;
  // statusColor: string = 'grey';
  processingStatus: number;         // 0:in queue, 1:completed
  predictResult: string;
  isOOS: boolean            //  1:fail , 2: pass
}

interface response {
  hasError: boolean;
  msg: string;
  data: any[];
}


@Component({
    selector: 'app-prediction-machinetray-page',
    templateUrl: './prediction-machinetray-page.component.html',
    styleUrls: ['./prediction-machinetray-page.component.scss']
})

export class PredictionMachinetrayPageComponent implements OnInit {

  public dashboardtime: string;
  public timer;
  public chartTimer;
  public pageTitle = '';
  public machineID: string;

  // for the 3 KPIs calculation
  public predictionResultList: any[] = [];
  public PredictedPassResult = '0';
  public PredictedFailResult = '0.00';
  public TotalFailCase = 0;
  public TotalCase = 0;
  public TotalFailOverTotalCase = '';

  // for real-time chart
  public realTimeChartSeries: any[];
  public categoriesRealTimeChart: string[] = [];
  public minYaxisRealTimeChart: number;
  public maxYaxisRealTimeChart: number;
  public minYaxisDailyYieldChart: number;
  public maxYaxisDailyYieldChart: number;
  public HardCodedTargetRate = 70;


  //for tray physical layout
  public numItemPerTray = 12;	//controls physical layout of tray
  public numItemPerRow = 4;	//controls physical layout of tray
  public numOfRowsPerTray: number = this.numItemPerTray/this.numItemPerRow ;  //not hardcoded, = numItemPerTray/numItemPerRow
  rowArray: Array<Number> = createArray(this.numOfRowsPerTray)//array size based on numOfRowsPerTray, from 0 to numOfRowsPerTray - 1

  public workOrderNo: string; // = 22092000005;
  public workPieceList: Array<IWorkPiece>;
  public wpList: Array<IWorkPiece>; // steven
  public elemId: string; // steven
  public predictResult: number;  // steven
  public hasRetrievedCompletedPredictions = false; // steven, should be false, set to true for testing
  public counter = 0;  // steven
  public completedPredictionsByWO: any[];  // steven
  public noCompletedPredictionsByWO: number; // steven, set to 0 for testing
  public noOfPredictionsCnt = 0; // steven
  public predictionBatchSize = 1;  // steven, hardcoded to 1 for laser welding
  public prevWO = '';
  public predictionType = '';
  public mlModel = '';
  public mlLibrary = '';
  public wpCounter = 0;
  public wpNameArray: Array<string> = ["[૮ ˶ᵔ ᵕ ᵔ˶ ა]", "[1,2]", "[1,3]", "[1,4]",
    "[2,1]", "[2,2]", "[2,3]", "[2,4]",
    "[3,1]", "[3,2]", "[3,3]", "[(♡´𓋰`♡)]",
  ]

  public highlightRow: boolean = false;

  // default configs  --ws
  public defaultWp: IWorkPiece = {
    wpId: 1,
    wpName: '',
    processingStatus: 0,
    predictResult: '',
    isOOS: false
  };

  public defaultTray: IMachineTray = {
    workOrderNo: '',
    trayNumber: 1,
    numItemPerTray: 12,
    rowSize: 4,
  };

  // for table  --ws
  public predHistTableDs: any[] = [];
  public status: any[] = [
    {
      kind: 'Pass',
      color: 'green'
    },
    {
      kind: 'Fail',
      color: 'red'
    },
    {
      kind: 'Pending',
      color: 'grey'
    },
    // {
    //     kind: 'Aborted',            // added by ws. NIU for now
    //     color: 'orange'
    // }
  ];
  public modelTypes: Array<ModelType>;
  public preprocessingFiles: Array<PreprocessingFile>;
  public resultRow = '';

  // for view details tray
  public isViewDetailsClicked = false;    // was false;
  public vdTitle = '';
  vdworkOrderNo: string; // = 22092000005;
  numItemPerVdTray = 12;
  numItemPerVdRow = 4;
  public vdWpList: Array<IWorkPiece>;
  public vdWorkPieceList: Array<IWorkPiece>;
  public vdElemId: string;
  public prevVdWO = '';
  public vdPredictionType = '';
  public vdMlModel = '';
  public vdMlLibrary = '';
  public vdInterval;
  public notFirstTray = false;

  // this is for the Model File
  public modelFiles: Array<ModelFile>;
  public defaultModelFile: ModelFile = {
    id: null,
    modelTypeId: null,
    modelFileName: 'Select model file'
  };

  // for % pass KPI
  public value = 10;
  public colors = [
    {
      to: 50,
      color: '#FF5733',
    },
    {
      from: 50,
      to: 75,
      color: '#FF981A',
    },
    {
      from: 75,
      color: '#4CBB17',
    },
  ];
  // for next batch prediction
  public futurePrediction: any[] = [];
  public nextBatchPredictedResults: any[] = [];
  public showNextBatchPrediction = false;
  public onPageLoad = false;  // default true
  public nextWorkpieceArray = [{
    curWorkpieceId: '11',
    nextWorkpiece: '12'
  },
    {
      curWorkpieceId: '12',
      nextWorkpiece: '13'
    },
    {
      curWorkpieceId: '13',
      nextWorkpiece: '14'
    },
    {
      curWorkpieceId: '14',
      nextWorkpiece: '21'
    },
    {
      curWorkpieceId: '21',
      nextWorkpiece: '22'
    },
    {
      curWorkpieceId: '22',
      nextWorkpiece: '23'
    },
    {
      curWorkpieceId: '23',
      nextWorkpiece: '24'
    },
    {
      curWorkpieceId: '24',
      nextWorkpiece: '31'
    },
    {
      curWorkpieceId: '31',
      nextWorkpiece: '32'
    },
    {
      curWorkpieceId: '32',
      nextWorkpiece: '33'
    },
    {
      curWorkpieceId: '33',
      nextWorkpiece: '34'
    }
  ];

  // public nextWorkpieceArray: any
  private _hubConnection: HubConnection;

  constructor(
    private sanitizer: DomSanitizer,
    private translate: TranslateService,
    private mlModelService: MlModelService,
    private route: ActivatedRoute,
    public http: HttpClient,
    private settings: SETTINGS,
    @Inject(DOCUMENT) private document: Document) {

    this.mlModelService.getAllModelTypes().subscribe(resp => {
        this.modelTypes = resp.data;
      }
    );

    this.mlModelService.getAllModelFiles().subscribe(resp =>
      this.modelFiles = resp.data);

    this.mlModelService.getPreprocessingFiles().subscribe(resp =>
      this.preprocessingFiles = resp.data);

    this.http.get<any>(settings.ENDPOINT.API + '/Database/Summary/GetCurrentPredictionResult').subscribe(prediction => {
      this.predictionResultList = prediction.data;
      this.CalculatePassRate(prediction.data);
      this.populateRealTimeChart();
      this.predHistTableDs = this.setTable(this.predictionResultList.filter(p => p.machineId === this.machineID));
      this.setTrayOnPgLoad(this.predHistTableDs);
    }, err => {
      console.log('Something went wrong ', err);
    });

  }

  ngOnInit(): void {

    this.setVdtrayLayout();

    this.route
      .queryParams
      .subscribe(params => {
        this.machineID = params['machineId'];
      });

    if (global.MachineID === '') {
      global.UpdateMachineID(this.machineID);
    } else {
      if (global.MachineID !== this.machineID) {
        global.resetMachineSeries();
        global.UpdateMachineID(this.machineID);
      }
    }

    //for visualisation, create the tray layout
    this.wpList = new Array<IWorkPiece>(this.numItemPerTray);
    for (let i = 0; i < this.numItemPerTray; i++) {
      let pcs = {
        wpId: 0,
        wpName: '',
        processingStatus: 0,
        predictResult: '',
        isOOS: false
      }
      this.wpList[i] = pcs;
    }

    this.addRows(this.numItemPerTray);

    //url = Server Url
    this._hubConnection = new HubConnectionBuilder().withUrl(this.settings.ENDPOINT.SIGNALR).build();
    this._hubConnection.start()
      .then(() => console.log('result page connection start'))
      .catch(err => {
        console.log('Error while establishing the connection');
      });

    this._hubConnection.on('BroadcastPredictionResult', (prediction) => {
      if (prediction.machineId === this.machineID) {
        console.log(prediction);

        const topic = prediction.topic.toString().toUpperCase();

        if (topic.includes('NADINE') || topic.includes('PARSNET')) {
          this.predictionType = 'Next Batch';
        } else {
          this.predictionType = 'Current Batch';
        }

        this.mlModel = prediction.modelName


        let notFirstTray = false;

        this.setColorPredictionResult(prediction, this.predictionType);
        this.predictionResultList.push(prediction);
        this.CalculatePassRate(this.predictionResultList);
        // this.updateStatsRow(prediction);
        this.updateStatsRow(this.predictionResultList);

        if (this.predictionType === 'Next Batch') {

          let workpieceId = String(prediction.workpieceId);
          workpieceId = '[' + workpieceId.substring(0, 1) + ',' + workpieceId.substring(1, 2) + ']';
          prediction.workpieceId = workpieceId;
          this.futurePrediction.push(prediction);
          this.nextBatchPredictedResults = [...this.futurePrediction];
          this.showNextBatchPrediction = true;
        } else {
          // this.nextBatchPredictedResults = []
          this.showNextBatchPrediction = false;
        }
      }
    })

    //timer to update the dashboard time every 1s
    this.timer = setInterval(() => {
      this.dashboardtime = this.createPageSubtitle();
    }, 1000);

    //timer to update the real-time predicted yield chart every 15s
    this.chartTimer = setInterval(() => {
      if (new Date() >= global.nextMachineCleanUpTime) {
        global.updateNextMachineCleanUpTime(this.calculateNextCleanUpTime());
        global.resetMachineSeries();
      }

      // if(global.machineSeries[0].data.length === 0 || this.PredictedPassResult !== global.machineSeries[0].data[global.machineSeries[0].data.length-1])
      // {
      this.populateRealTimeChart();
      //}

      //this.populateDailyChart();
    }, 15000);


    this.timer = setInterval(() => {
      this.dashboardtime = this.createPageSubtitle();
    }, 1000);

    this.pageTitle = this.createPageTitle()
  }

  ngAfterViewInit(): void{
    this.setSquareHeight();
  }

  ngOnDestroy(): void {

  }

  searchMLModelByTopic(searchTop: string): void {

    this.mlModelService.getAllModelTypes().subscribe(resp => {
        this.modelTypes = resp.data;
      }
    );

    this.mlModelService.getAllModelFiles().subscribe(resp =>
      this.modelFiles = resp.data);

    this.mlModelService.getPreprocessingFiles().subscribe(resp =>
      this.preprocessingFiles = resp.data);


    let topic: string = searchTop;
    let modelType: number = null;
    let modelFile: number = null;

    let modelList: any[] = null;

    const headers = {'Content-Type': 'application/json; charset=utf-8'};
    const body = {
      Topic: topic,
      ModelTypeId: modelType,
      ModelFileId: modelFile,
    };

    this.http.post<response>(this.settings.ENDPOINT.MLMODEL + '/MLModel/SearchMLModel', JSON.stringify(body), {headers}).subscribe(resp => {
      modelList = resp.data;
      // set tray
      this.predictionType = 'Current Batch'
      this.mlLibrary = modelList[0].library
      this.mlModel = modelList[0].modelName
    });
  }

  public createPageTitle(): string {
    return 'Quality Dashboard: ' + this.machineID;
  }

  public createPageSubtitle(): string {
    return this.calcDashboardTime();
  }

  public CalculatePassRate(prediction: any) {

    if (prediction.length > 0) {
      this.TotalCase = prediction.filter(p => p.machineId === this.machineID).length;
      this.TotalFailCase = prediction.filter(p => p.isOOS == true && p.machineId === this.machineID).length;
      this.TotalFailOverTotalCase = this.TotalFailCase.toString() + '/' + this.TotalCase.toString();
      this.PredictedPassResult = Math.round(((this.TotalCase - this.TotalFailCase) / this.TotalCase * 100)).toString();
      this.PredictedFailResult = (this.TotalFailCase / this.TotalCase * 100).toFixed(2).toString();
    }

    // const DistinctProcess = prediction.filter(p => p.machineId === this.machineID).map(item => item.processId).filter((value, index, self) => self.indexOf(value) === index);
    // DistinctProcess.forEach( (process) => {
    //   let pr = new PassRateWithProcessId();
    //   const TotalResult = prediction.filter(p => p.machineId === this.machineID && p.processId === process);
    //   const TotalPassResult = prediction.filter(p => p.isOOS == false && p.machineId === this.machineID && p.processId === process);
    //   const rate = TotalPassResult.length / TotalResult.length * 100;
    //   pr.machineId = this.machineID;
    //   pr.processId = process;
    //   pr.passRate = rate.toFixed(2).toString() + "%";
    //   pr.targetPassRate = this.HardCodedTargetRate + "%"
    //   this.passRateWithProcessIdList.push(pr);
    // });
  }

  public calculateNextCleanUpTime(): Date {

    let currentDate = new Date();
    if (currentDate.getHours() >= Number(global.DashStartTime)) {
      currentDate.setDate(currentDate.getDate() + 1);
    }

    return new Date(formatDate(currentDate, 'yyyy-MM-dd', 'en_US') + ' ' + global.DashStartTime + ':00:00');
  }

  public populateRealTimeChart() {
    const myClonedArray = [];

    let localeId = this.translate.currentLang;

    global.machineSeries.forEach(val => myClonedArray.push(Object.assign({}, val)));

    if (myClonedArray[0].data.length == 6) {
      myClonedArray[0].data.shift();
      myClonedArray[0].data.push(this.PredictedPassResult);

      //calculate y-axis min value
      this.minYaxisRealTimeChart = Math.floor(this.CalcChartYaxisMin(myClonedArray, this.HardCodedTargetRate) / 10) * 10;

      //calculate y-axis max value
      this.maxYaxisRealTimeChart = Math.ceil(this.CalcChartYaxisMax(myClonedArray, this.HardCodedTargetRate) / 10) * 10;

      myClonedArray[1].data.shift();
      myClonedArray[1].data.push(this.HardCodedTargetRate);

      this.realTimeChartSeries = myClonedArray;

      this.categoriesRealTimeChart = [...global.categoryRealTimeMachinePg];
      let currentDate = new Date();
      this.categoriesRealTimeChart.shift();
      this.categoriesRealTimeChart.push(currentDate.toLocaleString(localeId, {
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
      }));
      global.categoryRealTimeMachinePg.shift();
      global.categoryRealTimeMachinePg.push(currentDate.toLocaleString(localeId, {
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
      }));
    } else {
      myClonedArray[0].data.push(this.PredictedPassResult);
      myClonedArray[1].data.push(this.HardCodedTargetRate);
      this.realTimeChartSeries = myClonedArray;

      //calculate y-axis min value
      this.minYaxisRealTimeChart = Math.floor(this.CalcChartYaxisMin(myClonedArray, this.HardCodedTargetRate) / 10) * 10;

      //calculate y-axis max value
      this.maxYaxisRealTimeChart = Math.ceil(this.CalcChartYaxisMax(myClonedArray, this.HardCodedTargetRate) / 10) * 10;

      this.categoriesRealTimeChart = [...global.categoryRealTimeMachinePg];
      let currentDate = new Date();
      this.categoriesRealTimeChart.push(currentDate.toLocaleString(localeId, {
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
      }));
      global.categoryRealTimeMachinePg.push(currentDate.toLocaleString(localeId, {
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
      }));

    }
  }

  private setSquareHeight( ) {
    if (this.numOfRowsPerTray > 3) {
      let squaresEle = document.getElementsByClassName('square');
      let labelsEle = document.getElementsByClassName('label');
      let hwt = 100 / this.numOfRowsPerTray;
      // let hwt = 10;
      for (let i = 0; i < squaresEle.length; i++) {
        const sqEle = squaresEle[i] as HTMLElement;
        const labelEle = labelsEle[i] as HTMLElement;
        sqEle.style.height = `${hwt}%`;
        sqEle.style.width = `${hwt}%`;
        labelEle.style.width = `${hwt}%`;
      }
    }
  }

  public setTableHeight(row: number) : number {
    let gridHt = 150;
    if (row >= 2) {
      gridHt = 100*row;
    }
    return gridHt;
  }


  public addRows(numItemPerTray: number) {

    for (let i = 0; i < numItemPerTray; i++) {
      this.wpList[i].wpId = i + 1;
      this.wpList[i].wpName = this.wpNameArray[i];
    }
  }


  public onSelect(thisWorkOrder: any) {
    this.isViewDetailsClicked = true;
    this.vdTitle = thisWorkOrder.id;
    this.setTrayForVD(thisWorkOrder);
  }

  closeVD(): void {
    this.isViewDetailsClicked = false;
    this.setVdtrayLayout();       // reset lol
    window.scroll(0, 0); // return to top of window
  }

  public setVdtrayLayout(machineId?: string) {
    // for visualisation, create the tray layout for view details overlay -- ws
    switch (this.machineID) {
      default:
        this.wpList =  this.createTrayLayout(this.numOfRowsPerTray, this.numItemPerRow);
        this.vdWpList = this.createTrayLayout(this.numOfRowsPerTray, this.numItemPerRow, 'vd');
    }
  }

  searchMLModelByTopicforVd( searchTop: string  ): void  {

    this.mlModelService.getAllModelTypes().subscribe(resp => {
        this.modelTypes = resp.data;
      }
    );

    this.mlModelService.getAllModelFiles().subscribe(resp =>
      this.modelFiles = resp.data);

    this.mlModelService.getPreprocessingFiles().subscribe(resp =>
      this.preprocessingFiles = resp.data);


    const topic: string = searchTop;
    const modelType: number = null;
    const modelFile: number = null;

    let modelList: any[] = null;

    const headers = { 'Content-Type': 'application/json; charset=utf-8' };
    const body = {
      Topic:  topic,
      ModelTypeId: modelType,
      ModelFileId: modelFile,
    };

    this.http.post<response>(this.settings.ENDPOINT.MLMODEL + '/MLModel/SearchMLModel', JSON.stringify(body), { headers }).subscribe(resp => {
      modelList = resp.data;
      // set tray
      this.vdPredictionType = 'Current Batch';
      this.vdMlModel = modelList[0].modelName;
      this.vdMlLibrary = modelList[0].library;
    });
  }


  public clearTray() {
    this.wpList.forEach(function (item, index) {
      let square = document.getElementById(item.wpId.toString())
      square.classList.remove('fail', 'pass')
      square.classList.add('square')

    })
  }

  public colorCode(code: string): SafeStyle {
    if (parseInt(code) < this.HardCodedTargetRate) {
      this.resultRow = '#FF0000';
    } else {
      this.resultRow = 'transparent';
    }
    return this.sanitizer.bypassSecurityTrustStyle(this.resultRow);
  }

  private createTrayLayout(numOfRowsPerTray: number, wpNumInTray: number, str?: string): Array<any> {
    let count = 1;
    if (str !== undefined) {
      return Array.from({length: numOfRowsPerTray}, () => Array.from({length: wpNumInTray}, () => `${str}${count++}`));
    }
    return Array.from({length: numOfRowsPerTray}, () => Array.from({length: wpNumInTray}, () => count++));
  }

  private calcDashboardTime(): string {

    let localeId = this.translate.currentLang;
    let d = new Date();
    d.setHours(8);
    d.setMinutes(0);
    d.setSeconds(0);
    d.setMilliseconds(0);
    let startdatetime = d.getTime();

    let curTime = new Date().getTime();

    if (curTime < startdatetime) {
      let startdate = new Date((startdatetime - (24 * 60 * 60 * 1000)));
      let curdate = new Date(curTime);

      return startdate.toLocaleString(localeId, {
        year: 'numeric',
        month: 'short',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
      }) + ' - ' + curdate.toLocaleString(localeId, {
        year: 'numeric',
        month: 'short',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
      });

      //let pipe = new DatePipe("fr-FR");
      //let dashboarddate = pipe.transform(startdate);
      //return dashboarddate;

    } else {
      //let options = {year: 'numeric', month: 'short', day: 'numeric'};
      let startdate = new Date(startdatetime);
      let curdate = new Date(curTime);

      return startdate.toLocaleString(localeId, {
        year: 'numeric',
        month: 'short',
        day: 'numeric',
        hour: "numeric",
        minute: 'numeric',
        second: "numeric"
      }) + ' - ' + curdate.toLocaleString(localeId, {
        year: 'numeric',
        month: 'short',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
      });

      // let localeId = "zh-CN";
      // let pipe = new DatePipe(localeId);
      // let dashboarddate = pipe.transform(startdate, localeId, "yyyy-MM-dd hh:mm:ss");
      // return dashboarddate;
    }
  }

  private CalcChartYaxisMax(data: any[], targetline: number) {
    let maxYaxisRealTimeChart: number

    let max = Math.max(...data[0].data);
    if (max >= targetline) {
      maxYaxisRealTimeChart = Math.round(max + 0.2 * max);
    } else {
      maxYaxisRealTimeChart = Math.round(targetline + 0.2 * targetline);
    }

    return maxYaxisRealTimeChart;
  }

  private CalcChartYaxisMin(data: any[], targetline: number) {
    let minYaxisRealTimeChart: number

    let min = Math.min(...data[0].data);
    if (min <= targetline) {
      minYaxisRealTimeChart = Math.round(min - 0.2 * min);
    } else {
      minYaxisRealTimeChart = Math.round(targetline - 0.2 * targetline)
    }
    if (minYaxisRealTimeChart <= 0) {
      minYaxisRealTimeChart = 0;
    }

    return minYaxisRealTimeChart;
  }

  private setColorPredictionResult(prediction: any, predictionType: string) {
    console.log('enter setColorPredictionResult ');

    if (prediction.batchId === this.prevWO) {
      // get length of WO to date
      this.setTrayLivePred(prediction, this.wpCounter);
    } else {
      this.workOrderNo = prediction.batchId;
      this.clearTray();
      this.setTrayLivePred(prediction, 0);
      console.log('exit setColorPredictionResult ');

    }
  }

  private setTrayOnPgLoad(thisPred: any): boolean {

    let notFirstTray = false
    let lastIndex = thisPred.length - 1;

    // check if pred length is more than 1
    if (thisPred.length > 0) {
      this.setTray(thisPred[lastIndex], thisPred[lastIndex].workpieces.length);
      this.searchMLModelByTopic(thisPred[lastIndex].topic)
      notFirstTray = true;
    }
    return notFirstTray;
  }


  private setTray( prediction: any, wpCount?: number)   {

    this.workOrderNo = prediction.id;

    if (wpCount !== undefined)
    {
      this.wpCounter = wpCount;
    }else {
      this.wpCounter = 0;
    };

    this.wpCounter = prediction.workpieces.length;
    console.log('end counter: ' + this.wpCounter);

    let numRowsPerTray = this.numItemPerTray / this.predictionBatchSize;

    const noOfCompletedPredictionsByWO = prediction.workpieces.length;

    let firstNPredictions;
    if (noOfCompletedPredictionsByWO > 0){
      if (noOfCompletedPredictionsByWO % numRowsPerTray !== 0) {
        firstNPredictions = Math.floor(noOfCompletedPredictionsByWO / numRowsPerTray) * numRowsPerTray;
      }
      else {
        firstNPredictions = 0;
      }
    }
    else {
      firstNPredictions = 0;
    }

    const lastTrayCompletedPredictions =  prediction.workpieces.slice(firstNPredictions, noOfCompletedPredictionsByWO);


    console.log('lastTrayCompletedPredictions: ' + lastTrayCompletedPredictions.length);

    let predictionCnt;
    for (predictionCnt = 1; predictionCnt <= lastTrayCompletedPredictions.length; predictionCnt++){

      let rowNo
      for (rowNo = 1; rowNo <= numRowsPerTray; rowNo++){
        if (Number.isInteger((predictionCnt - rowNo)/numRowsPerTray+1)) { break;}
      }

      let startId = 1 + (rowNo-1)* this.predictionBatchSize


      //injection molding
      //startId = 2, startId + this.predictionBatchSize = 4
      for (let p = startId; p < startId + this.predictionBatchSize; p++){
        let sqId = p;
        this.setWorkpieceColor(sqId.toString(), lastTrayCompletedPredictions[predictionCnt-1].isOOS);
      }

    }

  }

  private setTrayLivePred(prediction: any, wpCount?: number) {
    this.workOrderNo = prediction.batchId;

    if (wpCount !== undefined) {
      this.wpCounter = wpCount + 1;
    } else {
      this.wpCounter = 0;
    };


    let numRowsPerTray = this.numItemPerTray / this.predictionBatchSize;

    let rowNo;
    for (rowNo = 1; rowNo <= numRowsPerTray; rowNo++) {
      if (Number.isInteger((this.wpCounter - rowNo) / numRowsPerTray + 1)) {
        break;
      }
    }

    let startId = 1 + (rowNo - 1) * this.predictionBatchSize;
    for (let p = startId; p < startId + this.predictionBatchSize; p++) {
      let sqId = p;
      if (sqId === 1) {
        this.wpList.forEach(function (item, index) {
          if (item.wpId != 1) {
            let square = document.getElementById(item.wpId.toString())
            square.classList.remove('fail', 'pass')
            square.classList.add('square')
          }
        })
      }
      this.setWorkpieceColor(sqId.toString(), prediction.isOOS);
    }
    console.log('end counter: ' + this.wpCounter);
  }

  private addVDRows(itemRow: any, total: any, arr: Array<any>): Array<any> {
    for (let i = 0; i < arr.length; i++) {
      if ( (i + 1) % itemRow === 0) {
        arr[i] = itemRow;

        if (Math.floor(i / this.numItemPerRow) == 0){
          const rowNumber = 'vd1';
          this.vdWpList[i].wpName = rowNumber + (itemRow).toString();
        }
        else if (Math.floor(i / this.numItemPerRow) == 1){
          const rowNumber = 'vd2';
          this.vdWpList[i].wpName = rowNumber + (itemRow).toString();
        }
        else if (Math.floor(i / this.numItemPerRow) == 2){
          const rowNumber = 'vd3';
          this.vdWpList[i].wpName = rowNumber + (itemRow).toString();
        }
      }
      else {
        arr[i] = (i + 1) % itemRow ;

        if (Math.floor(i / this.numItemPerRow) == 0){
          const rowNumber = 'vd1';
          this.vdWpList[i].wpName = rowNumber + ((i + 1) % itemRow).toString();
        }
        else if (Math.floor(i / this.numItemPerRow) == 1){
          const rowNumber = 'vd2';
          this.vdWpList[i].wpName = rowNumber + ((i + 1) % itemRow).toString();
        }
        else if (Math.floor(i / this.numItemPerRow) == 2){
          const rowNumber = 'vd3';
          this.vdWpList[i].wpName = rowNumber + ((i + 1) % itemRow).toString();
        }
      }
    }
    return arr;
  }

  private setTrayForVD( thisPred: any )   {
    this.searchMLModelByTopicforVd(thisPred.topic);
    this.wpCounter = 0;
    thisPred.workpieces.forEach (x => {
      let sqId = this.wpCounter + 1;
      this.setWorkpieceColor( 'vd' + sqId.toString(), !x.isOOS);
      this.wpCounter++;
    });
  }

  private setWorkpieceColor(id: string, isOOS: boolean){
    const square = document.getElementById(id);
    if (isOOS === true){
      square.classList.add('fail');
      square.classList.remove('pass');
    }
    else if (isOOS === false){
      square.classList.add('pass');
      square.classList.remove('fail');
    }
  }

  private updateStatsRow(pred: any) {
    this.populateRealTimeChart();
    this.predHistTableDs = this.setTable(pred);
  }

  private setTable(clonePredResultList: any[]): any[] {

    let predHistTableDs = [], wpList = [];
    let lastWorkOrder = 0, noOfWO = predHistTableDs.length;

    clonePredResultList = clonePredResultList.filter(p => p.machineId === this.machineID);

    for (let i = 0; i < clonePredResultList.length; i++) {
      // get WO id
      const workOrderNo = clonePredResultList[i].batchId;

      // check WO exist. For 1st Wo
      if (predHistTableDs.length === 0) {
        // set first work order as the id
        const thisWorkOrder = createNewWO(clonePredResultList[i], workOrderNo, noOfWO, []);
        // add work order into ds
        predHistTableDs.push(thisWorkOrder);
        // create wp array
        wpList.push(
          {
            wpId: clonePredResultList[i].workpieceId,
            processingStatus: 1,
            predictResult: clonePredResultList[i].predictResult,
            isOOS: clonePredResultList[i].isOOS
          });
        // insert data
        predHistTableDs[0].workpieces = wpList;

        // if not the first entry
      } else {
        noOfWO = predHistTableDs.length;
        lastWorkOrder = predHistTableDs[(noOfWO - 1)].id;

        if (lastWorkOrder !== workOrderNo) {
          if (checkIfWOExist(predHistTableDs, workOrderNo) != null) {

            const indexOfWO = checkIfWOExist(predHistTableDs, workOrderNo);
            const thisWpList = predHistTableDs[indexOfWO].workpieces;
            thisWpList.push(
              {
                wpId: clonePredResultList[i].workpieceId,
                processingStatus: 1,
                predictResult: clonePredResultList[i].predictResult,
                isOOS: clonePredResultList[i].isOOS
              });
            predHistTableDs[indexOfWO].workpieces = thisWpList;
            // copy to new index
            predHistTableDs.push(predHistTableDs[indexOfWO]);
            predHistTableDs.splice(indexOfWO, 1);

          } else {
            const thisPred = clonePredResultList[i];
            const thisWorkOrder = createNewWO(thisPred, workOrderNo, noOfWO, []);
            // add work order into ds
            predHistTableDs.push(thisWorkOrder);
            // create wp array
            wpList = new Array<IWorkPiece>();
            wpList.push(
              {
                wpId: clonePredResultList[i].workpieceId,
                processingStatus: 0,
                predictResult: clonePredResultList[i].predictResult,
                isOOS: clonePredResultList[i].isOOS
              });
            // insert data
            predHistTableDs[noOfWO].workpieces = wpList;
          }
        } else {
          const thisWpList = predHistTableDs[noOfWO - 1].workpieces;
          thisWpList.push(
            {
              wpId: clonePredResultList[i].workpieceId,
              processingStatus: 1,
              predictResult: clonePredResultList[i].predictResult,
              isOOS: clonePredResultList[i].isOOS
            });
          predHistTableDs[noOfWO - 1].workpieces = thisWpList;
        }

      }
    }

    const lastWOIndex = predHistTableDs.length - 1;
    this.prevWO = predHistTableDs[lastWOIndex].id;

    for (let x = 0; x < predHistTableDs.length; x++) {
      setDetailsForWO(predHistTableDs, x);
    }

    return predHistTableDs;
  }

  public extractNumber( str: string): number {
    const regex = /\d+/g;
    return str.match(regex) ? +str.match(regex)[0] : null;
  }
}

function createNewWO( thisPred: any, workOrderNo: any, noOfWOinArr: number, wpList: any ) : any {
    let thisWO = {
        id: workOrderNo,
        topic: thisPred.topic,
        status: 'Pending',
        startDt: formatDate(thisPred.createdDateTime, 'dd-MMM-yyyy hh:mm a', 'en_US'),
        passPredict: 0,
        failTotalQty: 'na',
        workpieces: wpList,
    }
    return thisWO;
}

function setDetailsForWO ( predHistTableDs: any, index: number ) {
    let totalQty = predHistTableDs[index].workpieces.length  ;

    predHistTableDs[index].status = 'Completed'
    let predCnt = predHistTableDs[index].workpieces.filter ( p => p.isOOS === false).length;
    let failCnt = totalQty  - predCnt;
    predHistTableDs[index].passPredict = toFixedPrecision ( ( predCnt / totalQty ) * 100, 2)
    predHistTableDs[index].failTotalQty = failCnt + ' / ' + totalQty
}

function checkIfWOExist ( predDs: any, thisWONum: any ) : number {
    let woExist: number = null;
    for (let w = 0; w < predDs.length; w++) {
        if (thisWONum === predDs[w].id)
            woExist = w;
    }
    return woExist;
}

function createArray(n: number): number[] {
  let array = [];
  for (let i = 0; i <= n; i++) {
    array.push(i);
  }
  return array;
}