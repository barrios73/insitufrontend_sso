import { Component, OnInit } from '@angular/core';
import { IconThemeColor} from '@progress/kendo-angular-icons/dist/es2015/common/models/theme-color';
import { IconModule} from '@progress/kendo-angular-icons';
import {setData} from '@progress/kendo-angular-intl';
import {TranslateService} from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import * as global from '../../global';
import { formatDate } from '@angular/common';
import { ENDPOINT } from '../../settings/environment';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { HttpClient } from '@angular/common/http';
import { SequenceEqualSubscriber } from 'rxjs/internal/operators/sequenceEqual';
// import {  } from '@progress/kendo-svg-icons';

interface IWorkOrder {
  workOrderNo: number;
  totalItemsInWO: number;
  // remainingItems: number;
}

interface IMachineTray {
  numItemPerTray: number;
  numItemPerRow: number;
  // workPieceList: WorkPiece[];
  numItemPerColumn: number;
}

interface IWorkPiece {
  itemId: string;
  // statusColor: string = 'grey';
  processingStatus: number;
  predictResult: number;
}




class WorkOrder implements IMachineTray,IWorkOrder
{
  workOrderNo: number;
  totalItemsInWO: number;
  numItemPerTray: number;
  numItemPerColumn: number;
  numItemPerRow: number;
  // workPieceList: WorkPiece[];
  // remainingItems: number;
}



@Component({
  selector: 'app-prediction-machinetray-page',
  templateUrl: './prediction-machinetray-page.component.html',
  styleUrls: ['./prediction-machinetray-page.component.scss']
})
export class PredictionMachinetrayPageComponent implements OnInit {



  private _hubConnection: HubConnection;
  public dashboardtime: string;
  public timer;
  public chartTimer;
  public pageTitle: string = '';
  public machineID: string

  //for the 3 KPIs calculation
  public predictionResultList: any[]=[];
  public PredictedPassResult: string = '0';
  public PredictedFailResult: string = '0.00';
  public TotalFailCase: number = 0;
  public TotalCase: number = 0;
  public TotalFailOverTotalCase: string = '';


  //for real-time chart
  public realTimeChartSeries: any[];
  public categoriesRealTimeChart: string[] = [];
  public minYaxisRealTimeChart: number
  public maxYaxisRealTimeChart: number
  public minYaxisDailyYieldChart: number
  public maxYaxisDailyYieldChart: number
  public HardCodedTargetRate: number = 70;
  

  //for visualisation
  workpiece: IWorkPiece;
  workOrderNo: number //= 22092000005;
  numItemPerTray: number = 12;
  numItemPerRow: number = 4;
  public workPieceList: Array<IWorkPiece>;
  public thisWO: WorkOrder;

  public wpList: Array<IWorkPiece>; //steven
  public elemId: string //steven
  public predictResult: number  //steven
  public hasRetrievedCompletedPredictions: boolean = false //steven, should be false, set to true for testing
  public counter: number = 0  //steven
  public completedPredictionsByWO: any[]  //steven
  public noCompletedPredictionsByWO: number //steven, set to 0 for testing
  public noOfPredictionsCnt: number = 0 //steven
  public predictionBatchSize: number = 1  //steven, hardcoded to 1 for laser welding
  public prevWO: string = ''
  public predictionType: string = ''
  public mlModel: string = ''
  public mlLibrary: string = ''

  public status: any[] = [
    {
      kind: 'Pass',
      color: 'green'
    },
    {
      kind: 'Fail',
      color: 'red'
    },
    {
      kind: 'Processing',
      color: 'blue'
    },
    {
      kind: 'Pending',
      color: 'grey'
    },
  ];

  //for % pass KPI
  public value = 10;
  public colors = [
    {
      to: 50,
      color: "#FF5733",
    },
    {
      from: 50,
      to: 75,
      color: "#FF981A",
    },
    {
      from: 75,
      color: "#4CBB17",
    },
  ];

  //for next batch prediction
  public futurePrediction: any[] = []
  public nextBatchPredictedResults: any[] = []
  public showNextBatchPrediction: boolean = false
  //public nextWorkpieceArray: any

  public nextWorkpieceArray = [{"curWorkpieceId":"11",
                                "nextWorkpiece":"12"},
                              {"curWorkpieceId":"12",
                              "nextWorkpiece":"13"},
                              {"curWorkpieceId":"13",
                              "nextWorkpiece":"14"},
                              {"curWorkpieceId":"14",
                              "nextWorkpiece":"21"},
                              {"curWorkpieceId":"21",
                              "nextWorkpiece":"22"},
                              {"curWorkpieceId":"22",
                              "nextWorkpiece":"23"},
                              {"curWorkpieceId":"23",
                              "nextWorkpiece":"24"},
                              {"curWorkpieceId":"24",
                              "nextWorkpiece":"31"},
                              {"curWorkpieceId":"31",
                              "nextWorkpiece":"32"},
                              {"curWorkpieceId":"32",
                              "nextWorkpiece":"33"},
                              {"curWorkpieceId":"33",
                              "nextWorkpiece":"34"}
                          ]


  constructor(private translate: TranslateService,
    private route: ActivatedRoute,
    private http: HttpClient) {


      this.http.get<any>(ENDPOINT.API + '/Database/Summary/GetCurrentPredictionResult').subscribe(prediction => {
        this.predictionResultList = prediction.data;
        this.CalculatePassRate(prediction.data);
        this.populateRealTimeChart();
      }, err =>{
        console.log('Something went wrong ', err);
      })      
  }

  ngOnInit(): void {

    this.route
    .queryParams
    .subscribe(params => {
      this.machineID = params['machineId'];
    });

    if(global.MachineID === ''){
      global.UpdateMachineID(this.machineID);
    }
    else{
      if(global.MachineID !== this.machineID){
        global.resetMachineSeries();
        global.UpdateMachineID(this.machineID);
      }
    }

    //for visualisation, create the tray layout
    this.wpList = new Array<IWorkPiece>(this.numItemPerTray);
    for (let i = 0; i<this.numItemPerTray; i++){
      let pcs =  {
          itemId:'',
          processingStatus:0,
          predictResult:2
      }      
      this.wpList[i] = pcs;
    }

    let wpArray = new Array(12); //steven
    this.workPieceList = this.addRows(4,12, wpArray);


    //url = Server Url
    this._hubConnection = new HubConnectionBuilder().withUrl(ENDPOINT.SIGNALR).build();
    this._hubConnection.start()
      .then(() => console.log('result page connection start'))
      .catch(err => {
        console.log('Error while establishing the connection')
      });

    this._hubConnection.on('BroadcastPredictionResult', (prediction) => {
      
      console.log(prediction)

      let topic = prediction.topic.toString().toUpperCase();

      if (topic.includes('NADINE') || topic.includes('PARSNET')){
        this.predictionType = 'Next Batch'
      }
      else {
        this.predictionType = 'Current Batch'
      }

      this.mlModel = prediction.modelName
      this.mlLibrary = prediction.library

      this.setColorPredictionResult(prediction, this.predictionType);
      this.predictionResultList.push(prediction); 
      this.CalculatePassRate(this.predictionResultList);

      if (this.predictionType == 'Next Batch'){
        
        let workpieceId = String(prediction.workpieceId)
        workpieceId = "[" + workpieceId.substring(0,1) + "," + workpieceId.substring(1,2) + "]"
        prediction.workpieceId = workpieceId
        this.futurePrediction.push(prediction)
        this.nextBatchPredictedResults = [...this.futurePrediction]
        this.showNextBatchPrediction = true
      }
      else {
        //this.nextBatchPredictedResults = []
        this.showNextBatchPrediction = false
      }


      
    })

    //timer to update the dashboard time every 1s
    this.timer = setInterval(() => {
      this.dashboardtime = this.createPageSubtitle();
    }, 1000);

    //timer to update the real-time predicted yield chart every 15s
    this.chartTimer = setInterval(() => {
      if(new Date() >= global.nextMachineCleanUpTime){
        global.updateNextMachineCleanUpTime(this.calculateNextCleanUpTime());
        global.resetMachineSeries();
      }

      // if(global.machineSeries[0].data.length === 0 || this.PredictedPassResult !== global.machineSeries[0].data[global.machineSeries[0].data.length-1])
      // {
        this.populateRealTimeChart();
      //}

      //this.populateDailyChart();
    }, 15000);



    this.timer = setInterval(() => {
      this.dashboardtime = this.createPageSubtitle();
      }, 1000);

    this.pageTitle = this.createPageTitle();


    // ToDo: setTray (grey)

    //ToDo: change color base on



  }

  ngOnDestroy(): void {

  }

  
  public createPageTitle(): string{
    let title = 'Quality Dashboard: ' + this.machineID;
    return title;
  }

  public createPageSubtitle():string{
    return this.calcDashboardTime();
  }

  private calcDashboardTime(): string{
   
    let localeId = this.translate.currentLang;
    let d = new Date();
    d.setHours(8);
    d.setMinutes(0);
    d.setSeconds(0);
    d.setMilliseconds(0);
    let startdatetime = d.getTime();

    let curTime = new Date().getTime();
    
    if (curTime < startdatetime){
      let startdate = new Date((startdatetime - (24*60*60*1000)));
      let curdate = new Date(curTime);

      return startdate.toLocaleString(localeId, {year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric'}) + ' - ' + curdate.toLocaleString(localeId, {year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric'});
      
      //let pipe = new DatePipe("fr-FR");
      //let dashboarddate = pipe.transform(startdate);
      //return dashboarddate;

    }
    else{
      //let options = {year: 'numeric', month: 'short', day: 'numeric'};
      let startdate = new Date(startdatetime);
      let curdate = new Date(curTime);
      
      return startdate.toLocaleString(localeId, {year: 'numeric', month: 'short', day: 'numeric', hour: "numeric", minute: 'numeric', second: "numeric"}) + ' - ' + curdate.toLocaleString(localeId, {year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric'});
       
      // let localeId = "zh-CN";
      // let pipe = new DatePipe(localeId);
      // let dashboarddate = pipe.transform(startdate, localeId, "yyyy-MM-dd hh:mm:ss");
      // return dashboarddate;      
    }
  }


  public CalculatePassRate(prediction: any){

    if(prediction.length > 0){
      this.TotalCase = prediction.filter(p => p.machineId === this.machineID).length;
      this.TotalFailCase = prediction.filter(p => p.isOOS == true && p.machineId === this.machineID).length;
      this.TotalFailOverTotalCase = this.TotalFailCase.toString() + '/' + this.TotalCase.toString();
      this.PredictedPassResult = Math.round(((this.TotalCase - this.TotalFailCase) / this.TotalCase * 100)).toString();
      this.PredictedFailResult = (this.TotalFailCase / this.TotalCase * 100).toFixed(2).toString();
    }

    // const DistinctProcess = prediction.filter(p => p.machineId === this.machineID).map(item => item.processId).filter((value, index, self) => self.indexOf(value) === index);
    // DistinctProcess.forEach( (process) => {
    //   let pr = new PassRateWithProcessId();
    //   const TotalResult = prediction.filter(p => p.machineId === this.machineID && p.processId === process);
    //   const TotalPassResult = prediction.filter(p => p.isOOS == false && p.machineId === this.machineID && p.processId === process);
    //   const rate = TotalPassResult.length / TotalResult.length * 100;
    //   pr.machineId = this.machineID;
    //   pr.processId = process;
    //   pr.passRate = rate.toFixed(2).toString() + "%";
    //   pr.targetPassRate = this.HardCodedTargetRate + "%"
    //   this.passRateWithProcessIdList.push(pr);
    // });
  }


  public calculateNextCleanUpTime(): Date{

    let currentDate = new Date();
    if(currentDate.getHours() >= Number(global.DashStartTime)){
      currentDate.setDate(currentDate.getDate() + 1);
    }

    return new Date(formatDate(currentDate, 'yyyy-MM-dd', 'en_US') + ' ' + global.DashStartTime + ':00:00');
  }


  public populateRealTimeChart(){
    const myClonedArray = [];

    let localeId = this.translate.currentLang;

    global.machineSeries.forEach(val => myClonedArray.push(Object.assign({}, val)));

    if (myClonedArray[0].data.length == 6){
      myClonedArray[0].data.shift();
      myClonedArray[0].data.push(this.PredictedPassResult);

      //calculate y-axis min value
      this.minYaxisRealTimeChart = Math.floor(this.CalcChartYaxisMin(myClonedArray,this.HardCodedTargetRate)/10)*10;

      //calculate y-axis max value
      this.maxYaxisRealTimeChart = Math.ceil(this.CalcChartYaxisMax(myClonedArray,this.HardCodedTargetRate)/10)*10;

      myClonedArray[1].data.shift();
      myClonedArray[1].data.push(this.HardCodedTargetRate);

      this.realTimeChartSeries = myClonedArray;

      this.categoriesRealTimeChart = [...global.categoryRealTimeMachinePg];
      let currentDate = new Date();
      this.categoriesRealTimeChart.shift();
      this.categoriesRealTimeChart.push(currentDate.toLocaleString(localeId, {hour: 'numeric', minute: 'numeric', second: 'numeric'}));
      global.categoryRealTimeMachinePg.shift();
      global.categoryRealTimeMachinePg.push(currentDate.toLocaleString(localeId, {hour: 'numeric', minute: 'numeric', second: 'numeric'}));
    }
    else {
      myClonedArray[0].data.push(this.PredictedPassResult);
      myClonedArray[1].data.push(this.HardCodedTargetRate);
      this.realTimeChartSeries = myClonedArray;

      //calculate y-axis min value
      this.minYaxisRealTimeChart = Math.floor(this.CalcChartYaxisMin(myClonedArray,this.HardCodedTargetRate)/10)*10;

      //calculate y-axis max value
      this.maxYaxisRealTimeChart = Math.ceil(this.CalcChartYaxisMax(myClonedArray,this.HardCodedTargetRate)/10)*10;

      this.categoriesRealTimeChart = [...global.categoryRealTimeMachinePg];
      let currentDate = new Date();
      this.categoriesRealTimeChart.push(currentDate.toLocaleString(localeId, {hour: 'numeric', minute: 'numeric', second: 'numeric'}));
      global.categoryRealTimeMachinePg.push(currentDate.toLocaleString(localeId, {hour: 'numeric', minute: 'numeric', second: 'numeric'}));

    }

  }


  private CalcChartYaxisMax(data:any[],targetline: number){
    let maxYaxisRealTimeChart: number

    let max = Math.max(...data[0].data);
      if (max >= targetline){
        maxYaxisRealTimeChart = Math.round(max + 0.2*max);
      }
      else {
        maxYaxisRealTimeChart = Math.round(targetline + 0.2*targetline);
      }

      return maxYaxisRealTimeChart;
  }

  private CalcChartYaxisMin(data:any[],targetline: number){
    let minYaxisRealTimeChart: number

      let min = Math.min(...data[0].data);
      if (min <= targetline){
        minYaxisRealTimeChart = Math.round(min - 0.2*min);
      }
      else{
        minYaxisRealTimeChart = Math.round(targetline - 0.2*targetline)
      }
      if (minYaxisRealTimeChart <= 0){
        minYaxisRealTimeChart = 0;
      }

      return minYaxisRealTimeChart;
  }



  public addRows (itemRow: any, total: any, arr: Array<any>) : Array<any> {
    for (var i = 0; i < arr.length; i++) {
      if ( (i+1) % itemRow === 0) {
        arr[i] = itemRow;

        if (Math.floor(i / this.numItemPerRow) == 0){
          let rowNumber = '1';
          this.wpList[i].itemId = rowNumber + (itemRow).toString();  
        }
        else if (Math.floor(i / this.numItemPerRow) == 1){
          let rowNumber = '2';
          this.wpList[i].itemId = rowNumber + (itemRow).toString();  
        }
        else if (Math.floor(i / this.numItemPerRow) == 2){
          let rowNumber = '3';
          this.wpList[i].itemId = rowNumber + (itemRow).toString();  
        }

      }
      else {
        arr[i] = (i + 1) % itemRow ;

        if (Math.floor(i / this.numItemPerRow) == 0){
          let rowNumber = '1';
          this.wpList[i].itemId = rowNumber + ((i + 1) % itemRow).toString();  
        }
        else if (Math.floor(i / this.numItemPerRow) == 1){
          let rowNumber = '2';
          this.wpList[i].itemId = rowNumber + ((i + 1) % itemRow).toString();  
        }
        else if (Math.floor(i / this.numItemPerRow) == 2){
          let rowNumber = '3';
          this.wpList[i].itemId = rowNumber + ((i + 1) % itemRow).toString();  
        }

        console.log('This Array: ' + arr);

      }
    }
    return arr;
  }


  private setColorPredictionResult(prediction: any, predictionType: string){

    this.workOrderNo = prediction.batchId

    //when this page is loaded, we do not know if there are completed predictions for the WO
    //of current prediction. So we set hasRetrievedCompletedPRedictions = false to check
    if (this.hasRetrievedCompletedPredictions == false){
      //this.counter++
      
      //get completed predictions for WO of current prefrom dB

      this.completedPredictionsByWO = this.predictionResultList.filter(x => x.topic == prediction.topic && x.batchId == prediction.batchId && x.processId == prediction.processId )
      let noOfCompletedPredictionsByWO = this.completedPredictionsByWO.length     //5, 12, 16
      
      let firstNPredictions
      if (noOfCompletedPredictionsByWO > 0){
        if (noOfCompletedPredictionsByWO % 12 != 0){
        // noOfCompletedPredictionsByWO = 5,12, 16

          firstNPredictions = Math.floor(noOfCompletedPredictionsByWO / 12)*12
          //firstNPredictions = 0,12 (this is for 16)
        } 
        else {
          firstNPredictions = 0
        }  
      }
      else {
        firstNPredictions = 0
      }

      let lastTrayCompletedPredictions =  this.completedPredictionsByWO.slice(firstNPredictions, noOfCompletedPredictionsByWO)
      

      





      //////////////////////////////////////////////////////////////////////////////////
      //check this lastTrayCompletedPredictions.length + 1, correct?????????????????????
      //this.noOfPredictionsCnt = lastTrayCompletedPredictions.length + 1   
      this.noOfPredictionsCnt = noOfCompletedPredictionsByWO + 1











      //set colour for completed predictions
      if (lastTrayCompletedPredictions.length > 0){
        lastTrayCompletedPredictions.forEach(x => {
          this.setWorkpieceColor(x.workpieceId, x.isOOS)
        })  
      }

      //set colour for incoming prediction
      if (predictionType == "Next Batch"){
        if (prediction.workpieceId != '34'){
          let id = this.nextWorkpieceArray.filter(x => x.curWorkpieceId == prediction.workpieceId)[0].nextWorkpiece
          this.setWorkpieceColor(id, prediction.isOOS)
        }
      }
      else {
        this.setWorkpieceColor(prediction.workpieceId, prediction.isOOS)
      }


      this.hasRetrievedCompletedPredictions = true
      this.prevWO = prediction.batchId;
    }
    else {
      if (prediction.batchId == this.prevWO){
        //this.noOfPredictionsCnt = this.noCompletedPredictionsByWO + this.counter
        this.noOfPredictionsCnt = this.noOfPredictionsCnt + 1 //this.counter
              
        //modulo result is whole number --> reach the last piece of tray e.g. 12th, 24th workpiece  
        if ((this.noOfPredictionsCnt-1) % (this.numItemPerTray / this.predictionBatchSize) == 0){
      
          //turn current workpiece colour according to result, rest grey colour
          if (predictionType == "Next Batch"){
            if (prediction.workpieceId != '34'){
              let id = this.nextWorkpieceArray.filter(x => x.curWorkpieceId == prediction.workpieceId)[0].nextWorkpiece
              this.setWorkpieceColor(id, prediction.isOOS)
            }            
          }
          else {
            this.setWorkpieceColor(prediction.workpieceId, prediction.isOOS)
          }

          //set the rest to grey colour
          this.wpList.forEach(function(item, index){
            if (predictionType == 'Current Batch'){
              if (item.itemId != prediction.workpieceId){
                let square = document.getElementById(item.itemId)
                square.classList.remove('fail', 'pass')
                square.classList.add('square')
              }
            }
            else if (predictionType == 'Next Batch'){
              if (item.itemId != '11' && item.itemId != '12'){
                let square = document.getElementById(item.itemId)
                square.classList.remove('fail', 'pass')
                square.classList.add('square')
              }
            }
          })


        }
        else {
          //change only the specific workpiece colour
          //turn current workpiece colour according to result, rest grey colour
          if (predictionType == "Next Batch"){
            if (prediction.workpieceId != '34'){
              let id = this.nextWorkpieceArray.filter(x => x.curWorkpieceId == prediction.workpieceId)[0].nextWorkpiece
              this.setWorkpieceColor(id, prediction.isOOS)
            }
          }
          else {
            this.setWorkpieceColor(prediction.workpieceId, prediction.isOOS)
          }
        }
      }
      else {
        //set the colour of the workpieceId of the current prediction
        //set the rest to grey          
        if (predictionType == "Next Batch"){
          if (prediction.workpieceId != '34'){
            let id = this.nextWorkpieceArray.filter(x => x.curWorkpieceId == prediction.workpieceId)[0].nextWorkpiece
            this.setWorkpieceColor(id, prediction.isOOS)
          }
        }
        else {
          this.setWorkpieceColor(prediction.workpieceId, prediction.isOOS)
        }

        //set the rest to grey colour
        this.wpList.forEach(function(item, index){
          if (predictionType == 'Current Batch'){
            if (item.itemId != prediction.workpieceId){
              let square = document.getElementById(item.itemId)
              square.classList.remove('fail', 'pass')
              square.classList.add('square')
            }
          }
          else if (predictionType == 'Next Batch'){
            if (item.itemId != '11' && item.itemId != '12'){
              let square = document.getElementById(item.itemId)
              square.classList.remove('fail', 'pass')
              square.classList.add('square')
            }
          }
        })
        
          this.prevWO = prediction.batchId
          this.noOfPredictionsCnt = 1
        
      }          
    }
  }


  private setWorkpieceColor(id: string, isOOS: boolean){
    let square = document.getElementById(id)
    if (isOOS == true){
      square.classList.add('fail')
      square.classList.remove('pass')
    }
    else if (isOOS == false){
      square.classList.add('pass')
      square.classList.remove('fail')
    }      
  }

}
