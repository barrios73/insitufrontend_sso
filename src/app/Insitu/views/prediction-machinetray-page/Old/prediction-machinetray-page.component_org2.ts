import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { IconThemeColor} from '@progress/kendo-angular-icons/dist/es2015/common/models/theme-color';
import { IconModule} from '@progress/kendo-angular-icons';
import {setData} from '@progress/kendo-angular-intl';
import {TranslateService, LangChangeEvent} from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import * as global from '../../global';
import { formatDate } from '@angular/common';
import { SETTINGS } from '../../settings/environment';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { HttpClient } from '@angular/common/http';
import { SequenceEqualSubscriber } from 'rxjs/internal/operators/sequenceEqual';
import {toFixedPrecision} from "@progress/kendo-angular-inputs/dist/es2015/common/math";
import {MlModelService} from '../../service/mlmodel/mlmodel.service';
import { MlConfigPageComponent } from "../ml-config-page/ml-config-page.component";


// import services and interfaces
import { IResponse, ModelFile, PreprocessingFile, ModelType } from "../../interface/ml-interfaces";
import {any} from "codelyzer/util/function";
import {EditEvent} from "@progress/kendo-angular-grid";

interface IMachineTray {
    numItemPerTray: number;
    numItemPerRow: number;
    // workPieceList: WorkPiece[];
    numItemPerColumn: number;
}

interface IWorkPiece {
    itemId: string;
    // statusColor: string = 'grey';
    processingStatus: number;         // 0:in queue, 1:completed
    predictResult: number;            //  1:fail , 2: pass
}

interface response {
    hasError: boolean;
    msg: string;
    data: any[];
}


@Component({
    selector: 'app-prediction-machinetray-page',
    templateUrl: './prediction-machinetray-page.component.html',
    styleUrls: ['./prediction-machinetray-page.component.scss']
})


export class PredictionMachinetrayPageComponent implements OnInit {
    public dashboardtime: string;
    public timer;
    public chartTimer;
    public pageTitle: string = '';
    public machineID: string;
    //for the 3 KPIs calculation
    public predictionResultList: any[]=[];
    public PredictedPassResult: string = '0';
    public PredictedFailResult: string = '0.00';
    public TotalFailCase: number = 0;
    public TotalCase: number = 0;
    public TotalFailOverTotalCase: string = '';
    //for real-time chart
    public realTimeChartSeries: any[];
    public categoriesRealTimeChart: string[] = [];
    public minYaxisRealTimeChart: number
    public maxYaxisRealTimeChart: number
    public minYaxisDailyYieldChart: number
    public maxYaxisDailyYieldChart: number
    public HardCodedTargetRate: number = 70;

    //for visualisation
    workpiece: IWorkPiece;
    workOrderNo: string //= 22092000005;
    numItemPerTray: number = 12;
    numItemPerRow: number = 4;
    public workPieceList: Array<IWorkPiece>;
    public wpList: Array<IWorkPiece>; //steven
    public elemId: string //steven
    public predictResult: number  //steven
    public hasRetrievedCompletedPredictions: boolean = false //steven, should be false, set to true for testing
    public counter: number = 0  //steven
    public completedPredictionsByWO: any[]  //steven
    public noCompletedPredictionsByWO: number //steven, set to 0 for testing
    public noOfPredictionsCnt: number = 0 //steven
    public predictionBatchSize: number = 1  //steven, hardcoded to 1 for laser welding
    public prevWO: string = ''
    public predictionType: string = ''
    public mlModel: string = ''
    public mlLibrary: string = ''

    // for table  --ws
    public predHistTableDs: any[] = [];
    public tempHistTable: any[] = [];
    public status: any[] = [
        {
            kind: 'Pass',
            color: 'green'
        },
        {
            kind: 'Fail',
            color: 'red'
        },
        {
            kind: 'Processing',
            color: 'blue'
        },
        {
            kind: 'Pending',
            color: 'grey'
        },
        // {
        //     kind: 'Aborted',            // added by ws. NIU for now
        //     color: 'orange'
        // }
    ];
    public modelTypes: Array<ModelType>;
    public preprocessingFiles: Array<PreprocessingFile>;

    //for view details tray
    public isViewDetailsClicked: boolean = false;    //was false;
    public vdTitle:string = '';
    vdworkOrderNo: string //= 22092000005;
    numItemPerVdTray: number = 12;
    numItemPerVdRow: number = 4;
    public vdWpList: Array<IWorkPiece>;
    public vdWorkPieceList: Array<IWorkPiece>;
    public vdElemId: string;
    public prevVdWO: string = ''
    public vdPredictionType: string = ''
    public vdMlModel: string = ''
    public vdMlLibrary: string = ''
    public vdInterval;

    // this is for the Model File
    public modelFiles: Array<ModelFile>;
    public defaultModelFile: ModelFile = {
        id: null,
        modelTypeId: null,
        modelFileName: 'Select model file'
    };
    //for % pass KPI
    public value = 10;
    public colors = [
        {
            to: 50,
            color: "#FF5733",
        },
        {
            from: 50,
            to: 75,
            color: "#FF981A",
        },
        {
            from: 75,
            color: "#4CBB17",
        },
    ];
    //for next batch prediction
    public futurePrediction: any[] = []
    public nextBatchPredictedResults: any[] = []
    public showNextBatchPrediction: boolean = false
    public onPageLoad: boolean = false;  // default true
    public nextWorkpieceArray = [{"curWorkpieceId":"11",
        "nextWorkpiece":"12"},
        {"curWorkpieceId":"12",
            "nextWorkpiece":"13"},
        {"curWorkpieceId":"13",
            "nextWorkpiece":"14"},
        {"curWorkpieceId":"14",
            "nextWorkpiece":"21"},
        {"curWorkpieceId":"21",
            "nextWorkpiece":"22"},
        {"curWorkpieceId":"22",
            "nextWorkpiece":"23"},
        {"curWorkpieceId":"23",
            "nextWorkpiece":"24"},
        {"curWorkpieceId":"24",
            "nextWorkpiece":"31"},
        {"curWorkpieceId":"31",
            "nextWorkpiece":"32"},
        {"curWorkpieceId":"32",
            "nextWorkpiece":"33"},
        {"curWorkpieceId":"33",
            "nextWorkpiece":"34"}
    ]

    //public nextWorkpieceArray: any
    private _hubConnection: HubConnection


    constructor(
        private translate: TranslateService,
        private mlModelService: MlModelService,
        private route: ActivatedRoute,
        public http: HttpClient,
        private settings: SETTINGS) {

        this.mlModelService.getAllModelTypes().subscribe(resp => {
                this.modelTypes = resp.data;
            }
        );

        this.mlModelService.getAllModelFiles().subscribe(resp =>
            this.modelFiles = resp.data);

        this.mlModelService.getPreprocessingFiles().subscribe(resp =>
            this.preprocessingFiles = resp.data);

        this.http.get<any>(this.settings.ENDPOINT.API + '/Database/Summary/GetCurrentPredictionResult').subscribe(prediction => {
            this.settings.LOCALE.ID = 'en-US';
            this.predictionResultList = prediction.data;
            this.CalculatePassRate(prediction.data);
            this.populateRealTimeChart();
            this.predHistTableDs = this.setTable( this.predictionResultList.filter(p => p.machineId === this.machineID) );
            
            let tempHistTable = [];
            this.predHistTableDs.forEach(x => {
                //if (event.lang == 'en-US'){
                    //x.startDt = x.startDt.toLocaleString(event.lang, {year: 'numeric', month: 'short', day: 'numeric'})
                //}
                //else {
                let y = { 
                    "id":"",
                    "topic":"",
                    "status":"",
                    "startDt":"",
                    "passPredict":"",
                    "failTotalQty":"",
                    "workpieces":[]
                }

                y.id = x.id;
                y.topic = x.topic;
                y.status = x.status;
                y.startDt = new Date(x.startDt).toLocaleString(this.settings.LOCALE.ID, {year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric'})
                y.passPredict = x.passPredict;
                y.failTotalQty = x.failTotalQty;
                y.workpieces = x.workpieces;
                    //x.startDt = new Date(x.startDt).toLocaleString(event.lang, {year: 'numeric', month: 'short', day: 'numeric'})
                //}
                tempHistTable.push(y)

                // console.log("Before: " + x.startDt)
                // console.log("After: " + x.startDt)
            })

            this.tempHistTable = [...tempHistTable];

            this.setTrayOnPgLoad(this.predHistTableDs);

        }, err =>{
            console.log('Something went wrong ', err);
        });

    }


    ngOnInit(): void {

        this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
            console.log(event.lang);

            let tempHistTable = [];

            this.predHistTableDs.forEach(x => {
                //if (event.lang == 'en-US'){
                    //x.startDt = x.startDt.toLocaleString(event.lang, {year: 'numeric', month: 'short', day: 'numeric'})
                //}
                //else {
                let y = { 
                    "id":"",
                    "topic":"",
                    "status":"",
                    "startDt":"",
                    "passPredict":"",
                    "failTotalQty":"",
                    "workpieces":[]
                }

                y.id = x.id;
                y.topic = x.topic;
                y.status = x.status;
                y.startDt = new Date(x.startDt).toLocaleString(event.lang, {year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric'})
                y.passPredict = x.passPredict;
                y.failTotalQty = x.failTotalQty;
                y.workpieces = x.workpieces;
                    //x.startDt = new Date(x.startDt).toLocaleString(event.lang, {year: 'numeric', month: 'short', day: 'numeric'})
                //}
                tempHistTable.push(y)

                // console.log("Before: " + x.startDt)
                // console.log("After: " + x.startDt)
            })

            this.tempHistTable = [...tempHistTable];



          });

        this.setVdtrayLayout();

        this.route
            .queryParams
            .subscribe(params => {
                this.machineID = params['machineId'];
            });

        if(global.MachineID === ''){
            global.UpdateMachineID(this.machineID);
        }
        else{
            if(global.MachineID !== this.machineID){
                global.resetMachineSeries();
                global.UpdateMachineID(this.machineID);
            }
        }

        //for visualisation, create the tray layout
        this.wpList = new Array<IWorkPiece>(this.numItemPerTray);
        for (let i = 0; i<this.numItemPerTray; i++){
            let pcs =  {
                itemId:'',
                processingStatus:0,
                predictResult:2
            }
            this.wpList[i] = pcs;
        }


        let wpArray = new Array(12); //steven
        this.workPieceList = this.addRows(4,12, wpArray);


        //url = Server Url
        this._hubConnection = new HubConnectionBuilder().withUrl(this.settings.ENDPOINT.SIGNALR).build();
        this._hubConnection.start()
            .then(() => console.log('result page connection start'))
            .catch(err => {
                console.log('Error while establishing the connection')
            });

        this._hubConnection.on('BroadcastPredictionResult', (prediction) => {

            if(prediction.machineId === this.machineID) {
                console.log(prediction)

                let topic = prediction.topic.toString().toUpperCase();
    
                if (topic.includes('NADINE') || topic.includes('PARSNET')){
                    this.predictionType = 'Next Batch'
                }
                else {
                    this.predictionType = 'Current Batch'
                }
    
                this.mlModel = prediction.modelName
    
    
                let notFirstTray = false;
    
                this.checkWoExistSetTray ( this.predHistTableDs, prediction); // NIU
                this.setColorPredictionResult(prediction, this.predictionType);
                this.predictionResultList.push(prediction);
                this.CalculatePassRate(this.predictionResultList);
                //this.updateStatsRow(prediction);
                this.updateStatsRow(this.predictionResultList);
    
                if (this.predictionType == 'Next Batch'){
    
                    let workpieceId = String(prediction.workpieceId)
                    workpieceId = "[" + workpieceId.substring(0,1) + "," + workpieceId.substring(1,2) + "]"
                    prediction.workpieceId = workpieceId
                    this.futurePrediction.push(prediction)
                    this.nextBatchPredictedResults = [...this.futurePrediction]
                    this.showNextBatchPrediction = true
                }
                else {
                    //this.nextBatchPredictedResults = []
                    this.showNextBatchPrediction = false
                }    
            }

        })

        //timer to update the dashboard time every 1s
        this.timer = setInterval(() => {
            this.dashboardtime = this.createPageSubtitle();
        }, 1000);

        //timer to update the real-time predicted yield chart every 15s
        this.chartTimer = setInterval(() => {
            if(new Date() >= global.nextMachineCleanUpTime){
                global.updateNextMachineCleanUpTime(this.calculateNextCleanUpTime());
                global.resetMachineSeries();
            }

            // if(global.machineSeries[0].data.length === 0 || this.PredictedPassResult !== global.machineSeries[0].data[global.machineSeries[0].data.length-1])
            // {
            this.populateRealTimeChart();
            //}

            //this.populateDailyChart();
        }, 15000);


        this.timer = setInterval(() => {
            this.dashboardtime = this.createPageSubtitle();
        }, 1000);

        this.pageTitle = this.createPageTitle()
    }

    ngOnDestroy(): void {

    }

    searchMLModelByTopic ( searchTop: string  ): void  {

        this.mlModelService.getAllModelTypes().subscribe(resp => {
                this.modelTypes = resp.data;
            }
        );

        this.mlModelService.getAllModelFiles().subscribe(resp =>
            this.modelFiles = resp.data);

        this.mlModelService.getPreprocessingFiles().subscribe(resp =>
            this.preprocessingFiles = resp.data);


        let topic: string = searchTop;
        let modelType: number = null;
        let modelFile: number = null;

        let modelList: any[] = null;

        const headers = { 'Content-Type': 'application/json; charset=utf-8' };
        const body = {
            Topic:  topic,
            ModelTypeId: modelType,
            ModelFileId: modelFile,
        };

        this.http.post<response>(this.settings.ENDPOINT.MLMODEL + '/MLModel/SearchMLModel', JSON.stringify(body), { headers }).subscribe(resp => {
            modelList = resp.data;
            // set tray
            this.predictionType = 'Current Batch'
            this.mlLibrary = modelList[0].library
            this.mlModel = modelList[0].modelName
        });
    }

    public createPageTitle(): string{
        return 'Quality Dashboard: ' + this.machineID;
    }

    public createPageSubtitle():string{
        return this.calcDashboardTime();
    }

    public CalculatePassRate(prediction: any){

        if(prediction.length > 0){
            this.TotalCase = prediction.filter(p => p.machineId === this.machineID).length;
            this.TotalFailCase = prediction.filter(p => p.isOOS == true && p.machineId === this.machineID).length;
            this.TotalFailOverTotalCase = this.TotalFailCase.toString() + '/' + this.TotalCase.toString();
            this.PredictedPassResult = Math.round(((this.TotalCase - this.TotalFailCase) / this.TotalCase * 100)).toString();
            this.PredictedFailResult = (this.TotalFailCase / this.TotalCase * 100).toFixed(2).toString();
        }

        // const DistinctProcess = prediction.filter(p => p.machineId === this.machineID).map(item => item.processId).filter((value, index, self) => self.indexOf(value) === index);
        // DistinctProcess.forEach( (process) => {
        //   let pr = new PassRateWithProcessId();
        //   const TotalResult = prediction.filter(p => p.machineId === this.machineID && p.processId === process);
        //   const TotalPassResult = prediction.filter(p => p.isOOS == false && p.machineId === this.machineID && p.processId === process);
        //   const rate = TotalPassResult.length / TotalResult.length * 100;
        //   pr.machineId = this.machineID;
        //   pr.processId = process;
        //   pr.passRate = rate.toFixed(2).toString() + "%";
        //   pr.targetPassRate = this.HardCodedTargetRate + "%"
        //   this.passRateWithProcessIdList.push(pr);
        // });
    }

    public calculateNextCleanUpTime(): Date{

        let currentDate = new Date();
        if(currentDate.getHours() >= Number(global.DashStartTime)){
            currentDate.setDate(currentDate.getDate() + 1);
        }

        return new Date(formatDate(currentDate, 'yyyy-MM-dd', 'en_US') + ' ' + global.DashStartTime + ':00:00');
    }

    public populateRealTimeChart(){
        const myClonedArray = [];

        //let localeId = this.translate.currentLang;

        global.machineSeries.forEach(val => myClonedArray.push(Object.assign({}, val)));

        if (myClonedArray[0].data.length == 6){
            myClonedArray[0].data.shift();
            myClonedArray[0].data.push(this.PredictedPassResult);

            //calculate y-axis min value
            this.minYaxisRealTimeChart = Math.floor(this.CalcChartYaxisMin(myClonedArray,this.HardCodedTargetRate)/10)*10;

            //calculate y-axis max value
            this.maxYaxisRealTimeChart = Math.ceil(this.CalcChartYaxisMax(myClonedArray,this.HardCodedTargetRate)/10)*10;

            myClonedArray[1].data.shift();
            myClonedArray[1].data.push(this.HardCodedTargetRate);

            this.realTimeChartSeries = myClonedArray;

            this.categoriesRealTimeChart = [...global.categoryRealTimeMachinePg];
            let currentDate = new Date();
            this.categoriesRealTimeChart.shift();
            this.categoriesRealTimeChart.push(currentDate.toLocaleString(this.settings.LOCALE.ID, {hour: 'numeric', minute: 'numeric', second: 'numeric'}));
            global.categoryRealTimeMachinePg.shift();
            global.categoryRealTimeMachinePg.push(currentDate.toLocaleString(this.settings.LOCALE.ID, {hour: 'numeric', minute: 'numeric', second: 'numeric'}));
        }
        else {
            myClonedArray[0].data.push(this.PredictedPassResult);
            myClonedArray[1].data.push(this.HardCodedTargetRate);
            this.realTimeChartSeries = myClonedArray;

            //calculate y-axis min value
            this.minYaxisRealTimeChart = Math.floor(this.CalcChartYaxisMin(myClonedArray,this.HardCodedTargetRate)/10)*10;

            //calculate y-axis max value
            this.maxYaxisRealTimeChart = Math.ceil(this.CalcChartYaxisMax(myClonedArray,this.HardCodedTargetRate)/10)*10;

            this.categoriesRealTimeChart = [...global.categoryRealTimeMachinePg];
            let currentDate = new Date();
            this.categoriesRealTimeChart.push(currentDate.toLocaleString(this.settings.LOCALE.ID, {hour: 'numeric', minute: 'numeric', second: 'numeric'}));
            global.categoryRealTimeMachinePg.push(currentDate.toLocaleString(this.settings.LOCALE.ID, {hour: 'numeric', minute: 'numeric', second: 'numeric'}));

        }

    }

    public addRows (itemRow: any, total: any, arr: Array<any>) : Array<any> {
        for (var i = 0; i < arr.length; i++) {
            if ( (i+1) % itemRow === 0) {
                arr[i] = itemRow;

                if (Math.floor(i / this.numItemPerRow) == 0){
                    let rowNumber = '1';
                    this.wpList[i].itemId = rowNumber + (itemRow).toString();
                }
                else if (Math.floor(i / this.numItemPerRow) == 1){
                    let rowNumber = '2';
                    this.wpList[i].itemId = rowNumber + (itemRow).toString();
                }
                else if (Math.floor(i / this.numItemPerRow) == 2){
                    let rowNumber = '3';
                    this.wpList[i].itemId = rowNumber + (itemRow).toString();
                }

            }
            else {
                arr[i] = (i + 1) % itemRow ;

                if (Math.floor(i / this.numItemPerRow) == 0){
                    let rowNumber = '1';
                    this.wpList[i].itemId = rowNumber + ((i + 1) % itemRow).toString();
                }
                else if (Math.floor(i / this.numItemPerRow) == 1){
                    let rowNumber = '2';
                    this.wpList[i].itemId = rowNumber + ((i + 1) % itemRow).toString();
                }
                else if (Math.floor(i / this.numItemPerRow) == 2){
                    let rowNumber = '3';
                    this.wpList[i].itemId = rowNumber + ((i + 1) % itemRow).toString();
                }

                console.log('This Array: ' + arr);

            }
        }
        return arr;
    }

    private calcDashboardTime(): string{

        //let localeId = this.translate.currentLang;
        let d = new Date();
        d.setHours(8);
        d.setMinutes(0);
        d.setSeconds(0);
        d.setMilliseconds(0);
        let startdatetime = d.getTime();

        let curTime = new Date().getTime();

        if (curTime < startdatetime){
            let startdate = new Date((startdatetime - (24*60*60*1000)));
            let curdate = new Date(curTime);

            //let dashboardDate = formatDate(startdate, 'd MMM yyyy h:mm:ss a', this.settings.LOCALE.ID) + '-' + formatDate(curdate, 'd MMM yyyy h:mm:ss a', this.settings.LOCALE.ID);
            //return dashboardDate;
            return startdate.toLocaleString(this.settings.LOCALE.ID, {year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric'}) + ' - ' + curdate.toLocaleString(this.settings.LOCALE.ID, {year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric'});
        }
        else{
            //let options = {year: 'numeric', month: 'short', day: 'numeric'};
            let startdate = new Date(startdatetime);
            let curdate = new Date(curTime);

            //let dashboardDate = formatDate(startdate, 'd MMM yyyy h:mm:ss a', this.settings.LOCALE.ID) + '-' + formatDate(curdate, 'd MMM yyyy h:mm:ss a', this.settings.LOCALE.ID);
            //return dashboardDate;
            return startdate.toLocaleString(this.settings.LOCALE.ID, {year: 'numeric', month: 'short', day: 'numeric', hour: "numeric", minute: 'numeric', second: "numeric"}) + ' - ' + curdate.toLocaleString(this.settings.LOCALE.ID, {year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric'});

            // let localeId = "zh-CN";
            // let pipe = new DatePipe(localeId);
            // let dashboarddate = pipe.transform(startdate, localeId, "yyyy-MM-dd hh:mm:ss");
            // return dashboarddate;
        }
    }

    private CalcChartYaxisMax(data:any[],targetline: number){
        let maxYaxisRealTimeChart: number

        let max = Math.max(...data[0].data);
        if (max >= targetline){
            maxYaxisRealTimeChart = Math.round(max + 0.2*max);
        }
        else {
            maxYaxisRealTimeChart = Math.round(targetline + 0.2*targetline);
        }

        return maxYaxisRealTimeChart;
    }

    private CalcChartYaxisMin(data:any[],targetline: number){
        let minYaxisRealTimeChart: number

        let min = Math.min(...data[0].data);
        if (min <= targetline){
            minYaxisRealTimeChart = Math.round(min - 0.2*min);
        }
        else{
            minYaxisRealTimeChart = Math.round(targetline - 0.2*targetline)
        }
        if (minYaxisRealTimeChart <= 0){
            minYaxisRealTimeChart = 0;
        }

        return minYaxisRealTimeChart;
    }

    private setColorPredictionResult(prediction: any, predictionType: string){

        if (prediction.batchId == this.prevWO){
            //this.workOrderNo = prediction.batchId;
            if (prediction.workpieceId == '11'){
                this.wpList.forEach(function(item, index){
                    if (item.itemId != prediction.workpieceId){
                        let square = document.getElementById(item.itemId)
                        square.classList.remove('fail', 'pass')
                        square.classList.add('square')
                    }
                })
                this.setWorkpieceColor(prediction.workpieceId, prediction.isOOS)
            }
            else {
                this.workOrderNo = prediction.batchId
                this.setWorkpieceColor(prediction.workpieceId, prediction.isOOS)
            }

        }
        else {
            this.workOrderNo = prediction.batchId

            this.wpList.forEach(function(item, index){
                if (item.itemId != prediction.workpieceId){
                    let square = document.getElementById(item.itemId)
                    square.classList.remove('fail', 'pass')
                    square.classList.add('square')
                }
            })
            this.setWorkpieceColor(prediction.workpieceId, prediction.isOOS)


            this.prevWO = prediction.batchId
        }


            // if (prediction.batchId == this.prevWO){
            //     //this.noOfPredictionsCnt = this.noCompletedPredictionsByWO + this.counter
            //     this.noOfPredictionsCnt = this.noOfPredictionsCnt + 1 //this.counter

            //     //modulo result is whole number --> reach the last piece of tray e.g. 12th, 24th workpiece
            //     if ((this.noOfPredictionsCnt-1) % (this.numItemPerTray / this.predictionBatchSize) == 0){

            //         //turn current workpiece colour according to result, rest grey colour
            //         if (predictionType == "Next Batch"){
            //             if (prediction.workpieceId != '34'){
            //                 let id = this.nextWorkpieceArray.filter(x => x.curWorkpieceId == prediction.workpieceId)[0].nextWorkpiece
            //                 this.setWorkpieceColor(id, prediction.isOOS)
            //             }
            //         }
            //         else {
            //             this.setWorkpieceColor(prediction.workpieceId, prediction.isOOS)
            //         }

            //         //set the rest to grey colour
            //         this.wpList.forEach(function(item, index){
            //             if (predictionType == 'Current Batch'){
            //                 if (item.itemId != prediction.workpieceId){
            //                     let square = document.getElementById(item.itemId)
            //                     square.classList.remove('fail', 'pass')
            //                     square.classList.add('square')
            //                 }
            //             }
            //             else if (predictionType == 'Next Batch'){
            //                 if (item.itemId != '11' && item.itemId != '12'){
            //                     let square = document.getElementById(item.itemId)
            //                     square.classList.remove('fail', 'pass')
            //                     square.classList.add('square')
            //                 }
            //             }
            //         })
            //     }
            //     else {
            //         //change only the specific workpiece colour
            //         //turn current workpiece colour according to result, rest grey colour
            //         if (predictionType == "Next Batch"){
            //             if (prediction.workpieceId != '34'){
            //                 let id = this.nextWorkpieceArray.filter(x => x.curWorkpieceId == prediction.workpieceId)[0].nextWorkpiece
            //                 this.setWorkpieceColor(id, prediction.isOOS)
            //             }
            //         }
            //         else {
            //             this.setWorkpieceColor(prediction.workpieceId, prediction.isOOS)
            //         }
            //     }
            // }
            // else {
            //     //set the colour of the workpieceId of the current prediction
            //     //set the rest to grey
            //     if (predictionType == "Next Batch"){
            //         if (prediction.workpieceId != '34'){
            //             let id = this.nextWorkpieceArray.filter(x => x.curWorkpieceId == prediction.workpieceId)[0].nextWorkpiece
            //             this.setWorkpieceColor(id, prediction.isOOS)
            //         }
            //     }
            //     else {
            //         this.setWorkpieceColor(prediction.workpieceId, prediction.isOOS)
            //     }

            //     //set the rest to grey colour
            //     this.wpList.forEach(function(item, index){
            //         if (predictionType == 'Current Batch'){
            //             if (item.itemId != prediction.workpieceId){
            //                 let square = document.getElementById(item.itemId)
            //                 square.classList.remove('fail', 'pass')
            //                 square.classList.add('square')
            //             }
            //         }
            //         else if (predictionType == 'Next Batch'){
            //             if (item.itemId != '11' && item.itemId != '12'){
            //                 let square = document.getElementById(item.itemId)
            //                 square.classList.remove('fail', 'pass')
            //                 square.classList.add('square')
            //             }
            //         }
            //     })

            //     this.prevWO = prediction.batchId
            //     this.noOfPredictionsCnt = 1


        //}
    }


    private setTrayOnPgLoad( thisPred: any ) : boolean {

        let notFirstTray = false
        let lastIndex = thisPred.length - 1;

        // check if pred length is more than 1
        if (thisPred.length > 0 )
        {
            this.setTray(thisPred[lastIndex]);
            this.searchMLModelByTopic (thisPred[lastIndex].topic)
            notFirstTray = true;
        }
        return notFirstTray;
    }


    private setTray ( predictionLastWO: any )   {

        this.workOrderNo = predictionLastWO.id;

        let noOfCompletedPredictionsByWO = predictionLastWO.workpieces.length

        let firstNPredictions
        if (noOfCompletedPredictionsByWO > 0){
            if (noOfCompletedPredictionsByWO % 12 != 0){
                firstNPredictions = Math.floor(noOfCompletedPredictionsByWO / 12)*12
            }
            else {
                firstNPredictions = 0
            }
        }
        else {
            firstNPredictions = 0
        }

        let lastTrayCompletedPredictions =  predictionLastWO.workpieces.slice(firstNPredictions, noOfCompletedPredictionsByWO)


        if (lastTrayCompletedPredictions.length > 0){
            lastTrayCompletedPredictions.forEach (x => {
            this.setWorkpieceColor(x.itemId, !x.predictResult)
            })
        }
    }


    public onSelect (thisWorkOrder: any) {

        this.isViewDetailsClicked = true; 
        this.vdTitle = thisWorkOrder.id;
        this.setTrayForVD(thisWorkOrder)          
    }


    closeVD(): void {
        this.isViewDetailsClicked = false;
        this.setVdtrayLayout();       //reset lol
        window.scroll(0, 0); // return to top of window
    }


    public setVdtrayLayout() {
        //for visualisation, create the tray layout for view details overlay -- ws
        this.vdWpList = new Array<IWorkPiece>(this.numItemPerTray);
        for (let i = 0; i<this.numItemPerTray; i++){
            let pcs =  {
                itemId:'',
                processingStatus:0,
                predictResult:2
            }
            this.vdWpList[i] = pcs;
        }

        let vdWpArray = new Array(12);
        this.vdWorkPieceList = this.addVDRows(4,12, vdWpArray);
        console.log("vdWorkPieceList: " + this.vdWorkPieceList);
    }


    private addVDRows (itemRow: any, total: any, arr: Array<any>) : Array<any> {
        for (var i = 0; i < arr.length; i++) {
            if ( (i+1) % itemRow === 0) {
                arr[i] = itemRow;

                if (Math.floor(i / this.numItemPerRow) == 0){
                    let rowNumber = 'vd1';
                    this.vdWpList[i].itemId = rowNumber + (itemRow).toString();
                }
                else if (Math.floor(i / this.numItemPerRow) == 1){
                    let rowNumber = 'vd2';
                    this.vdWpList[i].itemId = rowNumber + (itemRow).toString();
                }
                else if (Math.floor(i / this.numItemPerRow) == 2){
                    let rowNumber = 'vd3';
                    this.vdWpList[i].itemId = rowNumber + (itemRow).toString();
                }
            }
            else {
                arr[i] = (i + 1) % itemRow ;

                if (Math.floor(i / this.numItemPerRow) == 0){
                    let rowNumber = 'vd1';
                    this.vdWpList[i].itemId = rowNumber + ((i + 1) % itemRow).toString();
                }
                else if (Math.floor(i / this.numItemPerRow) == 1){
                    let rowNumber = 'vd2';
                    this.vdWpList[i].itemId = rowNumber + ((i + 1) % itemRow).toString();
                }
                else if (Math.floor(i / this.numItemPerRow) == 2){
                    let rowNumber = 'vd3';
                    this.vdWpList[i].itemId = rowNumber + ((i + 1) % itemRow).toString();
                }

                //console.log('This Array: ' + arr);
            }
        }
        return arr;
    }


    private setTrayForVD ( thisPred: any )   {
        this.searchMLModelByTopicforVd(thisPred.topic);

        thisPred.workpieces.forEach (x => {
            this.setWorkpieceColorforVD( 'vd'+x.itemId, !x.predictResult)
        })
    }


    private batchIdFilter (id: string) : string {
        return id.slice(0, -7);
    }


    searchMLModelByTopicforVd ( searchTop: string  ): void  {

        this.mlModelService.getAllModelTypes().subscribe(resp => {
                this.modelTypes = resp.data;
            }
        );

        this.mlModelService.getAllModelFiles().subscribe(resp =>
            this.modelFiles = resp.data);

        this.mlModelService.getPreprocessingFiles().subscribe(resp =>
            this.preprocessingFiles = resp.data);


        let topic: string = searchTop;
        let modelType: number = null;
        let modelFile: number = null;

        let modelList: any[] = null;

        const headers = { 'Content-Type': 'application/json; charset=utf-8' };
        const body = {
            Topic:  topic,
            ModelTypeId: modelType,
            ModelFileId: modelFile,
        };

        this.http.post<response>(this.settings.ENDPOINT.MLMODEL + '/MLModel/SearchMLModel', JSON.stringify(body), { headers }).subscribe(resp => {
            modelList = resp.data;
            // set tray
            this.vdPredictionType = 'Current Batch';
            this.vdMlModel = modelList[0].modelName;
            this.vdMlLibrary = modelList[0].library;
        });
    }


    private checkWoExistSetTray ( predHist: any[], prediction: any ) : boolean {
        let notFirstTray = false;

        for (let i = 0; i <  predHist.length; i++) {
            if ( prediction.id === predHist[i].id) {
                // get workpieces if tray exists
                this.setTray(predHist[i]);
                this.searchMLModelByTopic (predHist[0].topic);
                notFirstTray = true;
            }
        }
        return notFirstTray;
    }


    private setWorkpieceColor(id: string, isOOS: boolean){
        let square = document.getElementById(id)
        if (isOOS == true){
            square.classList.add('fail')
            square.classList.remove('pass')
        }
        else if (isOOS == false){
            square.classList.add('pass')
            square.classList.remove('fail')
        }
    }


    private setWorkpieceColorforVD (id: string, isOOS: boolean){
        let square = document.getElementById(id)
        if (isOOS == true){
            square.classList.add('fail')
            square.classList.remove('pass')
        }
        else if (isOOS == false){
            square.classList.add('pass')
            square.classList.remove('fail')
        }
    }


    private updateStatsRow(pred: any) {
        //this.http.get<any>(ENDPOINT.API + '/Database/Summary/GetCurrentPredictionResult').subscribe(pred => {
            //this.predictionResultList = pred.data;
            //this.CalculatePassRate(pred.data);
            //this.populateRealTimeChart();
            // this.predHistTableDs = this.setTable( this.predictionResultList );
        //}, err =>{
            //console.log('Something went wrong ', err);
        //})

            //this.CalculatePassRate(pred.data);
            this.populateRealTimeChart();
            this.predHistTableDs = this.setTable(pred);

            let tempHistTable = [];
            this.predHistTableDs.forEach(x => {
                //if (event.lang == 'en-US'){
                    //x.startDt = x.startDt.toLocaleString(event.lang, {year: 'numeric', month: 'short', day: 'numeric'})
                //}
                //else {
                let y = { 
                    "id":"",
                    "topic":"",
                    "status":"",
                    "startDt":"",
                    "passPredict":"",
                    "failTotalQty":"",
                    "workpieces":[]
                }

                y.id = x.id;
                y.topic = x.topic;
                y.status = x.status;
                y.startDt = new Date(x.startDt).toLocaleString(this.settings.LOCALE.ID, {year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric'})
                y.passPredict = x.passPredict;
                y.failTotalQty = x.failTotalQty;
                y.workpieces = x.workpieces;
                    //x.startDt = new Date(x.startDt).toLocaleString(event.lang, {year: 'numeric', month: 'short', day: 'numeric'})
                //}
                tempHistTable.push(y)

                // console.log("Before: " + x.startDt)
                // console.log("After: " + x.startDt)
            })

            this.tempHistTable = [...tempHistTable];

            //this.tempHistTable = JSON.parse(JSON.stringify(this.predHistTableDs));
            //[...this.predHistTableDs];
    }


    private setTable ( clonePredResultList: any[] ) : any[] {

        let predHistTableDs=[], wpList = [];
        let lastWorkOrder =0 , noOfWO = predHistTableDs.length;

        clonePredResultList = clonePredResultList.filter(p => p.machineId === this.machineID);

        for (let i = 0; i < clonePredResultList.length; i++)  {
            // get WO id
            let workOrderNo = clonePredResultList[i].batchId;

            // check WO exist. For 1st Wo
            if (predHistTableDs.length === 0 ) {
                // set first work order as the id
                let thisWorkOrder =  createNewWO(clonePredResultList[i], workOrderNo , noOfWO , this.settings.LOCALE.ID, []);
                // add work order into ds
                predHistTableDs.push(thisWorkOrder);
                // create wp array
                wpList = new Array<IWorkPiece>();
                wpList.push(
                    {
                        itemId: clonePredResultList[i].workpieceId,
                        processingStatus: 1,
                        predictResult: !(clonePredResultList[i].isOOS)
                    });
                // insert data
                predHistTableDs[0].workpieces = wpList

                // if not the first entry
            } else {
                noOfWO = predHistTableDs.length;
                lastWorkOrder = predHistTableDs[(noOfWO-1)].id

                if (lastWorkOrder != workOrderNo){
                    if (checkIfWOExist(predHistTableDs, workOrderNo) != null )  {

                        let indexOfWO = checkIfWOExist(predHistTableDs, workOrderNo);
                        let thisWpList = predHistTableDs[indexOfWO].workpieces;
                        thisWpList.push(
                            {
                                itemId: clonePredResultList[i].workpieceId,
                                processingStatus: 1,
                                predictResult: !(clonePredResultList[i].isOOS)
                            });
                        predHistTableDs[indexOfWO].workpieces = thisWpList
                        // copy to new index
                        predHistTableDs.push(predHistTableDs[indexOfWO]);
                        predHistTableDs.splice(indexOfWO, 1);

                    } else  {
                        let thisPred = clonePredResultList[i];
                        let thisWorkOrder = createNewWO(thisPred, workOrderNo, noOfWO, this.settings.LOCALE.ID, []);
                        // add work order into ds
                        predHistTableDs.push(thisWorkOrder);
                        // create wp array
                        wpList = new Array<IWorkPiece>();
                        wpList.push(
                            {
                                itemId: clonePredResultList[i].workpieceId,
                                processingStatus: 1,
                                predictResult: !(clonePredResultList[i].isOOS)
                            });
                        // insert data
                        predHistTableDs[noOfWO].workpieces = wpList
                    }
                }else {
                    let thisWpList = predHistTableDs[noOfWO-1].workpieces;
                    thisWpList.push(
                        {
                            itemId: clonePredResultList[i].workpieceId,
                            processingStatus: 1,
                            predictResult: !(clonePredResultList[i].isOOS)
                        });
                    predHistTableDs[noOfWO - 1].workpieces = thisWpList;
                }

            }
        }

        let lastWOIndex = predHistTableDs.length-1;
        this.prevWO = predHistTableDs[lastWOIndex].id

        for (let x = 0; x < predHistTableDs.length; x++ ) {
            setDetailsForWO(predHistTableDs, x) ;
        }

        return predHistTableDs;
    }
 
}



function createNewWO( thisPred: any, workOrderNo: any, noOfWOinArr: number, localeId: string, wpList: any ) : any {
    
    let thisWO = {
        id: workOrderNo,
        topic: thisPred.topic,
        status: 'Pending',
        startDt: thisPred.createdDateTime,
        //startDt: formatDate(thisPred.createdDateTime, 'd MMM yyyy hh:mm a', localeId),
        passPredict: 0,
        failTotalQty: 'na',
        workpieces: wpList,
    }
    return thisWO;
}

function setDetailsForWO ( predHistTableDs: any, index: number ) {
    let totalQty = predHistTableDs[index].workpieces.length  ;

    predHistTableDs[index].status = 'Completed'
    let predCnt = predHistTableDs[index].workpieces.filter ( p => p.predictResult === true).length;
    let failCnt = totalQty  - predCnt;
    // console.log(' last index pass of ' + workOrderNo + ' : ' + predCnt)
    // console.log('last index fail of ' + workOrderNo + ' : ' +failCnt + ', total : ' + predHistTableDs[lastIndex].totalqty)
    predHistTableDs[index].passPredict = toFixedPrecision ( ( predCnt / totalQty ) * 100, 2)
    predHistTableDs[index].failTotalQty = failCnt + ' / ' + totalQty
}

function checkIfWOExist ( predDs: any, thisWONum: any ) : number {
    let woExist: number = null;
    for (let w = 0; w < predDs.length; w++) {
        if (thisWONum === predDs[w].id)
            woExist = w;
    }
    return woExist;
}


