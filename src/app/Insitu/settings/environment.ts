import {TranslateService} from '@ngx-translate/core';
import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class SETTINGS {

  private localeId: BehaviorSubject<string> 
    
  constructor(
    private translate: TranslateService
  ){
  }


  LOCALE = {
    ID: this.translate.currentLang
  }
  
  ENDPOINT = {
    SIGNALR: 'http://localhost:5003/notify',
    API: 'http://localhost:5004/api',    
    MLMODEL: 'http://localhost:4999/api',  //'http://192.168.137.1:4999/api' 
    COMMON: 'http://localhost:4998/api',
    QUALITY: 'http://localhost:6001/api',
    
    //  SIGNALR: 'http://172.20.115.26:5003/notify',
    //  API: 'http://172.20.115.26:5004/api',
    //  MLMODEL: 'http://172.20.115.26:4999/api',  //'http://localhost:5000/api' 
    //  COMMON: 'http://172.20.115.26:4998/api',
    //  QUALITY: 'http://172.20.115.26:6001/api'
  };

  // getLocaleId(): Observable<string> {
  //   return this.localeId;
  // }

}


