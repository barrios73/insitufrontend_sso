'use strict';

export let DashStartTime: string = "20"; 

//#region Summary Page
export let series: any[] = [
  {
    name: "% Pass Prediction",
    data: [],
    markers:{
      size:5,
      type: 'circle',
      background: 'red',
    },
    color:"blue"
  },
  {
    name: "Target",
    data: [],
    markers:{
      visible: false
    },  
    color: "#000000"
  }
];

export let nextCleanUpTime: Date;

export function updateNextCleanUpTime(value: Date){
  nextCleanUpTime = value;
}

export function resetSeries(){
  series[0].data = [];
  series[1].data = [];
}
//#endregion

//#region Machine Detail page
export let machineId = "";
export function UpdateMachineID(value: string){
  machineId = value;
}

export let machineSeries: any[] = [
  {
    name: "% Pass Prediction",
    data: [],
    markers:{
      size:5,
      type: 'circle'
    },
    color:"blue"
  },
  {
    name: "Target",
    data: [],
    markers:{
      visible:false
    },
    color:"#000000"
  }
];

export let nextMachineCleanUpTime: Date; 

export function updateNextMachineCleanUpTime(value: Date){
  nextMachineCleanUpTime = value;
}
export function resetMachineSeries(){
  machineSeries[0].data = [];
  machineSeries[1].data = [];
}
 

//export let categoryTime: any[] = [];
export let categoryRealTimeSummaryPg: any[] = [];
export let categoryRealTimeMachinePg: any[] = [];

//#endregion