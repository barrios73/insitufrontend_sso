import { Injectable } from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {ToastService} from '@dis/services/message/toast.service';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  constructor(private toast: ToastService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
  
    const token = localStorage.getItem('token');

    if (token){
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    }
    
    return next.handle(request)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          let errorMsg = '';
          if (error.error != null){
            if (error.error.errors != undefined){

              let keys = Object.keys(error.error.errors)
              
              errorMsg = `Error Code: ${error.error.status}
              Message: ${error.error.errors[keys[0]][0]}
              Error: ${error.error.title}`;
            }
            else if (error.error instanceof ErrorEvent) {
              errorMsg = `Error: ${error.message}`;
            }
            else {
              errorMsg = `Error Code: ${error.status}
              Message: ${error.message}
              Error: ${error.error}`;
            }
  
            //disable the display of the err msg, use kendo dialog for customised msg
            //this.toast.error(errorMsg);
            return throwError(error);
          }  
        })
      );
  }
}
