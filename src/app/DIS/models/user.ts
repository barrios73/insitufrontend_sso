export class UserData {
    _id: string;
    username: string;
    role: string;
    group: string;
    fullname: string;
    email: string;
    mobile: string;
    createdTimestamp: string;
}