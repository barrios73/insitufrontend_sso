import { Routes } from '@angular/router';
import { AuthGuard } from '@dis/auth/auth.guard';
import { RoleTypes } from '@dis/auth/roles.enum';

/**
 *  BELOW ARE ROUTES USED IN TEMPLATE
 *  DO NOT EDIT
 *
 *  Treat this as a sample when you link your pages in routes.config.ts
 */

// NOTE: Do not add views within /DIS folder (this folder will be updated and your project specific code shouldn't be here)
// The views below are within /DIS folder because we created them and are responsible for updating them
import { LoginComponent } from '@dis/views/login/login.component';
import { SamplePageComponent } from '@dis/views/sample-page/sample-page.component';
import { EditedPageComponent } from '@dis/views/edited-page/edited-page.component';
import {DashboardOneComponent} from '@dis/views/dashboard-one/dashboard-one.component';
import {DashboardTwoComponent} from '@dis/views/dashboard-two/dashboard-two.component';
import {DashboardThreeComponent} from '@dis/views/dashboard-three/dashboard-three.component';
import {TablesComponent} from '@dis/views/tables/tables.component';
import {InputFieldsComponent} from '@dis/views/input-fields/input-fields.component';
import {FormFillingComponent} from '@dis/views/form-filling/form-filling.component';
import { PredictionSummaryPageComponent } from 'src/app/Insitu/views/prediction-summary-page/prediction-summary-page.component';
import { AnalyticsYieldTrendPageComponent } from 'src/app/Insitu/views/analytics-yieldtrend-page/analytics-yieldtrend-page.component';
import { AnalyticsIssuesPageComponent } from 'src/app/Insitu/views/analytics-issues-page/analytics-issues-page.component';
import { PredictionMachinePageComponent } from 'src/app/Insitu/views/prediction-machine-page/prediction-machine-page.component';
import { MlConfigPageComponent } from 'src/app/Insitu/views/ml-config-page/ml-config-page.component';
import { MlModeltypePageComponent } from 'src/app/Insitu/views/ml-modeltype-page/ml-modeltype-page.component';
import { MlModelfilePageComponent } from 'src/app/Insitu/views/ml-modelfile-page/ml-modelfile-page.component';
import { MlUploadfilePageComponent } from 'src/app/Insitu/views/ml-uploadfile-page/ml-uploadfile-page.component';
import { PredictionMachinetrayPageComponent } from 'src/app/Insitu/views/prediction-machinetray-page/prediction-machinetray-page.component';


export const AppTemplateRoutes: Routes = [
  // Below is how to include a page
  { path: 'login', component: LoginComponent, canActivate: [AuthGuard] }, //not used
  // Below is how to include a page that can be accessed after any user is logged in
  {
    path: 'prediction-summary-page',
    component: PredictionSummaryPageComponent,
    canActivate: [AuthGuard], // ONLY acceptable ELEVATION can access after login
    data: {
      elevation: [
        RoleTypes.ROLE_ADMIN,
        RoleTypes.ROLE_USER
      ] // List out all roles that are acceptable
    }
  },
  {
    path: 'analytics-yieldtrend-page',
    component: AnalyticsYieldTrendPageComponent,
    canActivate: [AuthGuard], // ONLY acceptable ELEVATION can access after login
    data: {
      elevation: [
        RoleTypes.ROLE_ADMIN,
        RoleTypes.ROLE_USER
      ] // List out all roles that are acceptable
    }
  },
  {
    path: 'analytics-issues-page',
    component: AnalyticsIssuesPageComponent,
    canActivate: [AuthGuard], // ONLY acceptable ELEVATION can access after login
    data: {
      elevation: [
        RoleTypes.ROLE_ADMIN,
        RoleTypes.ROLE_USER
      ] // List out all roles that are acceptable
    }
  },
  {
    path: 'prediction-machine-page',
    component: PredictionMachinePageComponent,
    canActivate: [AuthGuard], // ONLY acceptable ELEVATION can access after login
    data: {
      elevation: [
        RoleTypes.ROLE_ADMIN,
        RoleTypes.ROLE_USER
      ] // List out all roles that are acceptable
    }
  },
  {
    path: 'prediction-machinetray-page',
    component: PredictionMachinetrayPageComponent,
    canActivate: [AuthGuard], // ONLY acceptable ELEVATION can access after login
    data: {
      elevation: [
        RoleTypes.ROLE_ADMIN,
        RoleTypes.ROLE_USER
      ] // List out all roles that are acceptable
    }
  },
  {
    path: 'ml-config-page',
    component: MlConfigPageComponent,
    canActivate: [AuthGuard], // ONLY acceptable ELEVATION can access after login
    data: {
      elevation: [
        RoleTypes.ROLE_ADMIN
      ] // List out all roles that are acceptable
    }
  },
  {
    path: 'ml-modeltype-page',
    component: MlModeltypePageComponent,
    canActivate: [AuthGuard], // ONLY acceptable ELEVATION can access after login
    data: {
      elevation: [
        RoleTypes.ROLE_ADMIN
      ] // List out all roles that are acceptable
    }
  },  
  {
    path: 'ml-modelfile-page',
    component: MlModelfilePageComponent,
    canActivate: [AuthGuard], // ONLY acceptable ELEVATION can access after login
    data: {
      elevation: [
        RoleTypes.ROLE_ADMIN
      ] // List out all roles that are acceptable
    }
  },
  {
    path: 'ml-uploadfile-page',
    component: MlUploadfilePageComponent,
    canActivate: [AuthGuard], // ONLY acceptable ELEVATION can access after login
    data: {
      elevation: [
        RoleTypes.ROLE_ADMIN
      ] // List out all roles that are acceptable
    }
  },
  {
    path: 'sample',
    component: SamplePageComponent,
    canActivate: [AuthGuard], // To accept ALL access after login, use AuthGuard
    data: {
      elevation: [
        RoleTypes.ROLE_USER,
        RoleTypes.ROLE_ADMIN
      ] // List out all roles that are acceptable
    }
  },
  // Below is how to include a page that can be accessed after a user with SPECIFIED role is logged in
  {
    path: 'sample2',
    component: EditedPageComponent,
    canActivate: [AuthGuard], 
    data: {
      elevation: [
        RoleTypes.ROLE_ADMIN
      ] 
    }
  },
  {
    path: 'dashboard-one',
    component: DashboardOneComponent,
    canActivate: [AuthGuard], // ONLY acceptable ELEVATION can access after login
    data: {
      elevation: [
        RoleTypes.ROLE_ADMIN,
        //RoleTypes.ROLE_MANAGER,
        RoleTypes.ROLE_USER
      ] // List out all roles that are acceptable
    }
  },
  {
    path: 'dashboard-two',
    component: DashboardTwoComponent,
    canActivate: [AuthGuard], // ONLY acceptable ELEVATION can access after login
    data: {
      elevation: [
        RoleTypes.ROLE_ADMIN,
        RoleTypes.ROLE_USER
      ] // List out all roles that are acceptable
    }
  },
  {
    path: 'dashboard-three',
    component: DashboardThreeComponent,
    canActivate: [AuthGuard], // ONLY acceptable ELEVATION can access after login
    data: {
      elevation: [
        RoleTypes.ROLE_ADMIN,
        RoleTypes.ROLE_USER
      ] // List out all roles that are acceptable
    }
  },
  {
    path: 'table',
    component: TablesComponent,
    canActivate: [AuthGuard], // ONLY acceptable ELEVATION can access after login
    data: {
      elevation: [
        RoleTypes.ROLE_ADMIN,        
        RoleTypes.ROLE_USER
      ] // List out all roles that are acceptable
    }
  },
  {
    path: 'input-field',
    component: InputFieldsComponent,
    canActivate: [AuthGuard], // ONLY acceptable ELEVATION can access after login
    data: {
      elevation: [
        RoleTypes.ROLE_ADMIN,
        RoleTypes.ROLE_USER
      ] // List out all roles that are acceptable
    }
  },
  {
    path: 'form-filling',
    component: FormFillingComponent,
    canActivate: [AuthGuard], // ONLY acceptable ELEVATION can access after login
    data: {
      elevation: [
        RoleTypes.ROLE_ADMIN,
        RoleTypes.ROLE_USER
      ] // List out all roles that are acceptable
    }
  },
  { path: '**', redirectTo: 'prediction-summary-page' }
];
