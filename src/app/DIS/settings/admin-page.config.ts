export const config = {
    OTP_ALIAS: "CONFIGURE_TOTP",
    OTP_NAME: "Configure OTP",
    DEFAULT_GROUP: "New User Group",
    SAMPLE_EXCEL_DATA: [
        {
          username: 'Sample_1',
          group: 'User Group',
          fullname: 'SampleName One',
          email: 'SampleEmail_1@gmail.com',
          mobile: '88888888'
        },
        {
          username: 'Sample_2',
          group: 'Admin Group',
          fullname: 'SampleName Two',
          email: 'SampleEmail_2@gmail.com',
          mobile: '88888888'
        }
    ]
};