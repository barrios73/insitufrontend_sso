import { RoleTypes } from '@dis/auth/roles.enum';
// Will allow show/hide of links in sidebar when sign-on flow is implemented

export const config = [
  // Add navigation group here
  {
    group: 'Dashboard',
    // Add navigation items here
    items: [
      {
        name: 'Real-Time Dashboard',
        icon: 'crosstab',
        link: './prediction-summary-page',
        elevation: [RoleTypes.ROLE_ADMIN, RoleTypes.ROLE_USER]  //no RoleTypes.ROLE_MANAGER in SSO template
      }
    ]
  },
  {
    group: 'Configuration',    
    items: [
      {
        name: 'ML Model',
        icon: 'login',
        link: './ml-config-page',
        elevation: [
          RoleTypes.ROLE_ADMIN
        ]
      },
      // {
      //   name: 'ML Model Type',
      //   icon: 'login',
      //   link: './ml-modeltype-page',
      //   elevation: [
      //     RoleTypes.ROLE_ADMIN
      //   ]
      // },
      {
        name: 'ML Model File',
        icon: 'login',
        link: './ml-modelfile-page',
        elevation: [
          RoleTypes.ROLE_ADMIN
        ]
      },
      // {
      //   name: 'Upload File',
      //   icon: 'login',
      //   link: './ml-uploadfile-page',
      //   elevation: [
      //     RoleTypes.ROLE_ADMIN
      //   ]
      // },
      // {
      //   name: 'System',
      //   icon: 'login',
      //   link: './login',
      //   elevation: [
      //     RoleTypes.ROLE_ADMIN
      //   ]
      // }
    ]
  },
  // {
  //   group: 'Admin',
  //   items: [
  //     {
  //       name: 'User Admin',
  //       icon: 'crosstab',
  //       link: './sample2',
  //       elevation: [
  //         RoleTypes.ROLE_ADMIN
  //       ] 
  //     }

  //   ]
  // },
  {
    group: 'Analytics',
    items: [
    {
      name: 'Yield Trend',
      icon: 'crosstab',
      link: './analytics-yieldtrend-page',
      elevation: [RoleTypes.ROLE_ADMIN, RoleTypes.ROLE_USER]  //no RoleTypes.ROLE_MANAGER in SSO template
    },
    // {
    //   name: 'Quality Issues',
    //   icon: 'crosstab',
    //   link: './analytics-issues-page',
    //   elevation: [RoleTypes.ROLE_ADMIN, RoleTypes.ROLE_USER]  //no RoleTypes.ROLE_MANAGER in SSO template
    // }
    ]
  },
  // {
  //   group: 'Dashboard Sample',    
  // //   // Add navigation items here
  //   items: [
  //      {
  //        name: 'Dashboard Sample 1',
  //        icon: 'crosstab',
  //        link: './dashboard-one',
  //        elevation: [RoleTypes.ROLE_USER] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
  //      },
  //      {
  //        name: 'Dashboard Sample 2',
  //        icon: 'crosstab',
  //        link: './dashboard-two',
  //        elevation: [RoleTypes.ROLE_USER] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
  //      },
  //      {
  //        name: 'Dashboard Sample 3',
  //        icon: 'crosstab',
  //        link: './dashboard-three',
  //        elevation: [RoleTypes.ROLE_USER] // Specify user roles allowed to see this link: NOT YET IMPLEMENTED
  //      },
  //      {
  //        name: 'Table',
  //        icon: 'crosstab',
  //        link: './table',
  //        elevation: [RoleTypes.ROLE_USER]
  //      },
  //     {
  //       name: 'Input Field',
  //       icon: 'crosstab',
  //       link: './input-field',
  //       elevation: [RoleTypes.ROLE_USER, RoleTypes.ROLE_ADMIN]
  //     },
  // //     {
  // //       name: 'Form Filling',
  // //       icon: 'crosstab',
  // //       link: './form-filling',
  // //       elevation: [RoleTypes.ROLE_USER]
  // //     }
  //    ]
  // }
];
